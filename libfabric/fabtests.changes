-------------------------------------------------------------------
Mon Nov 22 07:57:54 UTC 2021 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.14.0
  - Add time stamps to log messages
  - Fix gdrcopy calculation of memory region size when aligned
  - Allow user to disable use of p2p transfers
  - Update fi_tostr print FI_SHARED_CONTEXT text instead of value
  - Update fi_tostr to output field names matching header file names
  - Fix narrow race condition in ofi_init
  - Add new fi_log_sparse API to rate limit repeated log output
  - Define memory registration for buffers used for collective operations
  - EFA, SHM, TCP, RXM, and verbs fixes

-------------------------------------------------------------------
Wed Nov  3 07:53:20 UTC 2021 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Enable PSM3 provider (jsc#SLE-18754)

-------------------------------------------------------------------
Fri Oct 29 11:13:43 UTC 2021 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.13.2
  - Sort DL providers to ensure consistent load ordering
  - Update hooking providers to handle fi_open_ops calls to avoid crashes
  - Replace cassert with assert.h to avoid C++ headers in C code
  - Enhance serialization for memory monitors to handle external monitors
  - EFA, SHM, TCP, RxM and vers fixes

-------------------------------------------------------------------
Wed Aug 25 07:41:46 UTC 2021 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.13.1
  - Enable loading ZE library with dlopen()
  - Add IPv6 support to fi_pingpong
  - EFA, PSM3 and SHM fixes

-------------------------------------------------------------------
Wed Jul  7 11:13:26 UTC 2021 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.13.0
  - Fix behavior of fi_param_get parsing an invalid boolean value
  - Add new APIs to open, export, and import specialized fid's
  - Define ability to import a monitor into the registration cache
  - Add API support for INT128/UINT128 atomics
  - Fix incorrect check for provider name in getinfo filtering path
  - Allow core providers to return default attributes which are lower then
    maximum supported attributes in getinfo call
  - Add option prefer external providers (in order discovered) over internal
    providers, regardless of provider version
  - Separate Ze (level-0) and DRM dependencies
  - Always maintain a list of all discovered providers
  - Fix incorrect CUDA warnings
  - Fix bug in cuda init/cleanup checking for gdrcopy support
  - Shift order providers are called from in fi_getinfo, move psm2 ahead of
    psm3 and efa ahead of psmX
  - See NEWS.md for changelog

-------------------------------------------------------------------
Fri Apr  2 07:30:34 UTC 2021 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.12.1
  - Fix initialization checks for CUDA HMEM support
  - Fail if a memory monitor is requested but not available
  - Adjust priority of psm3 provider to prefer HW specific providers,
    such as efa and psm2
  - EFA and PSM3 fixes
  - See NEWS.md for changelog

-------------------------------------------------------------------
Tue Mar  9 08:43:43 UTC 2021 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.12.0
  - See NEWS.md for changelog

-------------------------------------------------------------------
Wed Dec 16 08:29:07 UTC 2020 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.11.2 (bsc#1181983)
  - See NEWS.md for changelog

-------------------------------------------------------------------
Mon Oct 12 10:40:29 UTC 2020 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.11.1 (jsc#SLE-13312)
  - See NEWS.md for changelog
-------------------------------------------------------------------
Tue Aug 18 08:12:27 UTC 2020 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.11.0
  - See NEWS.md for changelog

-------------------------------------------------------------------
Thu May 14 08:59:09 UTC 2020 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.10.1
  - See NEWS.md for changelog

-------------------------------------------------------------------
Mon Apr 27 13:04:26 UTC 2020 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.10.0
  - See NEWS.md for changelog

-------------------------------------------------------------------
Thu Mar 19 08:29:38 UTC 2020 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.9.1 (bsc#1160275)
  - See NEWS.md for changelog

-------------------------------------------------------------------
Mon Nov 25 09:39:53 UTC 2019 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.9.0 (jsc#SLE-8257)
  - See NEWS.md for changelog

-------------------------------------------------------------------
Tue Oct  1 05:57:27 UTC 2019 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.8.1 (jsc#SLE-8257)
  - See NEWS.md for changelog

-------------------------------------------------------------------
Fri Sep  6 07:10:57 UTC 2019 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.8.0
  - See NEWS.md for changelog

-------------------------------------------------------------------
Wed Apr 24 17:13:07 UTC 2019 - Martin Liška <mliska@suse.cz>

- Disable LTO (boo#1133235).

-------------------------------------------------------------------
Tue Apr  9 06:46:41 UTC 2019 - Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>

- Update to 1.7.1
  - See NEWS.md for changelog

-------------------------------------------------------------------
Mon Feb 11 10:34:29 UTC 2019 - Jan Engelhardt <jengelh@inai.de>

- Remove silly Prefix: value, we do not support that in SUSE anyway.
- Update summaries, make use of %make_install.

-------------------------------------------------------------------
Thu Feb  7 07:24:21 UTC 2019 - nmoreychaisemartin@suse.com

- Update to v1.7.0
  - fabtests and libfabric repos have been merged upstream
  - Core
    - Add ability to report NIC details with fi_info data
    - Improve MR cache notification mechanisms
    - Set sockaddr address format correctly
    - Avoid possible null dereference in eq_read
    - Handle FI_PEEK in CQ/EQ readerr
    - Add debug messages to name server
    - Feature and performance enhancements added to internal buffer pool
    - Add support for huge pages
    - Decrease memory use for idle buffer pools
    - Refactor utility AV functionality
    - Generic counter support enhancements
    - Optimize EP and CQ locking based on application threading level
    - Enhance common support for EQ error handling
    - Add free/alloc memory notification hooks for MR cache support
    - Fix memory monitor unsubscribe handling
    - Add CQ fd wait support
    - Add CQ overflow protection
    - Enhance IPv6 addressing support for AVs
    - Enhancements to support for AV address lookup
    - Fixes for emulated epoll support
    - Allow layering of multiple utility providers
    - Minor bug fixes and optimization
  - Hook
    - Improved hooking infrastructure
    - Add support for installing multiple hooks
    - Support hooks provided by external libraries.
  - GNI
    - Fix CQ readfrom overwriting src_addr in case of multiple events
    - Signal wait set if error entry is added to CQ
    - Fix state data issue with SMSG buffers
    - Enhance and fix possible misuse of default authorization key
    - Add cancel support for SEP
    - Rework SEP setup
    - Suppress huge page counting for ARM
    - Fix incorrect check of FI_SYNC_ERR flag
  - PSM2
    - Requires PSM2 library version 10.2.260 or later
    - Clean up connection state in fi_av_remove
    - Use psm2_info_query to read HFI device info
    - Clean up CQ/counter poll list when endpoint is closed
    - Support shared address vector
    - Optimize CQ event conversion with psm2_mq_ipeek_dequeue_multi
    - Lock optimization for FI_THREAD_DOMAIN
    - Use new PSM2 fast path isend/irecv functions for large size RMA
    - Support building with latest PSM2 source code (version 11.2.68)
    - Support fabric direct
  - RxD
    - Initial release of RxD provider
    - Provides reliable datagram semantics over unreliable datagram EPs
    - Target is to improve scalability for very large clusters relative to RxM
  - RxM
    - Decrease memory use needed to maintain large number of connections
    - Set correct op_context and flags on CQ error completions
    - Fix file descriptor memory leaks
    - Introduce new protocol optimized for medium message transfers
    - Improve Rx software performance path
    - Use shared receive contexts if required by underlying provider
    - Handle addresses inserted multiple times into AV (for AV map)
    - Performance optimizations for single-thread applications
    - Rework deferred transmit processing
    - Separate and optimize eager and rendezvous protocol processing.
    - Fix passing incorrect addresses for AV insert/remove
    - Fix CM address handling
    - Fix race condition accessing connection handles
    - Simplify small RMA code path
    - Increment correct counter when processing FI_READ events
    - Dynamically grow the number of connections that can be supported
    - Fix padding in wire protocol structures
    - Report correct fi_addr when FI_SOURCE is requested
    - Fix truncating rendezvous messages
    - Fix use after free error in Rx buffer processing
    - Add support for manual progress
    - Make Tx/Rx queue sizes independent of MSG EP sizes
    - Decrease time needed to repost buffers to the MSG EP Rx queue.
    - Miscellaneous bug fixes
  - Sockets
    - Enable MSG EPs when user calls fi_accept
    - Fix fabric names to be underlying IP address
    - Add connection timeout environment variable.
    - Use size of addresses, not structures
    - Add debug messages to display selected addresses
    - Use loopback address in place of localhost
    - Simplify listen paths
    - Add support for IPv6
    - Code restructuring
    - Avoid unneeded address to string to address translations
    - Check length of iovec entries prior to access buffers
    - Fix segfault
    - Avoid acquiring nested spinlocks resulting in hangs
    - Fix use after free error in triggered op handling
    - New connection manager for MSG EPs to reduce number of threads
    - Avoid retrying recv operations if connection has been broken
    - Fixes for Windows socket support
  - TCP
    - Initial release of optimized socket based tcp provider
    - Supports MSG EPs, to be used in conjunction with RxM provider
    - Targets eventual replacement of sockets provider
  - Verbs
    - Remove RDM EP support.  Use RxM and RxD for RDM EPs.
    - Improve address handling and report in fi_getinfo
    - Handle FI_PEER when calling CQ/EQ readerr functions
    - Add support for XRC QPs.
    - Ignore destination address when allocating a PEP
    - Add workaround for i40iw incorrect return values when posting sends
    - Fix completion handling for FI_SELECTIVE_COMPLETION EP setting
    - Change format of fabric name to use hex instead of decimal values
    - Fix handling of err_data with EQ readerr
    - Report correct size of max_err_data
    - Fast path performance improvements
    - Improve progress under high system load
    - Optimize completion processing when handling hidden completions
    - Optimize RMA and MSG transfers by pre-formatting work requests
    - Remove locks based on application threading model
    - Add overflow support for CQ error events
    - Minor cleanups and bug fixes

-------------------------------------------------------------------
Thu Oct 25 10:52:50 UTC 2018 - nmoreychaisemartin@suse.com

- Update to v1.6.2 (fate#325852)
  - Core
    - Cleanup of debug messages
    - Fix compile issues with older compilers
    - Check that all debug compiler flags are supported by compiler
  - GNI
    - Fix problems with Scalable Endpoint creation
    - Fix interoperability problem with HPC toolkit
    - Improve configuration check for kdreg
  - PSM
    - Enforce FI_RMA_EVENT checking when updating counters
    - Fix race condition in fi_cq_readerr()
    - Always try to make progress when fi_cntr_read is called
  - PSM2
    - Revert "Avoid long delay in psm2_ep_close"
    - Fix memory corruption related to sendv
    - Performance tweak for bi-directional send/recv on KNL
    - Fix CPU detection
    - Enforce FI_RMA_EVENT checking when updating counters
    - Remove stale info from address vector when disconnecting
    - Fix race condition in fi_cq_readerr()
    - Adjust reported context numbers for special cases
    - Always try to make progress when fi_cntr_read is called
    - Support control functions related to MR mode
    - Unblock fi_cntr_wait on errors
    - Properly update error counters
    - Fix irregular performance drop for aggregated RMA operations
    - Reset Tx/Rx context counter when fabric is initialized
    - Fix incorrect completion event for iov send
    - Fix occasional assertion failure in psm2_ep_close
    - Avoid long delay in psm2_ep_close
    - Fix potential duplication of iov send completion
    - Replace some parameter checking with assertions
    - Check iov limit in sendmsg
    - Avoid adding FI_TRIGGER caps automatically
    - Avoid unnecessary calls to psmx2_am_progress()
  - RXM
    - Fix incorrect increments of error counters for small messages
    - Increment write completion counter for small transfers
    - Use FI_UNIVERSE_SIZE when defining MSG provider CQ size
    - Make TX, RX queue sizes independent of MSG provider
    - Make deferred requests opt-in
    - Fill missing rxm_conn in rx_buf when shared context is not used
    - Fix an issue where MSG endpoint recv queue got empty resulting
  in a hang
    - Set FI_ORDER_NONE for tx and rx completion ordering
    - Serialize access to repost_ready_list
    - Reprocess unexpected messages on av update
    - Fix a bug in matching directed receives
    - Fix desc field when postponing RMA ops
    - Fix incorrect reporting of mem_tag format
    - Don't include FI_DIRECTED_RECV, FI_SOURCE caps if they're not needed
    - Fix matching for RMA I/O vectors
    - Fix reading pointer after freeing it.
    - Avoid reading invalid AV entry
    - Handle deleting the same address multiple times
    - Fix crash in fi_av_remove if FI_SOURCE wasn't enabled
  - Sockets
    - Increase maximum messages size as MPICH bug work-around
    - Fix use after free error handling triggered ops.
  - Verbs
    - Detect string format of wildcard address in node argument
    - Don't report unusable fi_info (no source IP address)
    - Don't assert when a verbs device exposes unsupported MTU types
    - Report correct rma_iov_limit
    - Add new variable - FI_VERBS_MR_CACHE_MERGE_REGIONS
    - eq->err.err must return a positive error code

-------------------------------------------------------------------
Thu Mar 15 06:51:08 UTC 2018 - nmoreychaisemartin@suse.com

- Update to v1.6.0
  - Fixes stack smashing when using the verbs provider (bsc#1089190)
  - Core
    - Introduces support for performing RMA operations to persistent memory
      See FI_RMA_PMEM capability in fi_getinfo.3
    - Define additional errno values
    - General code cleanups and restructuring
    - Force provider ordering when using dynamically loaded providers
    - Add const to fi_getinfo() hints parameter
    - Improve use of epoll for better scalability
    - Fixes to generic name service
  - PSM
    - Move environment variable reading out from fi_getinfo()
    - Shortcut obviously unsuccessful fi_getinfo() calls
    - Remove excessive name sever implementation
    - Enable ordering of RMA operations
  - PSM2
    - Skip inactive units in round-robin context allocation
    - Allow contexts be shared by Tx-only and Rx-only endpoints
    - Use utility functions to check provider attributes
    - Turn on FI_THREAD_SAFE support
    - Make address vector operations thread-safe
    - Move environment variable reading out from fi_getinfo()
    - Reduce noise when optimizing tagged message functions
    - Shortcut obviously unsuccessful fi_getinfo() calls
    - Improve how Tx/Rx context limits are handled
    - Support auto selection from two different tag layout schemes
    - Add provider build options to debug output
    - Support remote CQ data for tagged messages, add specialization.
    - Support opening multiple domains
    - Put trigger implementation into a separate file
    - Update makefile and configure script
    - Replace allocated context with reserved space in psm2_mq_req
    - Limit exported symbols for DSO provider
    - Reduce HW context usage for certain TX only endpoints
    - Remove unnecessary dependencies from the configure script
    - Refactor the handling of op context type
    - Optimize the conversion between 96-bit and 64-bit tags
    - Code refactoring for completion generation
    - Remove obsolete feature checking code
    - Report correct source address for scalable endpoints
    - Allow binding any number of endpoints to a CQ/counter
    - Add shared Tx context support
    - Add alternative implementation for completion polling
    - Change the default value of FI_PSM2_DELAY to 0
    - Add an environment variable for automatic connection cleanup
    - Abstract the completion polling mechanism
    - Use the new psm2_am_register_handlers_2 function when available
    - Allow specialization when FI_COMPLETION op_flag is set.
    - Put Tx/Rx context related functions into a separate file
    - Enable PSM2 multi-ep feature by default
    - Add option to build with PSM2 source included
    - Simplify the code for checking endpoint capabilities
    - Simplify the handling of self-targeted RMA operations
    - Allow all free contexts be used for scalable endpoints
    - Enable ordering of RMA operations
    - Enable multiple endpoints over PSM2 multi-ep support
    - Support multiple Tx/Rx contexts in address vector
    - Remove the virtual lane mechanism
    - Less code duplication in tagged, add more specialization.
    - Allow PSM2 epid be reused within the same session
    - Turn on user adjustable inject size for all operations
    - Use pre-allocated memory pool for RMA requests
    - Add support for lazy connection
    - Various bug fixes
  - SHM
    - Initial release of shared memory provider
    - See the fi_shm.7 man page for details on available features and limitations
  - Sockets
    - Scalability enhancements
    - Fix issue associating a connection with an AV entry that could result in
      application hangs
    - Add support for new persistent memory capabilities
    - Fix fi_cq_signal to unblock threads waiting on cq sread calls
    - Fix epoll_wait loop handling to avoid out of memory errors
    - Add support for TCP keepalives, controllable via environment variables
    - Reduce the number of threads allocated for handling connections
    - Several code cleanups in response to static code analysis reports
    - Fix reporting multiple completion events for the same request in error cases
  - usNIC
    - Minor adjustments to match new core MR mode bits functionality
    - Several code cleanups in response to static code analysis reports
  - Verbs
    - Code cleanups and simplifications
    - General code optimizations to improve performance
    - Fix handling of wildcard addresses
    - Check for fatal errors during connection establishment
    - Support larger inject sizes
    - Fix double locking issue
    - Add support for memory registration caching (disabled by default)
    - Enable setting thread affinity for CM threads
    - Fix hangs in MPI closing RDM endpoints
    - Add support for different CQ formats
    - Fix RMA read operations over iWarp devices
    - Optimize CM progress handling
    - Several bug fixes

-------------------------------------------------------------------
Wed Dec 20 08:49:03 UTC 2017 - nmoreychaisemartin@suse.com

- Update to v1.5.3
  - Core
    - Handle malloc failures
    - Ensure global lock is initialized on Windows
    - Fix spelling and formatting errors in man pages
  - PSM
    - Fix print format mismatches
    - Remove 15 second startup delay when no hardware is installed
    - Preserve FI_MR_SCALABLE mode bit for backwards compatability
  - PSM2
    - Fix print format mismatches
    - Allow all to all communication between scalable endpoints
    - Preserve FI_MR_SCALABLE mode bit for backwards compatability
    - Fix reference counting issue with opened domains
    - Fix segfault for RMA/atomic operations to local scalable endpoints
    - Fix resource counting related issues for Tx/Rx contexts
    - Allow completion suppression when fi_context is non-NULL
    - Use correct queue for triggered operations with scalable endpoints
  - Sockets
    - Fix check for invalid connection handle
    - Fix crash in fi_av_remove
  - Util
    - Fix number of bits used for connection index
  - Verbs
    - Fix incorrect CQ entry data for MSG endpoints
    - Properly check for errors from getifaddrs
    - Retry getifaddr on failure because of busy netlink sockets
    - Ack CM events on error paths
- Remove 0001-prov-psm-Eliminate-psm2-compat-library-delay-with-hf.patch
   as it was merged upstream

-------------------------------------------------------------------
Mon Nov 20 16:27:13 UTC 2017 - nmoreychaisemartin@suse.com

- Update to v1.5.2
  - Core
    - Fix Power PC 32-bit build
  - Sockets
    - Fix incorrect reporting of counter attributes
  - Verbs
    - Fix reporting attributes based on device limits
    - Fix incorrect CQ size reported for iWarp NICs
    - Update man page with known issues for specific NICs
    - Fix FI_RX_CQ_DATA mode check
    - Disable on-demand paging by default (can cause data corruption)
    - Disable loopback (localhost) addressing (causing failures in MPI)

-------------------------------------------------------------------
Mon Oct  9 23:28:31 UTC 2017 - stefan.bruens@rwth-aachen.de

- Fix github issue #3393:
  Add 0001-prov-psm-Eliminate-psm2-compat-library-delay-with-hf.patch

-------------------------------------------------------------------
Thu Oct  5 07:10:28 UTC 2017 - nmoreychaisemartin@suse.com

- Update to v1.5.1
  - Core
    - Fix initialization used by DL providers to avoid crash
    - Add checks for null hints and improperly terminated strings
    - Check for invalid core names passed to fabric open
    - Provide consistent provider ordering when using DL providers
    - Fix OFI_LIKELY definitions when GNUC is not present
  - GNI
    - Add ability to detect local PE rank
    - Fix compiler/config problems
    - Fix CQ read error corruption
    - Remove tests of deprecated interfaces
  - PSM
    - Fix CQ corruption reporting errors
    - Always generate a completion on error
  - PSM2
    - Fix CQ corruption reporting errors
    - Always generate a completion on error
    - Add checks to handle out of memory errors
    - Add NULL check for iov in atomic readv/writev calls
    - Fix FI_PEEK src address matching
    - Fix bug in scalable endpoint address resolution
    - Fix segfault bug in RMA completion generation
  - Sockets
    - Fix missing FI_CLAIM src address data on completion
    - Fix CQ corruption reporting errors
    - Fix serialization issue wrt out of order CPU writes to Tx ring buffer
  - Verbs
    - Allow modifying rnr retry timout to improve performance
    - Add checks to handle out of memory errors
    - Fix crash using atomic operations for MSG EPs
- Fix dependency to libfabric1 for libfabric-devel in baselibs.conf

-------------------------------------------------------------------
Tue Sep  5 09:56:19 UTC 2017 - nmoreychaisemartin@suse.com

- Update _service to allow auto updates from github

-------------------------------------------------------------------
Thu Aug 10 08:29:42 UTC 2017 - nmoreychaisemartin@suse.com

- Update to v1.5.0
  * Authorization keys Authorization keys, commonly referred to as job keys,
    are used to isolate processes from communicating with other processes
    for security purposes.
  * Multicast support Datagram endpoints can now support multicast communication.
  * (Experimental) socket-like endpoint types New FI_SOCK_STREAM and FI_SOCK_DGRAM
    endpoint types are introduced. These endpoint types target support of cloud
    and enterprise based middleware and applications.
  * Tagged atomic support Atomic operations can now target tagged receive
    buffers, in addition to RMA buffers.
  * (Experimental) deferred work queues Deferred work queues are enhanced triggerred
    operations. They target support for collective-based operations.
  * New mode bits: FI_RESTRICTED_COMP and FI_NOTIFY_FLAGS_ONLY These mode bits
    support optimized completion processing to minimize software overhead.
  * Multi-threaded error reporting Reading CQ and EQ errors now allow the application
    to provide the error buffer, eliminating the need for the application to
    synchronize between multiple threads when handling errors.
  * FI_SOURCE_ERR capability This feature allows the provider to validate and
    report the source address for any received messages.
  * FI_ADDR_STR string based addressing Applications can now request and use
    addresses provided using a standardized string format. This makes it easier
    to pass full addressing data through a command line, or handle address exchange
    through text files.
  * Communication scope capabilities: FI_LOCAL_COMM and FI_REMOTE_COMM Used to
    indicate if an application requires communication with peers on the same
    node and/or remote nodes.
  * New memory registration modes The FI_BASIC_MR and FI_SCALABLE_MR memory registration
    modes have been replaced by more refined registration mode bits. This allows
    applications to make better use of provider hardware capabilities when dealing
    with registered memory regions.
  * New mode bit: FI_CONTEXT2 Some providers need more than the size provided by the
    FI_CONTEXT mode bit setting. To accomodate such providers, an FI_CONTEXT2 mode bit
    was added. This mode bit doubles the amount of context space that an application
    allocates on behalf of the provider.
  * PSM provider notes
    * Improve the name server functionality and move to the utility code
    * Handle updated mr_mode definitions
    * Add support of 32 and 64 bit atomic values
  * PSM2 provider notes
    * Add option to adjust the locking level
    * Improve the name server functionality and move to the utility code
    * Add support for string address format
    * Add an environment vaiable for message inject size
    * Handle FI_DISCARD in tagged receive functions
    * Handle updated mr_mode definitions
    * Add support for scalable endpoint
    * Add support of 32 and 64 bit atomic values
    * Add FI_SOURCE_ERR to the supported caps
    * Improve the method of checking device existence
  * Sockets provider notes
    * Updated and enhanced atomic operation support.
    * Add support for experimental deferred work queue operations.
    * Fixed counter signaling when used with wait sets.
    * Improved support on Windows.
    * Cleaned up event reporting for destroyed endpoints.
    * Fixed several possible crash scenarios.
    * Fixed handling socket disconnect events which could hang the provider.
  * UDP provider notes
    * Add support for multicast data transfers
  * Verbs provider notes
    * Fix an issue where if the user requests higher values for tx, rx
      context sizes than default it wasn't honored.
    * Introduce env variables for setting default tx, rx context sizes and iov limits.
    * Report correct completion ordering supported by MSG endpoints.
- Fix rpmbuild warnings
- libfabric-devel requires libfabric1, not libfabric
- Fix baselibs.conf

-------------------------------------------------------------------
Tue Jul  4 09:21:35 UTC 2017 - nmoreychaisemartin@suse.com

- Enable build on all archs
- Enable mlx build

-------------------------------------------------------------------
Fri Jun 30 07:42:15 UTC 2017 - nmoreychaisemartin@suse.com

- Add x86 build without libpsm2

-------------------------------------------------------------------
Tue May 16 06:43:19 UTC 2017 - nmoreychaisemartin@suse.com

- Update to v1.4.2 (bsc#1036907).

-------------------------------------------------------------------
Thu May 11 18:14:41 UTC 2017 - nmoreychaisemartin@suse.com

- Update to v1.4.2-rc1 (bsc#1036907).
- Update notes:
 - Fix for OS X clock_gettime() portability issue.
 - Updated default counter wait object for improved performance
 - Fix multi-threaded RMA progress stalls
 - Updated default counter wait object for improved performance
 - Fix multi-threaded RMA progress stalls
 - Fix error in fi_cq_sreadfrom aborting before timeout expires
 - Set atomic iov count correct correctly inside fi_atomicv
 - Fix handling of apps that call fork.  Move ibv_fork_init() before
   calling any other verbs call.
 - Fix crash in fi_write when connection is not yet established and
   write data size is below inline threshold.
 - Fix issues not handling multiple ipoib interfaces
 - Reduce lock contention on buffer pools in send/completion handling
   code.
 


-------------------------------------------------------------------
Wed Apr  5 10:19:28 UTC 2017 - josef.moellers@suse.com

- This version fixes bnc#990184
  (bnc#990184)

-------------------------------------------------------------------
Thu Mar 23 16:21:53 UTC 2017 - jengelh@inai.de

- RPM group fix

-------------------------------------------------------------------
Fri Mar 10 15:58:55 UTC 2017 - josef.moellers@suse.com

- PSM provider notes
  - Defer initialization of the PSM library to allow runtime selection from
    different versions of the same provider before fi_getinfo is called.
- PSM2 provider notes
  - Defer initialization of the PSM2 library to allow runtime selection from
    different versions of the same provider before fi_getinfo is called.
  - General bug fixes.
- UDP provider notes
  - Fix setting address format in fi_getinfo call.
- usNIC provider notes
  - Fixed compilation issues with newer versions of libibverbs.
  (fate#321883)
-------------------------------------------------------------------
Mon Jan 16 13:12:14 CET 2017 - ndas@suse.de

- Updated to version 1.4.0 for general stability(fate#321883)
- Summary of changes as follow: 
 - Add new options, `-f` and `-d`, to fi_info that can be used to
  specify hints about the fabric and domain name. Change port to `-P`
  and provider to `-p` to be more in line with fi_pingpong.

 *GNI provider notes

	- General bug fixes, plugged memory leaks, performance improvements,
  	  improved error handling and warning messages, etc.
	- Additional API support:
    - FI_THREAD_COMPLETION
    - FI_RMA_EVENT
    - iov length up to 8 for messaging data transfers

 *PSM provider notes

	- General bug fixes
	- Use utility provider for EQ, wait object, and poll set
	- Allow multi-recv to post buffer larger than message size limit

 *PSM2 provider notes

	- General bug fixes
    - Add support for multi-iov RMA read and aromic operations
    - Allow multi-recv to post buffer larger than message size limit


 *Verbs provider notes

	- Add fork support. It is enabled by default and can be turned off by
	setting the FI_FORK_UNSAFE variable to "yes". This can improve
	performance of memory registrations but also makes fork unsafe. The
	following are the limitations of fork support:
  	- Fabric resources like endpoint, CQ, EQ, etc. should not be used in
    the forked process.
  	- The memory registered using fi_mr_reg has to be page aligned since
    ibv_reg_mr marks the entire page that a memory region belongs to
    as not to be re-mapped when the process is forked (MADV_DONTFORK).
	- Fix a bug where source address info was not being returned in
  	fi_info when destination node is specified.

-------------------------------------------------------------------
Fri May  6 12:51:41 CEST 2016 - nads@suse.de

- Updated to version 1.3.0 for better PSM2 support as suggested by 
  fate#319253, comment #9.
  
  [*libfabric-libtool.patch]

  Summary of changes as follow: 
	*PSM provider notes
		- Remove PSM2 related code.
	*PSM2 provider notes
		- Add support for multi-iov send, tagged send, and RMA write.
		- Use utility provider for EQ, wait object, and poll set.
	*GNI provider notes
		- General bug fixes, plugged memory leaks, etc.
		- Added support for the following APIs:
  			- fi_endpoint: fi_getopt, fi_setopt, fi_rx_size_left, fi_tx_size_left, fi_stx_context
  			- fi_cq: fi_sread, fi_sreadfrom
  			- fi_msg: FI_MULTI_RECV (flag)
  			- fi_domain: FI_PROGRESS_AUTO (flag)
  			- fi_direct: FI_DIRECT
		- Added support for FI_EP_DGRAM (datagram endpoint):
		- Memory registration improvements:
		- Initial support for Cray Cluster Compatibility Mode (CCM)
	*MXM provider notes
		- Initial release
	*Sockets provider notes
		- Enable FABRIC_DIRECT
		- Enable sockets-provider to run on FreeBSD
		- Add support for fi_trywait
		- Add support for map_addr in shared-av creation
		- Add shared-av support on OSX
		- General bug fixes 
	*UDP provider notes
		- Initial release
	*usNIC provider notes
		- Implement fi_recvv and fi_recvmsg for FI_EP_RDM. [PR #1594]
		- Add support for FI_INJECT flag in the FI_EP_RDM implementation of fi_sendv.
	  	[PR #1594]
		- Handle FI_PEEK flag in fi_eq_sread. [PR #1758]
		- Implement waitsets [PR #1893]
		- Implement fi_trywait [PR #1893]
		- Fix progress thread deadlock [PR #1893]
		- Implement FD based CQ sread [PR #1893]
	*Verbs provider notes
	- Add support for fi_trywait
	- verbs/RDM
  		- Add support for RMA operations.
  		- Add support for fi_cq_sread and fi_cq_sreadfrom
  		- Rework connection management to make it work with fabtests and also allow
    	connection to self.
  	- Other bug fixes and performance improvements.

-------------------------------------------------------------------
Wed Apr  6 16:20:41 CEST 2016 - ndas@suse.de

- Moved man pages to main package.
- Fixed invalid library group.

-------------------------------------------------------------------
Wed Apr  6 15:40:25 CEST 2016 - ndas@suse.de

- Packaging version 1.2.0 for fate#319253

-------------------------------------------------------------------
Fri Feb 12 10:18:49 CET 2016 - pth@suse.de

- Use explicit file list instead of wildcards
- Package fi_info.
- Remove libtool.m4 from the package so that autoreconf installs
  a current version.

-------------------------------------------------------------------
Thu Feb 11 10:18:41 CET 2016 - pth@suse.de

- Initial package, based on the OFED specfile for libfabric
- Add libfabric-libtool.patch to disable static builds by default.
