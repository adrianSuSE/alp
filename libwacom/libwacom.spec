#
# spec file for package libwacom
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

%if 0%{?suse_version} > 1510
%bcond_without meson
%else
%bcond_with meson
%endif
Name:           libwacom
Version:        1.12
Release:        0
Summary:        Library to identify wacom tablets
License:        MIT
Group:          System/Libraries
URL:            https://linuxwacom.github.io/
#!RemoteAsset: sha256:290450d604f78bbd956eddb69f79f8d56f8ed1a5ccbb5e88e22fa84fa2fceb4f
Source0:        https://github.com/linuxwacom/libwacom/releases/download/%{name}-%{version}/%{name}-%{version}.tar.bz2
#!RemoteAsset: sha256:c1953ba197cbe5d8d6d57a0e5a891543fc77908c9a4399113d0253c397ba7a3f
Source1:        https://github.com/linuxwacom/libwacom/releases/download/%{name}-%{version}/%{name}-%{version}.tar.bz2.sig
Source2:        %{name}.keyring
Source99:       baselibs.conf
%if %{with meson}
BuildRequires:  meson >= 0.51.0
%endif
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gudev-1.0)
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  doxygen

%description
libwacom is a library to identify wacom tablets and their model-specific
features. It provides easy access to information such as "is this a
built-in on-screen tablet", "what is the size of this model", etc.

%package -n libwacom2
Summary:        Library to identify wacom tablets
Group:          System/Libraries
Requires:       %{name}-data >= %{version}

%description -n libwacom2
libwacom is a library to identify wacom tablets and their model-specific
features. It provides easy access to information such as "is this a
built-in on-screen tablet", "what is the size of this model", etc.

%package data
Summary:        Library to identify wacom tablets -- Data Files
Group:          System/Libraries

%description data
libwacom is a library to identify wacom tablets and their model-specific
features. It provides easy access to information such as "is this a
built-in on-screen tablet", "what is the size of this model", etc.

%package tools
Summary:        Library to identify wacom tablets -- Tools
Group:          Hardware/Other
Requires:       python3-libevdev
Requires:       python3-pyudev

%description tools
libwacom is a library to identify wacom tablets and their model-specific
features. It provides easy access to information such as "is this a
built-in on-screen tablet", "what is the size of this model", etc.

%package devel
Summary:        Library to identify wacom tablets -- Development Files
Group:          Development/Libraries/C and C++
Requires:       libwacom2 = %{version}

%description devel
libwacom is a library to identify wacom tablets and their model-specific
features. It provides easy access to information such as "is this a
built-in on-screen tablet", "what is the size of this model", etc.

%prep
%setup -q

%build
%if %{with meson}
%meson -Db_lto=true -Dtests=disabled
%meson_build
%else
%configure \
        --with-udev-dir=%{_udevrulesdir}/.. \
        --disable-static
make %{?_smp_mflags}
%endif

%install
%if %{with meson}
%meson_install
%else
%make_install
%endif

sed -e 's-#!/usr/bin/env python3-#!/usr/bin/python3-g' -i %{buildroot}%{_bindir}/*
find %{buildroot} -type f -name "*.la" -delete -print

%if %{with meson}
%check
%meson_test
%endif

%post -n libwacom2 -p /sbin/ldconfig
%postun -n libwacom2 -p /sbin/ldconfig

%files -n libwacom2
%license COPYING
%doc NEWS README.md
%{_libdir}/libwacom.so.2*

%files data
%dir %{_datadir}/libwacom
%{_datadir}/libwacom/*.tablet
%{_datadir}/libwacom/*.stylus
%{_datadir}/libwacom/layouts/
%dir %{_udevrulesdir}
%{_udevrulesdir}/65-libwacom.rules
%dir %{_udevhwdbdir}
%{_udevhwdbdir}/65-libwacom.hwdb

%files tools
%{_bindir}/libwacom-list-devices
%{_bindir}/libwacom-update-db
%{_bindir}/libwacom-show-stylus
%{_bindir}/libwacom-list-local-devices
%{_mandir}/man1/libwacom-list-devices.1%{?ext_man}
%{_mandir}/man1/libwacom-list-local-devices.1%{?ext_man}

%files devel
%{_includedir}/libwacom-1.0/
%{_libdir}/*.so
%{_libdir}/pkgconfig/libwacom.pc

%changelog
