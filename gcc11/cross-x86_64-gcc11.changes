-------------------------------------------------------------------
Thu Feb 10 12:31:01 UTC 2022 - Richard Biener <rguenther@suse.com>

- Add gcc11-PIE, similar to gcc-PIE but affecting gcc11 [bsc#1195628]

-------------------------------------------------------------------
Wed Feb  9 15:11:40 UTC 2022 - Richard Biener <rguenther@suse.com>

- Put libstdc++6-pp Requires on the shared library and drop
  to Recoomends.

-------------------------------------------------------------------
Tue Jan 18 10:30:23 UTC 2022 - Andreas Schwab <schwab@suse.de>

- Remove sys/rseq.h from include-fixed

-------------------------------------------------------------------
Wed Dec 15 10:39:51 UTC 2021 - Martin Liška <mliska@suse.cz>

- Update to gcc-11 branch head (d4a1d3c4b377f1d4acb), git1173
  * Fix D memory corruption in -M output.
  * Fix ICE in is_this_parameter with coroutines.  [boo#1193659]

-------------------------------------------------------------------
Fri Nov 26 15:01:01 UTC 2021 - Michael Matz <matz@suse.com>

- Enable the cross compilers also on i586
- Enable some cross compilers also in rings
- Remove cross compilers for i386 target

-------------------------------------------------------------------
Wed Nov 24 10:41:45 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (7510c23c1ec53aa4a62705f03), git1018
  * fixes issue with debug dumping together with -o /dev/null
  * fixes libgccjit issue showing up in emacs build  [boo#1192951]
- Package mwaitintrin.h

-------------------------------------------------------------------
Wed Oct 20 13:59:09 UTC 2021 - Richard Biener <rguenther@suse.com>

- Remove spurious exit from change_spec.

-------------------------------------------------------------------
Mon Sep 20 17:15:19 UTC 2021 - Michael Matz <matz@suse.com>

- Enable the full cross compiler, cross-aarch64-gcc11 and
  cross-riscv64-gcc11 now provide a fully hosted C (and C++)
  cross compiler, not just a freestanding one.  I.e. with a cross
  glibc.  They don't yet support the sanitizer libraries.
  Part of [jsc#OBS-124].

-------------------------------------------------------------------
Wed Aug 18 15:07:05 UTC 2021 - Andreas Schwab <schwab@suse.de>

- Require libgccjit%{libgccjit_sover}%{libgccjit_suffix} from
  libgccjit%{libgccjit_sover}-devel%{libdevel_suffix}.

-------------------------------------------------------------------
Mon Aug 16 06:34:40 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (056e324ce46a7924b5cf10f610), git610
  * Includes GCC 11.2 release
  * Includes fix for opie build with glibc 2.34  [boo#1188623]

-------------------------------------------------------------------
Wed Jul 21 09:08:04 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (076930b9690ac3564638636f6b), git536
  * Includes GCC 11.2 RC1.
- Refresh gcc10-foffload-default.patch

-------------------------------------------------------------------
Wed Jul 21 09:06:11 UTC 2021 - Richard Biener <rguenther@suse.com>

- Properly adjust GPL-3.0 WITH GCC-exception-3.1 to
  GPL-3.0-or-later WITH GCC-exception-3.1

-------------------------------------------------------------------
Mon Jul 19 14:49:07 UTC 2021 - Andreas Schwab <schwab@suse.de>

- Remove bits/unistd_ext.h from include-fixed

-------------------------------------------------------------------
Fri Jul  9 07:34:24 UTC 2021 - Richard Biener <rguenther@suse.com>

- Add BuildRequires on netcfg for the testsuite when testing Go.

-------------------------------------------------------------------
Tue Jul  6 15:51:37 UTC 2021 - Michael Matz <matz@suse.com>

- Provide a libc-bootstrap cross compiler for aarch64 and riscv64
- More preparation for a full glibc cross compiler (not yet active)

-------------------------------------------------------------------
Fri Jun 25 11:23:02 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (62bbb113ae68a7e724255e1714), git400
  * Fixes issue with legacy Fortran code.  [gcc#101123, boo#1187273]

-------------------------------------------------------------------
Thu Jun 17 06:28:18 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (79c1185de4a05fdea13b6b0207), git340
  * Fixes ceph build failure.  [gcc#101078]

-------------------------------------------------------------------
Tue Jun 15 09:16:11 UTC 2021 - Richard Biener <rguenther@suse.com>

- Change disable_32bit to only disable multilibs for arhcs subject
  to 32bit/64bit handling and make it effective on x86_64.
- Remove the duplicate spec header from cross.spec.in

-------------------------------------------------------------------
Thu Jun 10 10:03:37 UTC 2021 - Richard Biener <rguenther@suse.com>

- Add newlib-4.1.0-aligned_alloc.patch to fix nvptx cross build
  fail.  [bsc#1187153]

-------------------------------------------------------------------
Wed Jun  9 08:50:44 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (c6d2487098f9dde4f9ac59e5be), git273

-------------------------------------------------------------------
Tue Jun  8 08:22:22 UTC 2021 - Dirk Müller <dmueller@suse.com>

- tune armv7 to generic-armv7-a
- enable build for arm-none cross builders in rings,
   needed by arm-trusted-firmware

-------------------------------------------------------------------
Tue May 25 19:21:01 UTC 2021 - Andreas Schwab <schwab@suse.de>

- Fix value of %slibdir64 for usrmerge

-------------------------------------------------------------------
Mon May 10 12:08:19 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (23855a176609fe8dda6abaf2b2), git121
- Disable build-id generation on non-glibc targeting cross compilers.

-------------------------------------------------------------------
Thu Apr 29 08:39:32 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (cd0a059bd384da58d43674496a7), git67
  * Includes GCC 11.1 release
- Drop upstreamed gcc11-no-offload.patch.

-------------------------------------------------------------------
Wed Apr 21 12:31:23 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (7a7fc01b9d20afb1a2b805d93cb), git31
  * Includes GCC 11.1 RC2
- Add gcc11-no-offload.patch and
  gcc11-amdgcn-disable-hot-cold-partitioning.patch.
- Enable gfortran for offload compilers.
- BuildRequire procps from gcc11-testresults if we test go.
- Force using llvm11 for amdgcn offloading since llvm12 doesn't
  yet work.  Package expanded symlinks so concurrent installs do
  not pull in another llvm-mc.
- Add gcc11-gdwarf-4-default.patch to default to DWARF4 generation
  in SLES15 and older.

-------------------------------------------------------------------
Tue Apr 20 14:57:07 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update to gcc-11 branch head (27350b77a92062667427100afb4), git10
  * Includes GCC 11.1 RC1

-------------------------------------------------------------------
Fri Apr 16 19:54:03 UTC 2021 - Martin Liška <mliska@suse.cz>

- Bump to 49813aad3292f7f2bef69206274da78a9a7116ed.

-------------------------------------------------------------------
Thu Apr 15 11:22:19 UTC 2021 - Richard Biener <rguenther@suse.com>

- Disable nvptx offloading on aarch64 since it doesn't work.

-------------------------------------------------------------------
Wed Apr 14 19:51:08 UTC 2021 - Martin Liška <mliska@suse.cz>

- Bump to a87d3f964df31d4fbceb822c6d293e85c117d992.

-------------------------------------------------------------------
Fri Apr  9 12:25:39 UTC 2021 - Richard Biener <rguenther@suse.com>

- Remove gcc48-remove-mpfr-2.4.0-requirement.patch which does no
  longer apply.
- Arrange for a C++ 11 capable host compiler to be available.
- Do not require ISL for cross compiler builds on old distros.

-------------------------------------------------------------------
Thu Apr  8 19:52:03 UTC 2021 - Martin Liška <mliska@suse.cz>

- Bump to 123b3e03c911a43054c1f88f5d3110e1d084dd4e.

-------------------------------------------------------------------
Mon Mar  8 14:10:15 UTC 2021 - Richard Biener <rguenther@suse.com>

- Update embedded newlib version from 3.3.0 to 4.1.0.

-------------------------------------------------------------------
Mon Feb 22 10:44:40 UTC 2021 - Martin Liška <mliska@suse.cz>

- Pack %{GCCDIST}-gcc%{binsuffix}.
- Add a new dependency for libgccjit.

-------------------------------------------------------------------
Fri Feb  5 08:40:22 UTC 2021 - Martin Liška <mliska@suse.cz>

- New package, inherits from gcc10
  * gcc-add-defaultsspec.diff, add the ability to provide a specs
    file that is read by default
  * tls-no-direct.diff, avoid direct %fs references on x86 to not
    slow down Xen
  * gcc43-no-unwind-tables.diff, do not produce unwind tables for
    CRT files
  * gcc41-ppc32-retaddr.patch, fix expansion of __builtin_return_addr
    for ppc, just a testcase
  * gcc44-textdomain.patch, make translation files version specific
    and adjust textdomain to find them
  * gcc44-rename-info-files.patch, fix cross-references in info files
    when renaming them to be version specific
  * gcc48-libstdc++-api-reference.patch, fix link in the installed
    libstdc++ html documentation
  * gcc48-remove-mpfr-2.4.0-requirement.patch, make GCC work with
    earlier mpfr versions on old products
  * gcc5-no-return-gcc43-workaround.patch, make build work with
    host gcc 4.3
  * gcc7-remove-Wexpansion-to-defined-from-Wextra.patch, removes
    new warning from -Wextra
  * gcc7-avoid-fixinc-error.diff
  * gcc9-reproducible-builds-buildid-for-checksum.patch
  * gcc9-reproducible-builds.patch
  * gcc10-amdgcn-llvm-as.patch
  * gcc10-foffload-default.patch
- libgccjit subpackage is added.
- HWASAN is built for aarch64 target.
