-------------------------------------------------------------------
Wed Nov 24 10:43:35 UTC 2021 - Johannes Segitz <jsegitz@suse.com>

- Added hardening to systemd service(s) (bsc#1181400). Added patch(es):
  * harden_sysprof2.service.patch
  * harden_sysprof3.service.patch

-------------------------------------------------------------------
Fri Nov  5 08:21:10 UTC 2021 - Bjørn Lie <bjorn.lie@gmail.com>

- Update to version 3.42.1:
  + Build fixes for close()/lseek() usage
  + Show recording time in local time instead of UTC
  + Improve resolving of symbols in presence of toolbox, flatpak,
    and btrfs
  + Fix checking of paranoid state in Linux kernel
  + Updated translations.
- Drop sysprof-fix-includes-close-lseek.patch: Fixed upstream.

-------------------------------------------------------------------
Mon Nov  1 08:38:22 UTC 2021 - Yifan Jiang <yfjiang@suse.com>

- Add sysprof-fix-includes-close-lseek.patch to fix certain build
  errors on Leap and SLE (boo#1192200).

-------------------------------------------------------------------
Wed Sep 22 07:37:54 UTC 2021 - Bjørn Lie <bjorn.lie@gmail.com>

- Update to version 3.42.0:
  + Much work has been done to improve path resolution of maps
    found within containers such as podman, toolbox, and flatpak.
- Replace gcc-c++ with generic c++_compiler BuildRequires.

-------------------------------------------------------------------
Wed Mar 24 09:43:32 UTC 2021 - Dominique Leuenberger <dimstar@opensuse.org>

- Update to version 3.40.1:
  + Fix build system issue which caused symbols from
    libsysprof-capture.a to leak into the ABI of libraries
    consuming it. This was due to inheritance of GNU default symbol
    visibility. A recompilation of those libraries is necessary.

-------------------------------------------------------------------
Sat Mar 20 09:08:57 UTC 2021 - Michael Gorse <mgorse@suse.com>

- Update to version 3.40.0:
  + Improve symbol resolving on OSTree-based systems.
  + Updated translations.

-------------------------------------------------------------------
Sat Feb 27 23:48:28 UTC 2021 - Michael Gorse <mgorse@suse.com>

- Update to version 3.39.94:
  + Fix some licenses in headers.
  + Add API to sysprofd/libsysprof to tweak.
  + /proc/sys/kernel/perf_event_paranoid to improve capture
    quality.
  + Tooling will now always try to change this value while
    profiling + the system and ignore upon failure.
  + ftruncate() before overwriting proc files.
  + GTK aid has been removed as it is always enabled through the
    use of control-fd now.
  + Add a new Overlay capture frame type for mapping overlays
    between the host and the process filesystem namespace.
  + Use overlays to map flatpak containers to overlay directories
    for both /app and /usr.
  + Changes in version 3.39.92:
  + Use pic when compiling static libsysprof-capture.
  + Some changes to podman symbol decoding.
  + Handle NULL readers gracefully from capture cursors.
  + Use g_memdup2().
  + UI tweaks.
  + Translation updates.
- Up glib2 dependency.
- Add json-glib-1.0 to BuildRequires: new dependency.

-------------------------------------------------------------------
Fri Jan 15 08:34:13 UTC 2021 - Dominique Leuenberger <dimstar@opensuse.org>

- Update to version 3.39.90:
  + polkit is optional (again) for sysprof-cli.
  + Various fixes in kernel symbol resolving.
  + Fix build when stack-protector is not used.
  + Support elfparser on some interesting platforms.
  + Updtaed translations.

-------------------------------------------------------------------
Fri Oct 16 10:57:53 UTC 2020 - Bjørn Lie <bjorn.lie@gmail.com>

- Update to version 3.38.1:
  + Support for translating paths to binary symbols has been
    improved when using Btrfs subvolumes.
  + Build system fixes.
  + Fixes for sysprof_capture_reader_list_files() to match
    expectations.
  + Load proper speedtrack library in LD_PRELOAD.
  + Fixes when generating pkg-config files.
  + Fixes when using musl libc.
  + Updated translations.

-------------------------------------------------------------------
Mon Sep 14 10:29:34 UTC 2020 - dimstar@opensuse.org

- Update to version 3.38.0:
  + Updated translations.

-------------------------------------------------------------------
Mon Sep  7 09:03:18 UTC 2020 - Dominique Leuenberger <dimstar@opensuse.org>

- Build UI in as a 2nd flavor, try to break build cycle.

-------------------------------------------------------------------
Mon Sep  7 07:25:20 UTC 2020 - dimstar@opensuse.org

- Update to version 3.37.92:
  + Build system fixes
  + Fix incorrect warnings
  + Add various missing capture APIs for GTK
  + Be more careful about when polkit is queried
  + Updated translations.

-------------------------------------------------------------------
Fri Sep  4 13:14:34 UTC 2020 - dimstar@opensuse.org

- Update to version 3.37.90:
  + Various build fixes.
- Changes from version 3.37.2:
  + sysprof-capture no longer requires GLib and therefore the ABI
    has been bumped to 4. This allowed Sysprof to be used form GLib
    without circular dependencies.
  + Various UI tweaks.
  + Support for use as a subproject.
  + Updated translations.
- Drop sysprof-fix-build-32-bit-platforms.patch: fixed upstream.

-------------------------------------------------------------------
Mon Mar  9 00:12:06 UTC 2020 - Bjørn Lie <bjorn.lie@gmail.com>

- Add sysprof-fix-build-32-bit-platforms.patch: Fix build on 32 bit
  arches (glgo#GNOME/sysprof!24).

-------------------------------------------------------------------
Sun Mar  8 14:07:55 UTC 2020 - Bjørn Lie <bjorn.lie@gmail.com>

- Update to version 3.36.0:
  + Preload library has been moved to $libdir from $libexecdir.
  + Build system improvements.
  + Fix whole-system capture setting when using "Record Again"
    feature.
  + New SysprofCollector interfaces for samples, marks, and logs to
    sit alongside allocations.
  + Updated translations.
- Enable meson_test in check section again.

-------------------------------------------------------------------
Fri Feb 28 22:56:23 UTC 2020 - bjorn.lie@gmail.com

- Update to version 3.35.92:
  + A new profiler for tracking memory allocations within processes
    that are spawned by Sysprof. Select "Track Allocations" to use
    an LD_PRELOAD module which monitors allocations within the
    target process. A callgraph is provided with allocations and
    their stack traces. This brings the main features of the
    venerable "Memprof" into Sysprof.
  + Port Sysprof to work on RHEL 7.
  + Improvements to marks and counters displays.
  + A new data frame in libsysprof-capture for memory allocations
    with stack traces.
  + sysprof-cli --merge can now be used to merge multiple syscap
    files together.
  + sysprof <program-path> can now be used to open Sysprof directly
    to a program to execute.
  + Sysprof now builds with additional security protections and
    optimization flags such as -Bsymbolic, -Wl,-z,defs, -Wl,-z,now,
    and -Wl,-z,relro.
  + Fixes for macOS
  + The ELF symbol resolver is faster.
  + sysprof-cli now properly adds supplemental information to the
    capture file such as decoded symbols. This makes sharing syscap
    files across machines easier and more reliable.
  + A new mmap()'d ring buffer can be shared between processes for
    faster data transfer to sysprof. We expect more features around
    this SysprofCollector interface in future releases.
  + A new set if icons has been provided for the collectors
  + Updated translations.
- Add pkgconfig(libunwind-generic) BuildRequires: New optional
  dependency.

-------------------------------------------------------------------
Wed Feb 12 20:49:00 CET 2020 - dimstar@opensuse.org

- Update to version 3.35.3:
  + Build and test fixes.
  + Track changes to gdbus-codegen.
  + Include additional pkg-config variable for access to D-Bus
    data.
  + Updated translations.
- Drop sysprof-libsysprof-ui-avoid-use-of-env.patch: fixed
  upstream.

-------------------------------------------------------------------
Tue Nov 19 12:12:49 UTC 2019 - Bjørn Lie <bjorn.lie@gmail.com>

- Add sysprof-libsysprof-ui-avoid-use-of-env.patch: libsysprof-ui:
  avoid use of environ for -Werror=shadow.

-------------------------------------------------------------------
Mon Oct  7 20:19:14 UTC 2019 - Bjørn Lie <bjorn.lie@gmail.com>

- Update to version 3.34.1:
  + Process search fixes.

-------------------------------------------------------------------
Tue Sep 10 06:14:12 CDT 2019 - mgorse@suse.com

- Update to version 3.34.0:
  + Help fixes.
  + Build and test fixes.
  + Updated translations.
- Require meson >= 0.50.0.

-------------------------------------------------------------------
Thu Sep  5 20:13:37 NZST 2019 - luke@ljones.dev

- Update to version 3.33.92:
  + Check for GLib 2.61.3 or newer for features used when removing
    deprecated API usage from GLib.
  + Require meson 0.50
  + Updated translations.

-------------------------------------------------------------------
Thu Aug 15 09:33:26 CEST 2019 - bjorn.lie@gmail.com

- Update to version 3.33.90:
  + Tweak CSS styling a bit.
  + Hide RAPL rows if no counters were discovered.
  + Fix --no-battery in sysprof-cli.
  + Improve parsing of /proc/mounts and /proc/pid/mountinfo.
  + Improve support for using libsysprof from Flatpak.
  + Symbol directories are removed from public ABI and consumers
    should now add directories to SysprofElfSymbolResolver
    directly.
- Changes from version 3.33.4:
  + Build system fixes and improved backports.
  + New network, disk, battery, and energy sources.
  + Additional options for sysprof-cli including "syprof-cli
    --command" support.
  + i18n is now properly initialized at startup.
  + Improved styling.
  + A new "duplex" graph type for read/write type visuals.
- Changes from version 3.33.3:
  + This release of Sysprof contains the largest amount of work
    since we revived the project a few years ago. We have revamped
    and minimized the ABI of libsysprof-ui considerably. As port of
    the ABI update, symbols have been changed from Sp to Sysprof to
    reduce chances of collisions.
  + With these changes comes a soname bump and upgrade of the D-Bus
    API provided by the package. Some work has been taken to
    implement older versions of the sysprofd API so that older
    clients may continue to work.
  + Various build system improvements.
  + A new libsysprof-capture-3.a capture library has been provided
    to allow application developers to capture data and merge into
    the profiler stream.
  + The recording and viewing API has been redesigned and improved.
  + Sysprof has been backported to support GTK 3.22 and GLib 2.50.
  + Support for passing file-descriptors has been added to allow
    profiling integration with GNOME Shell, GJS, and GTK profilers.
  + New API to allow integration with external tooling that spawns
    applications is provided so that environment and arguments may
    be modified.
  + A new re-record feature has been added.
  + The capture format can now store log messages, metadata, and
    file content.
  + A new org.gnome.Sysprof3 daemon has been added, which is
    written using GDBus instead of libsystemd. This may improve
    portability to some distributions.
  + The portability of Sysprof to non-Linux systems has been
    improved, however many recording features are still not
    available on non-Linux platforms.
  + A new org.gnome.Sysprof3.Profiler XML D-Bus interface is
    provided for tooling that wants to export Sysprof content via
    D-Bus.
  + Various new capture sources have been provided.
  + sysprofd also implements a compat org.gnome.Sysprof2 D-Bus
    end-point for older clients.
  + Updated translations.
- Add pkgconfig(glib-2.0) and pkgconfig(libdazzle-1.0)
  BuildRequires: New dependencies.

-------------------------------------------------------------------
Fri Aug  2 08:23:32 UTC 2019 - Martin Liška <mliska@suse.cz>

- Use FAT LTO objects in order to provide proper static library.

-------------------------------------------------------------------
Mon May 13 07:46:35 UTC 2019 - Dominique Leuenberger <dimstar@opensuse.org>

- Replace systemd-gtk BuildRequires with pkgconfig(systemd): make
  the build cheaper by not having to wait for the 'real' systemd
  package to have built, but allow to use systemd-mini. The change
  in the stack causing this was polkit dropping its hard dep on
  systemd.

-------------------------------------------------------------------
Sat May 11 20:12:28 UTC 2019 - Bjørn Lie <bjorn.lie@gmail.com>

- Add systemd-gtk BuildRequires: Now needed due to changes
  elsewhere in the stack.

-------------------------------------------------------------------
Wed Mar 13 08:33:13 UTC 2019 - Bjørn Lie <bjorn.lie@gmail.com>

- Update to version 3.32.0:
  + Updated translations.

-------------------------------------------------------------------
Tue Feb 19 22:24:12 UTC 2019 - bjorn.lie@gmail.com

- Update to version 3.31.91:
  + Handle cancellation during profiler startup more gracefully.

-------------------------------------------------------------------
Thu Feb 14 22:32:17 UTC 2019 - bjorn.lie@gmail.com

- Update to version 3.31.90:
  + A new memory source for basic memory statistics. This is meant
    for overview only, not precise details.
  + Fix a small memory leak.
  + Allow disabling memory/cpu sources from sysprof-cli.
  + CSS styling updates.
  + Updated icon for GNOME 3.32 icon initiative.
  + The line visualizer can auto-discover Y axis range.
  + Build system improvements.
  + Updated translations.
- Update URL to https://wiki.gnome.org/Apps/Sysprof.

-------------------------------------------------------------------
Thu Jan  3 22:00:14 UTC 2019 - bjorn.lie@gmail.com

- Update to version 3.31.1:
  + Remove app-menu in favor of window-menu, following GNOME design
    guidelines for 3.32.
  + Fixes for i18n/l10n.
  + Build fixes to avoid use of non-portable __WORDSIZE.
  + Updated translations.
- Add meson_test macro in check section, run tests.

-------------------------------------------------------------------
Fri Nov  2 18:24:25 UTC 2018 - bjorn.lie@gmail.com

- Update to version 3.30.2:
  + The profiler menu button more properly handles changes to the
    toplevel window.
  + Fixes for i18n/l10n intialization.

-------------------------------------------------------------------
Wed Sep 26 12:03:56 UTC 2018 - bjorn.lie@gmail.com

- Update to version 3.30.1:
  + Various compilation fixes for alternative platforms, compilers,
    and toolchains.
  + Backport model filter fixes from libdazzle.
  + Updated translations.

-------------------------------------------------------------------
Wed Sep 12 20:17:04 UTC 2018 - antoine.belvire@opensuse.org

- Update to version 3.30.0:
  + Fix incorrect free func for GPtrArray.
  + Various build system improvements.
  + ENABLE_POLKIT is now repsected in the kallsyms support.
  + suggested-action fixes for CSS.
  + Updated translations.
- Drop sysprof-fix-incorrect-free-func.patch (fixed upstream).
- Remove lang package from recommended packages as it already
  supplements main package (same effect, it's redundant).

-------------------------------------------------------------------
Thu Jun 21 00:42:44 UTC 2018 - luc14n0@linuxmail.org

- Update to version 3.29.3:
  + Experimental tracking of various drm events (vblank and
    CRTC/MSC).
  + Drawing code now uses double instead of float for additional
    precision.
  + CLOCK_MONOTONIC is now the preferred clock.
  + A new capture type of "mark" has been added to the capture
    format. It supports an event name with a duration >= 0.
  + Various counters are automatically detected and displayed when
    opening a capture file.
  + A new in-tree tool "sysprof-cat" which can join multiple
    capture files together gracefully.
  + Fix a number of situations where cpu/pid were crossed.
  + Updated translations.
- Drop explicit gettext BuildRequires: meson doesn't look for it.

-------------------------------------------------------------------
Wed Apr 11 03:58:02 UTC 2018 - luc14n0@linuxmail.org

- Update to version 3.28.1:
  + Updated translations.
- Add hicolor-icon-theme BuildRequires for directory ownership
  purposes.
- Drop:
  + binutils-devel, gobject-introspection-devel and libxml2-tools
    BuildRequires packages: they are no longer required nor used
    anymore.
  + desktop_database_* and icon_theme_cache_* post/postun
    scriptlets: their functionality has been moved to RPM file
    triggers (version 4.13, suse_version 1330).

-------------------------------------------------------------------
Wed Mar 14 10:54:30 UTC 2018 - dimstar@opensuse.org

- Update to version 3.28.0:
  + SpCaptureWriter now pre-initializes the scratch buffer to zero
    to pacify valgrind.
  + Updated translations.

-------------------------------------------------------------------
Mon Mar  5 19:28:10 UTC 2018 - dimstar@opensuse.org

- Update to version 3.27.92:
  + Updated translations.
- Drop sysprof-drop-debug-code.patch: fixed upstream.

-------------------------------------------------------------------
Wed Feb 28 16:39:53 UTC 2018 - dimstar@opensuse.org

- Modernize spec-file by calling spec-cleaner

-------------------------------------------------------------------
Wed Feb 21 16:50:12 UTC 2018 - dimstar@opensuse.org

- Update to version 3.27.91:
  + Sysprof now exclusively uses the meson build system.
  + Improvements to the process filtering model used in the
    profiler popover.
  + More tests for utility helpers.
  + Some code was relicensed to LGPLv2.1+ to extend it's
    reusability.
  + A new libsysprof-capture-2.a static library is installed to
    allow external tooling to read and write the capture format.
    We expect tooling such as GJS to use this in the future.
  + A new kallsyms tokenizer was added.
  + If we fail to parse kallsyms as the effective user, we now
    query the sysprofd daemon to access and parse those symbols for
    us as root. This requires polkit authorization on the DBus
    connection just like our elevated perf_event_open() helper.
  + Now that we have to deal with overlapping kernel and user-space
    memory, Sysprof tries harder to check the current perf context.
    New API was added to symbol resolvers to facilitate this.
- Split out new subpackage sysprof-capture-devel-static.
- Use meson build system, following upstreams change:
  + Add meson BuildRequires.
  + Use meson/meson_build/meson_install instead of
    configure/make/make_install.
- Add sysprof-drop-debug-code.patch: Fix build on i586 by removing
  lingering debug code.

-------------------------------------------------------------------
Thu Oct  5 13:17:45 UTC 2017 - dimstar@opensuse.org

- Update to version 3.26.1:
  + Add missing FAQ to help documentation.

-------------------------------------------------------------------
Sat Sep 16 14:43:18 UTC 2017 - zaitor@opensuse.org

- Update to version 3.26.0:
  + No changes, stable bump only.

-------------------------------------------------------------------
Wed Sep  6 23:29:09 UTC 2017 - dimstar@opensuse.org

- Update to version 3.25.92:
  + Additional help topics.
  + Fix for a potential divide-by-zero when generating callgraphs.
  + Alignment attributes to allow building on armel/armhf/mipsel.
  + About dialog is now modal.
  + Each new Sysprof window gets its own window group.
  + Updated translations.

-------------------------------------------------------------------
Tue Apr 25 11:05:58 UTC 2017 - dimstar@opensuse.org

- Update to version 3.24.1:
  + About dialog is now modal to it's direct parent only.
  + Updated translations.

-------------------------------------------------------------------
Mon Mar 20 09:17:36 UTC 2017 - zaitor@opensuse.org

- Update to version 3.24.0:
  + Use gtk_show_uri_on_window() to improve help window placement
    on Wayland.
  + Updated translations.

-------------------------------------------------------------------
Tue Mar 14 13:27:51 UTC 2017 - dimstar@opensuse.org

- Update to version 3.23.92:
  + Compilation fix for 32-bit ARM.
  + Updated translations.

-------------------------------------------------------------------
Wed Mar  1 13:26:35 UTC 2017 - zaitor@opensuse.org

- Update to version 3.23.91:
  + Various compilation checks based on results from ARM builders.
  + Support for setting symbol directories to affect symbol
    resolution.
  + Translate paths from /newroot/ into the host filesystem when
    possible. This should allow some degree of profiling
    container-based applications and still resolve symbols on the
    host. Builder does this now when profiling Flatpak-based
    applications.
  + Updated translations.

-------------------------------------------------------------------
Sat Feb 18 17:53:31 UTC 2017 - dimstar@opensuse.org

- Update to version 3.23.90:
  + Build system support for Meson.
  + Appdata support has been added.
  + Desktop file can be translated.
  + Updated translations.

-------------------------------------------------------------------
Wed Nov 30 10:24:02 UTC 2016 - zaitor@opensuse.org

- Update to version 3.22.3:
  + Fix expansion of function text in callgraph.
  + Fix jump-to-function in callees list.
  + Disable record button while generating callgraph profile.
  + Ensure sysprof icons are available form libsysprof-ui-2.
  + sysprof-cli requires --force to overwrite previous capture.
  + Reduce code duplication in capture frame initialization.
  + Updated translations.

-------------------------------------------------------------------
Wed Nov  2 18:18:28 UTC 2016 - zaitor@opensuse.org

- Update to version 3.22.2:
  + ftruncate() when creating a capture in case we overwrite a
    previous capture file. This ensures we don't leave junk at the
    end of the capture.
  + Handle EAGAIN when writing the opportunistic end-time header.
  + Capture cursor should short circuit when no read delegate was
    found.
  + Updated translations.

-------------------------------------------------------------------
Sat Oct 15 11:27:09 UTC 2016 - zaitor@opensuse.org

- Update to version 3.22.1:
  + A new data source called "hostinfo" has been added. It records
    various CPU datapoints as "counters" in the sysprof capture.
  + A new visualizers abstraction has been added so that we can
    start providing new ways to look at profiler data.
  + A CPU visualizer has been added which renders datapoints
    recorded from the hostinfo data source.
  + Selecting ranges of the visualizer will update the callgraph
    limiting stacktrace samples, to the given time range.
  + Missing headers are now installed which may be needed by
    applications using libsysprof-ui.
  + A keyboard shortcuts dialog has been added.
  + A theme manager has been added to allow us to provide custom
    CSS for various themes. Currently, we have additional styling
    added for Adwaita and Adwaita-dark.
  + Updated translations.
- Pass --disable-Werror to configure, we do not need to handle
  warnings as errors.

-------------------------------------------------------------------
Mon Sep 19 22:26:05 UTC 2016 - zaitor@opensuse.org

- Update to version 3.22.0:
  + Updated translations.

-------------------------------------------------------------------
Fri Sep  2 08:40:22 UTC 2016 - zaitor@opensuse.org

- Update to version 3.21.91:
  + Allow collapsing the callgraph tree with Left arrow.
  + Add a full barrier before writing data_tail as suggested by
    Perf kernel documentation.
  + Support for additional architectures has been enabled by the
    use of C11 atomics.
  + Require -std=gnu11 for stdatomic.h.
  + Correct gettext domain for translation files.
- Drop sysprof-correct-gettext-domain.patch: Fixed upstream.

-------------------------------------------------------------------
Sat Aug 27 21:56:42 UTC 2016 - zaitor@opensuse.org

- Initial package, version 3.21.90.

