-------------------------------------------------------------------
Fri Oct 15 17:54:17 UTC 2021 - bjorn.lie@gmail.com

- Update to version 3.2.0:
  * This release includes compression efficiency and perceptual
    quality improvements, speedup and memory optimizations, as well
    as some new features.
  * New Features:
    + Introduced speeds 7, 8, and 9 for all intra mode.
    + Introduced speed 10 for real time mode.
    + Introduced an API that allows external partition decisions.
    + SVC: added support for compound prediction.
    + SVC: added support for fixed SVC modes.
  * Compression Efficiency Improvements:
    + Intra-mode search improvement.
    + Improved real time (RT) mode BDrate savings by ~5% (RT speed
      5) and ~12% (RT speed 6). The improvement was measured on the
      video conference set.
    + Improved real time mode for nonrd path (speed 7, 8, 9):
      BDrate gains of ~3-5%.
    + Rate control and RD adjustments based on ML research in VP9.
      Gains of ~0.5-1.0% for HD.
  * Perceptual Quality Improvements:
    + Added a new mode --deltaq-mode=3 to improve perceptual
      quality based on a differential contrast model for still
      images.
    + Added a new mode –deltaq-mode=4 to improve perceptual quality
      based on user rated cq_level data set for still images.
    + Weighting of some intra mode and partition size choices to
      better manage and retain texture.
  * Speedup and Memory Optimizations:
    + Further improved 2-pass good quality encoder speed:
      . Speed 2 speedup: 18%
      . Speed 3 speedup: 22%
      . Speed 4 speedup: 37%
      . Speed 5 speedup: 30%
      . Speed 6 speedup: 20%
    + Optimized the real time encoder (measured on the video
      conference set):
      . RT speed 5 speedup: 110%
      . RT speed 6 speedup: 77%
  * Bug Fixes
    + Issue 3069: Fix one-pass mode keyframe placement off-by-one
      error.
    + Issue 3156: Fix a bug in av1_quantize_lp AVX2 optimization.
- Drop libaom-CVE-2021-30474.patch: Fixed upstream.
- Drop libaom-CVE-2021-30475.patch: Fixed upstream.
- Remove changesgenerate option from _service file, use data from
  CHANGELOG to fill .changes instead as it it much nicer and human
  readable than just a list of commit messages.

-------------------------------------------------------------------
Wed Oct 13 18:20:02 UTC 2021 - Callum Farmer <gmbr3@opensuse.org>

- fix service data

-------------------------------------------------------------------
Fri Oct 08 11:01:26 UTC 2021 - gmbr3@opensuse.org

- Update to version 3.1.3:
  * Update CHANGELOG for v3.1.3-rc2
  * Detect chroma subsampling more directly
  * Detect chroma subsampling more directly
  * image2yuvconfig() should calculate uv_crop_width
  * aom/aom_encoder.h: remove configure option reference
  * aom_encoder.h: fix rc_overshoot_pct range
  * Update AUTHORS,CHANGELOG,CMakeLists.txt for v3.1.3
  * aom_install: don't exclude msvc from install
  * aom_install: use relpath for install
  * aom_install: Install lib dlls to bindir

-------------------------------------------------------------------
Thu Sep  9 13:47:19 UTC 2021 - pgajdos@suse.com

- security update
- added patches
  fix CVE-2021-30474 [bsc#1186799], aom_dsp/grain_table.c in libaom in AOMedia before 2021-03-30 has a use-after-free.
  + libaom-CVE-2021-30474.patch

-------------------------------------------------------------------
Wed Aug 25 09:13:44 UTC 2021 - pgajdos@suse.com

- security update
- added patches
  fix CVE-2021-30475 [bsc#1189497], Buffer overflow in aom_dsp/noise_model.c
  + libaom-CVE-2021-30475.patch

-------------------------------------------------------------------
Wed Aug 25 08:32:17 UTC 2021 - pgajdos@suse.com

- Update to version 3.1.2:
  * Update AUTHORS,CHANGELOG,CMakeLists.txt for v3.1.2
  * Avoid chroma resampling for 420mpeg2 input
  * Check array has two elements before using index 1
  * Fix DecodeScalabilityTest failure in realtime only
  * Store temporal_id and spatial_id of decoded frame
  * exports.cmake: use APPLE and WIN32 and use def for mingw-w64

-------------------------------------------------------------------
Fri Jul  2 18:39:38 UTC 2021 - Ferdinand Thiessen <rpm@fthiessen.de>

- Update to version 3.1.1
  * Fix vmaf model initialization error when not set to tune=vmaf
  * Fix consistent crash on near-static screen content, keyframe
    related
  * Fix tune=butteraugli mode
- Disable NEON on arm where not available.

-------------------------------------------------------------------
Mon May 10 14:00:23 UTC 2021 - daniel.molkentin@suse.com

- Update to version 3.1.0 (bsc#1185843):
  https://aomedia.googlesource.com/aom/+/refs/tags/v3.1.0

  This release adds an "all intra" mode to the encoder, which significantly
  speeds up the encoding of AVIF still images at speed 6.

  - Upgrading:
    All intra mode for encoding AVIF still images and AV1 all intra videos:
    AOM_USAGE_ALL_INTRA (2) can be passed as the 'usage' argument to
    aom_codec_enc_config_default().

    New encoder control IDs added:
      - AV1E_SET_ENABLE_DIAGONAL_INTRA: Enable diagonal (D45 to D203) intra
        prediction modes (0: false, 1: true (default)). Also available as
        "enable-diagonal-intra" for the aom_codec_set_option() function.

    New aom_tune_metric enum value: AOM_TUNE_BUTTERAUGLI. The new aomenc option
    --tune=butteraugli was added to optimize the encoder’s perceptual quality by
    optimizing the Butteraugli metric. Install libjxl (JPEG XL) and then pass
    -DCONFIG_TUNE_BUTTERAUGLI=1 to the cmake command to enable it.


- Includes 3.0
  https://aomedia.googlesource.com/aom/+/refs/tags/v3.0.0
This release includes compression efficiency improvement, speed improvement
for realtime mode, as well as some new APIs.

  - Upgrading:

    Support for PSNR calculation based on stream bit-depth.

    New encoder control IDs added:
      - AV1E_SET_ENABLE_RECT_TX
      - AV1E_SET_VBR_CORPUS_COMPLEXITY_LAP
      - AV1E_GET_BASELINE_GF_INTERVAL
      - AV1E_SET_ENABLE_DNL_DENOISING

    New decoder control IDs added:
      - AOMD_GET_FWD_KF_PRESENT
      - AOMD_GET_FRAME_FLAGS
      - AOMD_GET_ALTREF_PRESENT
      - AOMD_GET_TILE_INFO
      - AOMD_GET_SCREEN_CONTENT_TOOLS_INFO
      - AOMD_GET_STILL_PICTURE
      - AOMD_GET_SB_SIZE
      - AOMD_GET_SHOW_EXISTING_FRAME_FLAG
      - AOMD_GET_S_FRAME_INFO

    New aom_tune_content enum value: AOM_CONTENT_FILM

    New aom_tune_metric enum value: AOM_TUNE_VMAF_NEG_MAX_GAIN

    Coefficient and mode update can be turned off via
    AV1E_SET_{COEFF/MODE}_COST_UPD_FREQ.

    New key & value API added, available with aom_codec_set_option() function.
    Scaling API expanded to include 1/4, 3/4 and 1/8.

  - Enhancements:

    Better multithreading performance with realtime mode.
    New speed 9 setting for faster realtime encoding.
    Smaller binary size with low bitdepth and realtime only build.
    Temporal denoiser and its optimizations on x86 and Neon.
    Optimizations for scaling.
    Faster encoding with speed settings 2 to 6 for good encoding mode.
    Improved documentation throughout the library, with function level
    documentation, tree view and support for the dot tool.

  - Bug fixes:
    Aside from those mentioned in v2.0.1 and v2.0.2, this release includes the
    following bug fixes:

    Issue 2940: Segfault when encoding with --use-16bit-internal and --limit > 1
    Issue 2941: Decoder mismatch with --rt --bit-depth=10 and --cpu-used=8
    Issue 2895: mingw-w64 i686 gcc fails to build
    Issue 2874: Separate ssse3 functions from sse2 file.

-------------------------------------------------------------------
Mon Mar 01 20:25:43 UTC 2021 - elimat@opensuse.org

- Update to version 2.0.2:
  * Prepare for the libaom v2.0.2 release
  * Call av1_setup_frame_size() when dropping a frame
  * Avoid memset in filter_intra_predictor module
  * Fix a typo bug in apply_temporal_filter_planewise
  * Modify the assertion in temporal filter intrinsics
  * Fix unit test ThreadTestLarge.EncoderResultTest/49
  * Add -Wimplicit-function-declaration as C flag only
  * Update CHANGELOG for libaom v2.0.1
  * Set allow_screen_content_tools to 0 in rt mode
  * chroma_check: don't access UV planes if monochrome

-------------------------------------------------------------------
Tue May 19 12:32:12 UTC 2020 - daniel.molkentin@suse.com

- Update to version 2.0.0 "Applejack":
  
  This is the first official release of libaom. It has real-time
  mode and SVC support, as well as substantial documentation
  improvement and clean-ups.

  Important changes:
  * Decouple library version and so version.
  * Move functions into a header file
  * Remove av1_ prefix from static functions
  * Remove aom_ prefix from static functions
  * Remove aom_ prefix for static functions
  * Remove av1_ prefix for a static function
- Drop libaom-0002-link-threading-lib-with-shared-library.patch and
  libaom-0003-update-CHANGELOG.patch

-------------------------------------------------------------------
Tue Jul 31 21:29:49 UTC 2018 - 9+suse@cirno.systems

- Enable CONFIG_LOWBITDEPTH. Makes 8-bit decoding faster. The only
  reason this confusingly-named option is not enabled by default
  is that its behavior might not match the 16-bit reference code
  path, and that hardware vendors want to be able to validate
  their designs against it. No actual mismatches have been found.
  See https://bugs.chromium.org/p/aomedia/issues/detail?id=2062

-------------------------------------------------------------------
Mon Jul 30 13:09:23 UTC 2018 - bjorn.lie@gmail.com

- Pass conditional AOM_TARGET_CPU to cmake, fix build for arm and
  ppc, as well as optimize target cpu for other arches.

-------------------------------------------------------------------
Fri Jul 27 14:42:25 UTC 2018 - 9+suse@cirno.systems

- Init, v1.0.0
