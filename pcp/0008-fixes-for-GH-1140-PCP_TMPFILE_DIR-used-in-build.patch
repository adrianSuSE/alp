From adb769a8a9a56659774bc127a0bb7bc3527e4d25 Mon Sep 17 00:00:00 2001
From: Ken McDonell <kenj@kenj.id.au>
Date: Fri, 29 Jan 2021 11:18:18 +1100
Subject: [PATCH] fixes for GH #1140 - PCP_TMPFILE_DIR used in build

$PCP_TMPFILE_DIR does double duty for both build time and run time
(some scripts are used in the build AND installed in the packages).

As outlined in https://github.com/performancecopilot/pcp/issues/1140
if, via configure options, $PCP_TMPFILE_DIR is set to something that
will be a directory after package installation, but does not exist
on the build machine, the build would fail.

Change the files below to check if $PCP_TMPFILE_DIR is set and is
the name of an existing directory before using it, otherwise fallback
to /tmp.

modified:   src/libpcp/src/mk.exports
modified:   src/libpcp/src/mk.pmdbg
modified:   src/pmieconf/xtractnames
modified:   src/pmns/Make.stdpmid
---
 src/libpcp/src/mk.exports | 10 +++++++++-
 src/libpcp/src/mk.pmdbg   |  9 ++++++++-
 src/pmieconf/xtractnames  | 10 +++++++++-
 src/pmns/Make.stdpmid     | 10 +++++++++-
 4 files changed, 35 insertions(+), 4 deletions(-)

diff --git a/src/libpcp/src/mk.exports b/src/libpcp/src/mk.exports
index 2f4fdfefca..a22c15c2c8 100755
--- a/src/libpcp/src/mk.exports
+++ b/src/libpcp/src/mk.exports
@@ -13,7 +13,15 @@ fi
 
 . ../../include/pcp.conf
 
-tmp=`mktemp -d "$PCP_TMPFILE_DIR/pcp-build-mk.exports.XXXXXXXXX"` || exit 1
+if [ -n "$PCP_TMPFILE_DIR" -a -d "$PCP_TMPFILE_DIR" ]
+then
+    tmp=`mktemp -d "$PCP_TMPFILE_DIR/pcp-build-mk.exports.XXXXXXXXX"` || exit 1
+else
+    # assume run during a build and /tmp is a safe bet
+    #
+    tmp=`mktemp -d "/tmp/pcp-build-mk.exports.XXXXXXXXX"` || exit 1
+fi
+
 sts=0
 trap "rm -rf $tmp; exit \$sts" 0 1 2 3 15
 
diff --git a/src/libpcp/src/mk.pmdbg b/src/libpcp/src/mk.pmdbg
index 174fd3f515..44e88ef92d 100755
--- a/src/libpcp/src/mk.pmdbg
+++ b/src/libpcp/src/mk.pmdbg
@@ -45,7 +45,14 @@ then
     exit 1
 fi
 
-tmp=`mktemp -d "$PCP_TMPFILE_DIR/pcp-build-mk.pmdbg.XXXXXXXXX"` || exit 1
+if [ -n "$PCP_TMPFILE_DIR" -a -d "$PCP_TMPFILE_DIR" ]
+then
+    tmp=`mktemp -d "$PCP_TMPFILE_DIR/pcp-build-mk.pmdbg.XXXXXXXXX"` || exit 1
+else
+    # assume run during a build and /tmp is a safe bet
+    #
+    tmp=`mktemp -d "/tmp/pcp-build-mk.pmdbg.XXXXXXXXX"` || exit 1
+fi
 trap "rm -rf $tmp; exit 0" 0 1 2 3 15
 
 rm -f pmdbg.h
diff --git a/src/pmieconf/xtractnames b/src/pmieconf/xtractnames
index 6178f362a3..b5986b5f86 100755
--- a/src/pmieconf/xtractnames
+++ b/src/pmieconf/xtractnames
@@ -29,7 +29,15 @@ do
     fi
 done
 
-tmp=`mktemp -d "$PCP_TMPFILE_DIR/pmieconf-xtract.XXXXXXXXX"` || exit 1
+if [ -n "$PCP_TMPFILE_DIR" -a -d "$PCP_TMPFILE_DIR" ]
+then
+    tmp=`mktemp -d "$PCP_TMPFILE_DIR/pmieconf-xtract.XXXXXXXXX"` || exit 1
+else
+    # assume run during a build and /tmp is a safe bet
+    #
+    tmp=`mktemp -d "/tmp/pmieconf-xtract.XXXXXXXXX"` || exit 1
+fi
+
 trap "rm -rf $tmp; exit" 0 1 2 3 15
 
 _usage()
diff --git a/src/pmns/Make.stdpmid b/src/pmns/Make.stdpmid
index aa6cd74275..4d06929c84 100755
--- a/src/pmns/Make.stdpmid
+++ b/src/pmns/Make.stdpmid
@@ -16,7 +16,15 @@
 # source the PCP configuration environment variables
 . $PCP_DIR/etc/pcp.env
 
-tmp=`mktemp -d "$PCP_TMPFILE_DIR/pmns_stdpmid.XXXXXXXXX"` || exit 1
+if [ -n "$PCP_TMPFILE_DIR" -a -d "$PCP_TMPFILE_DIR" ]
+then
+    tmp=`mktemp -d "$PCP_TMPFILE_DIR/pmns_stdpmid.XXXXXXXXX"` || exit 1
+else
+    # assume run during a build and /tmp is a safe bet
+    #
+    tmp=`mktemp -d "/tmp/pmms_stdpmid.XXXXXXXXX"` || exit 1
+fi
+
 status=1
 trap "rm -rf $tmp; exit \$status" 0 1 2 3 15
 
