From: Bruce Rogers <brogers@suse.com>
Date: Sat, 19 Nov 2016 08:06:30 -0700
Subject: roms/Makefile: pass a packaging timestamp to subpackages with date
 info

References: bsc#1011213

Certain rom subpackages build from qemu git-submodules call the date
program to include date information in the packaged binaries. This
causes repeated builds of the package to be different, wkere the only
real difference is due to the fact that time build timestamp has
changed. To promote reproducible builds and avoid customers being
prompted to update packages needlessly, we'll use the timestamp of the
VERSION file as the packaging timestamp for all packages that build in a
timestamp for whatever reason.

Signed-off-by: Bruce Rogers <brogers@suse.com>
---
 roms/Makefile | 14 ++++++++++++--
 1 file changed, 12 insertions(+), 2 deletions(-)

diff --git a/roms/Makefile b/roms/Makefile
index b967b53bb76ee8a94fc9b37e4460..66d06f5831303c3d41e943290389 100644
--- a/roms/Makefile
+++ b/roms/Makefile
@@ -52,6 +52,12 @@ SEABIOS_EXTRAVERSION="-prebuilt.qemu.org"
 #
 EDK2_EFIROM = edk2/BaseTools/Source/C/bin/EfiRom
 
+# NB: Certain SUSE qemu subpackages use date information, but we want
+#  reproducible builds, so we use a pre-determined timestamp, rather
+#  than the current timestamp to acheive consistent results build to
+#  build.
+PACKAGING_TIMESTAMP = $(shell date -r ../VERSION +%s)
+
 default help:
 	@echo "nothing is build by default"
 	@echo "available build targets:"
@@ -104,7 +110,7 @@ build-seabios-config-%: config.%
 
 .PHONY: sgabios skiboot qboot
 sgabios:
-	$(MAKE) -C sgabios
+	$(MAKE) -C sgabios PACKAGING_TIMESTAMP=$(PACKAGING_TIMESTAMP)
 	cp sgabios/sgabios.bin ../pc-bios
 
 
@@ -123,11 +129,13 @@ efi-rom-%: build-pxe-roms build-efi-roms edk2-basetools
 
 build-pxe-roms:
 	$(MAKE) -C ipxe/src CONFIG=qemu \
+		PACKAGING_TIMESTAMP=$(PACKAGING_TIMESTAMP) \
 		CROSS_COMPILE=$(x86_64_cross_prefix) \
 		$(patsubst %,bin/%.rom,$(pxerom_targets))
 
 build-efi-roms: build-pxe-roms
 	$(MAKE) -C ipxe/src CONFIG=qemu \
+		PACKAGING_TIMESTAMP=$(PACKAGING_TIMESTAMP) \
 		CROSS_COMPILE=$(x86_64_cross_prefix) \
 		$(patsubst %,bin-x86_64-efi/%.efidrv,$(pxerom_targets))
 
@@ -151,7 +159,9 @@ edk2-basetools:
 		EXTRA_LDFLAGS='$(EDK2_BASETOOLS_LDFLAGS)'
 
 slof:
-	$(MAKE) -C SLOF CROSS=$(powerpc64_cross_prefix) qemu
+	$(MAKE) -C SLOF CROSS=$(powerpc64_cross_prefix) \
+		PACKAGING_TIMESTAMP=$(PACKAGING_TIMESTAMP) \
+		qemu
 	cp SLOF/boot_rom.bin ../pc-bios/slof.bin
 
 u-boot.e500:
