-------------------------------------------------------------------
Mon Dec 27 17:20:01 UTC 2021 - Ben Greiner <code@bnavigator.de>

- Skip python310: Not ready yet. Factory needs only the primary
  python3 flavor

-------------------------------------------------------------------
Tue Aug  3 09:42:00 UTC 2021 - Matej Cepl <mcepl@suse.com>

- Update to 1.12.0:
  - Dropped Python 3.5 support
  - Added the simplify_optional_unions config option
  - Fixed indentation of multiline strings
  - Changed formatting of None to point to the Python stdlib docs
  - Updated special dataclass handling

-------------------------------------------------------------------
Sat Apr 24 14:11:17 UTC 2021 - Matej Cepl <mcepl@suse.com>

- Remove no-net-tests.patch as adding the fixed
  python-sphinx-autodoc-typehints-system-object.inv.patch 
  makes tests requiring network working
  (gh#agronholm/sphinx-autodoc-typehints#174).

-------------------------------------------------------------------
Fri Apr 23 14:24:23 UTC 2021 - Matej Cepl <mcepl@suse.com>

- Add no-net-tests.patch which marks tests requiring network
  (gh#agronholm/sphinx-autodoc-typehints#174).

-------------------------------------------------------------------
Wed Sep 23 13:29:55 UTC 2020 - Dirk Mueller <dmueller@suse.com>

- update to 1.11.0:
  * Dropped support for Sphinx < 3.0
  * Added support for alternative parameter names (``arg``, ``argument``, ``parameter``)
  * Fixed import path for Signature (PR by Matthew Treinish)
  * Fixed ``TypeError`` when formatting a parametrized ``typing.IO`` annotation
  * Fixed data class displaying a return type in its ``__init__()`` method
- remove python-sphinx-autodoc-typehints-system-object.inv.patch (breaks the build)

-------------------------------------------------------------------
Fri Mar  6 14:50:12 UTC 2020 - pgajdos@suse.com

- version update to 1.10.3
  * Fixed ``TypeError`` (or wrong rendered class name) when an annotation is a generic class that has
    a ``name`` property
  * Fixed inner classes missing their parent class name(s) when rendered
  * Fixed ``KeyError`` when encountering mocked annotations (``autodoc_mock_imports``)
  * Rewrote the annotation formatting logic (fixes Python 3.5.2 compatibility regressions and an
    ``AttributeError`` regression introduced in v1.9.0)
  * Fixed decorator classes not being processed as classes
  * Added support for typing_extensions_
  * Added the ``typehints_document_rtype`` option (PR by Simon-Martin Schröder)
  * Fixed metaclasses as annotations causing ``TypeError``
  * Fixed rendering of ``typing.Literal``
  * Fixed OSError when generating docs for SQLAlchemy mapped classes
  * Fixed unparametrized generic classes being rendered with their type parameters
    (e.g. ``Dict[~KT, ~VT]``)
- added patches
  fix use object.inv which comes with python-doc
  + python-sphinx-autodoc-typehints-system-object.inv.patch

-------------------------------------------------------------------
Mon Sep 16 13:38:42 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Update to 1.8.0:
  * Fixed regression which caused TypeError or OSError when trying to set annotations due to PR #87
  * Fixed unintentional mangling of annotation type names
  * Added proper :py:data targets for NoReturn, ClassVar and Tuple
  * Added support for inline type comments (like (int, str) -> None) (PR by Bernát Gábor)
  * Use the native AST parser for type comment support on Python 3.8+

-------------------------------------------------------------------
Tue Sep 10 10:03:02 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Update to 1.7.0:
  * Fixed unwrapped local functions causing errors (PR by Kimiyuki Onaka)
  * Fixed AttributeError when documenting the __init__() method of a data class
  * Added support for type hint comments (PR by Markus Unterwaditzer)
  * Added flag for rendering classes with their fully qualified names (PR by Holly Becker)
- Remove merged patch sphinx21.patch

-------------------------------------------------------------------
Wed Jul  3 09:13:37 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Add patch to fix build with new sphinx:
  * sphinx21.patch

-------------------------------------------------------------------
Thu Jun  6 13:41:01 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Drop not really needed devel dependency

-------------------------------------------------------------------
Sat Mar 23 14:37:28 UTC 2019 - Sebastian Wagner <sebix+novell.com@sebix.at>

- Remove patch setup-tests.patch, we are using pytest now.

-------------------------------------------------------------------
Wed Mar 20 21:12:28 UTC 2019 - Sebastian Wagner <sebix+novell.com@sebix.at>

- initial package for version 1.6.0
