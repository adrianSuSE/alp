-------------------------------------------------------------------
Sat Dec  4 21:06:12 UTC 2021 - Ben Greiner <code@bnavigator.de>

- Update to 1.0.7
  * Fixed an issue where the wrong git executable could be used on
    Windows. (#213)
  * Fixed an issue where the Python 3.10 classifier was not
    automatically added. (#215)

-------------------------------------------------------------------
Wed Sep 22 09:47:36 UTC 2021 - Ben Greiner <code@bnavigator.de>

- Update to 1.0.6
  * Added support for more hash types gen generating hashes. (#207)
- Release 1.0.5
  * Fixed the copy of Package instances which led to file hashes
    not being available. (#193)
  * Fixed an issue where unsafe parameters could be passed to git
    commands. (#203)
  * Fixed an issue where the wrong git executable could be used on
    Windows. (#205)
- Release 1.0.4
  * Fixed an error in the way python markers with a precision >= 3
    were handled. (#180)
  * Fixed an error in the evaluation of in/not in markers (#189)

-------------------------------------------------------------------
Wed Apr 14 21:43:18 UTC 2021 - Ben Greiner <code@bnavigator.de>

- Update to 1.0.3
  * Fixed an error when handling single-digit Python markers (#156)
  * Fixed dependency markers not being properly copied when changing the constraint (#163).
- Release 1.0.2
  * Fixed a missing import causing an error in Poetry (#134).
- Release 1.0.1
  * Fixed PEP 508 representation of dependency without extras
    (#102).
  * Fixed an error where development dependencies were being
    resolved when invoking the PEP-517 backend (#101).
  * Fixed source distribution not being deterministic (#105).
  * Fixed an error where zip files were left open when building
    wheels (#122).
  * Fixed an error where explicitly included files were still not
    present in final distributions (#124).
  * Fixed wheel filename matching for recent architecture (#125,
    #129).
  * Fixed an error where the & character was not accepted for
    author names (#120).
  * Fixed the PEP-508 representation of some dependencies (#103).
  * Fixed the Requires-Python metadata generation (#127).
  * Fixed an error where pre-release versions were accepted in
    version constraints (#128). 

-------------------------------------------------------------------
Tue Oct 20 11:20:59 UTC 2020 - Dan Čermák <dcermak@suse.com>

Remove python2 subpackage:
python2 requires the typing package as a dependency, but we ship a too recent version of that and thus cannot build it for Leap + Python2 anymore

-------------------------------------------------------------------
Fri Oct 16 17:01:46 UTC 2020 - Benjamin Greiner <code@bnavigator.de>

- Let the pyproject_install macro do the compiling

-------------------------------------------------------------------
Sat Oct 10 20:06:27 UTC 2020 - Benjamin Greiner <code@bnavigator.de>

- unbundle vendored packages 

-------------------------------------------------------------------
Thu Oct  8 18:49:33 UTC 2020 - Benjamin Greiner <code@bnavigator.de>

- remove dephell dependency. poetry-core can build and install
  itself as a PEP517 backend (using pip as frontend)
- precompile the python files as in setuptools
  * gh#openSUSE/python-rpm-macros#37

-------------------------------------------------------------------
Mon Oct  5 13:35:26 UTC 2020 - Benjamin Greiner <code@bnavigator.de>

- Update to v1.0.0
  * first stable release. See CHANGLOG.md for changes between a6
    and release
- The released PyPI package does not have the tests. Use Github.

-------------------------------------------------------------------
Sun Jul 12 05:48:29 UTC 2020 - John Vandenberg <jayvdb@gmail.com>

- Update to v1.0.0a8

-------------------------------------------------------------------
Thu May  7 05:58:22 AM UTC 2020 - John Vandenberg <jayvdb@gmail.com>

- Initial spec for v1.0.0a6
