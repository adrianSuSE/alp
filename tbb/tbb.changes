-------------------------------------------------------------------
Fri Oct  8 08:11:54 UTC 2021 - Ben Greiner <code@bnavigator.de>

- Allow gcc11
- Add tbb-pr609-32bit-mwaitpkg.patch gh#oneapi-src/oneTBB#609
  * fixes 32-bit build with gcc11
- Only run ctest when --with test is given

-------------------------------------------------------------------
Tue Oct  5 09:41:04 UTC 2021 - Ben Greiner <code@bnavigator.de>

- Update to version 2021.4
  * Large release notes since 2020.3:
    https://software.intel.com/content/www/us/en/develop/articles/intel-oneapi-threading-building-blocks-release-notes.html
- Drop python2 package
- Drop patches:
  * disable-irml.patch -- install the library
  * optflags.patch -- build system change
  * reproducible.patch -- build system change
- Refresh cmake-remove-include-path.patch
- Add libirml subpackage for python module
- Add libtbbbind library package for NUMA support

-------------------------------------------------------------------
Fri Dec 25 11:10:11 UTC 2020 - Benjamin Greiner <code@bnavigator.de>

- Rework the building of python bindings
  * Fix egg info version
  * Remove shebang lines
  * fix lining issue by setting TBBROOT and tbbvars in install phase
  * Run python tests with irml library built (but not installed)
  * Build bindings packages for all existing python3 flavors
    gh#openSUSE/python-rpm-macros#66

-------------------------------------------------------------------
Mon Aug  3 05:52:53 UTC 2020 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2020.3
  * Changed body type concept of the flow::input_node.
    Set TBB_DEPRECATED_INPUT_NODE_BODY to 1 to compile with the previous
    concept of the body type.
  * Fixed compilation errors in C++20 mode due to ambiguity of comparison
    operators. Inspired by Barry Revzin
    (https://github.com/oneapi-src/oneTBB/pull/251).
  * Fixed an issue in TBBBuild.cmake that causes the build with no arguments
    to fail (https://github.com/oneapi-src/oneTBB/pull/233)

-------------------------------------------------------------------
Mon Mar 30 15:50:42 UTC 2020 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2020.2
  * Cross-allocator copying constructor and copy assignment operator
    for concurrent_vector are deprecated.
  * Added input_node to the flow graph API. It acts like a source_node 
    except for being inactive by default; source_node is deprecated.
  * Allocator template parameter for flow graph nodes is deprecated. Set
    TBB_DEPRECATED_FLOW_NODE_ALLOCATOR to 1 to avoid compilation errors.
  * Flow graph preview hetero-features are deprecated.
  * Fixed the task affinity mechanism to prevent unlimited memory
    consumption in case the number of threads is explicitly decreased.
  * Fixed memory leak related NUMA support functionality in task_arena.

-------------------------------------------------------------------
Tue Jan 21 15:25:51 UTC 2020 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2020.1
  * Fixed the issue of task_arena constraints not propagated on
    copy construction.
  * Fixed TBBGet.cmake script broken by TBB package name changes
    (https://github.com/intel/tbb/issues/209).

-------------------------------------------------------------------
Wed Dec 18 16:28:51 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2020.0 
  * Extended task_arena interface to simplify development of
    NUMA-aware applications.
  * Added warning notifications when the deprecated functionality is
    used.

-------------------------------------------------------------------
Thu Oct 10 12:45:32 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2019_u9
  * Multiple APIs are deprecated. For details, please see
    Deprecated Features appendix in the TBB reference manual.
  * Added C++17 deduction guides for flow graph nodes.

  Preview Features
  * Added isolated_task_group class that allows multiple threads to add 
    and execute tasks sharing the same isolation.
  * Extended the flow graph API to simplify connecting nodes.
  * Added erase() by heterogeneous keys for concurrent ordered containers.
  * Added a possibility to suspend task execution at a specific point
    and resume it later.
  
  Bugs fixed
  * Fixed the emplace() method of concurrent unordered containers to
    destroy a temporary element that was not inserted.
  * Fixed a bug in the merge() method of concurrent unordered
    containers.
  * Fixed behavior of a continue_node that follows buffering nodes.
  * Added support for move-only types to tbb::parallel_pipeline
  * Fixed detection of clang version when CUDA toolkit is installed
- Refresh patches:
  * cmake-remove-include-path.patch
  * disable-irml.patch
  * optflags.patch

-------------------------------------------------------------------
Fri Jun  7 10:44:30 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Add cmake-remove-include-path.patch to remove setting include
  path since we already install under /usr/include and this fixes
  idiot OpenCV trying to do -isystem $TBB_INCLUDE_DIR

-------------------------------------------------------------------
Thu Jun  6 12:22:31 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2019_U8
  * Fixed a bug in TBB 2019 Update 7 that could lead to incorrect memory
    reallocation on Linux (https://github.com/intel/tbb/issues/148).
  * Fixed enqueuing tbb::task into tbb::task_arena not to fail on threads
    with no task scheduler initialized
    (https://github.com/intel/tbb/issues/116).

-------------------------------------------------------------------
Tue Jun  4 14:54:09 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2019_U7
  * Added TBBMALLOC_SET_HUGE_SIZE_THRESHOLD parameter to set the
    lower bound for allocations that are not released back to OS
    unless a cleanup is explicitly requested.
  * Added zip_iterator::base() method to get the tuple of underlying
    iterators.
  * Improved async_node to never block a thread that sends a message
    through its gateway.
  * Extended decrement port of the tbb::flow::limiter_node to accept
    messages of integral types.
  * Removed the number_of_decrement_predecessors parameter from the
    constructor of flow::limiter_node. To allow its usage, set
    TBB_DEPRECATED_LIMITER_NODE_CONSTRUCTOR macro to 1.
  * Added ordered associative containers:
    concurrent_{map,multimap,set,multiset} (requires C++11).

-------------------------------------------------------------------
Tue May 14 07:53:56 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2019_U6
  * Added support for enqueuing tbb::task into tbb::task_arena
    (https://github.com/01org/tbb/issues/116).
  * Improved support for allocator propagation on concurrent_hash_map
    assigning and swapping.
  * Improved scalable_allocation_command cleanup operations to release
    more memory buffered by the calling thread.
  * Separated allocation of small and large objects into distinct memory
    regions, which helps to reduce excessive memory caching inside the
    TBB allocator.
- Disable python2 support

-------------------------------------------------------------------
Thu Apr 25 07:56:20 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Update to version 2019_U5
  * Too many changes to list, please see the included CHANGES file.
- Install TBBConfig*.cmake

-------------------------------------------------------------------
Fri Aug 24 04:10:21 UTC 2018 - bwiedemann@suse.com

- Extend reproducible.patch to not capture build kernel version (boo#1101107)

-------------------------------------------------------------------
Wed Aug  1 04:32:27 UTC 2018 - bwiedemann@suse.com

- Extend reproducible.patch to override build date (boo#1047218)

-------------------------------------------------------------------
Mon Jan  8 09:41:48 UTC 2018 - tchvatal@suse.com

- Add conditions to build with py2 and py3 respectively in order
  to allow us disable one based on codestream

-------------------------------------------------------------------
Thu Dec 21 12:20:59 UTC 2017 - idonmez@suse.com

- Add disable-irml.patch to disable linking to libirml
- Actually update to tarball to 2018_U2 release

-------------------------------------------------------------------
Sat Dec 16 15:29:08 UTC 2017 - idonmez@suse.com

- Update to version 2018_U2
  * lambda-friendly overloads for parallel_scan.
  * support of static and simple partitioners in
    parallel_deterministic_reduce.
  * initial support for Flow Graph Analyzer to do parallel_for.
  * reservation support in overwrite_node and write_once_node.
  * Fixed a potential deadlock scenario in the flow graph that
    affected Intel® TBB 2018 Initial Release.
  * Fixed constructors of concurrent_hash_map to be exception-safe.
  * Fixed auto-initialization in the main thread to be cleaned up at shutdown.
  * Fixed a crash when tbbmalloc_proxy is used together with dbghelp.
  * Fixed static_partitioner to assign tasks properly in case of nested parallelism.

-------------------------------------------------------------------
Wed Nov  1 17:31:14 UTC 2017 - mpluskal@suse.com

- Build python2 and python3 bindings
- Do not bundle python bindings with shared library

-------------------------------------------------------------------
Thu Sep 21 12:03:27 UTC 2017 - idonmez@suse.com

- Update to version 2018 release
  * Now fully supports this_task_arena::isolate() function.
  * Parallel STL, an implementation of the C++ standard library
    algorithms with support for execution policies, has been
    introduced.
  * Fixed a bug preventing use of streaming_node and opencl_node
    with Clang.
  * Fixed this_task_arena::isolate() function to work correctly
    with parallel_invoke and parallel_do algorithms.
  * Fixed a memory leak in composite_node.
  * Fixed an assertion failure in debug tbbmalloc binaries when
    TBBMALLOC_CLEAN_ALL_BUFFERS is used.

-------------------------------------------------------------------
Tue May 30 09:11:27 UTC 2017 - bwiedemann@suse.com

- Add reproducible.patch to not add build hostname+kernel to binary

-------------------------------------------------------------------
Wed May 24 12:21:12 UTC 2017 - idonmez@suse.com

- Update to version 2017_20170412 release
  * Added a blocking terminate extension to the task_scheduler_init
    class that allows an object to wait for termination of worker
    threads.

-------------------------------------------------------------------
Wed Apr 19 08:07:44 UTC 2017 - idonmez@suse.com

- Add missing include files boo#1034842

-------------------------------------------------------------------
Sun Mar  5 19:42:50 UTC 2017 - idonmez@suse.com

- Update to 2017_20170226 release
  * Added support for C++11 move semantics in parallel_do.
  * Constructors for many classes, including graph nodes, concurrent
    containers, thread-local containers, etc., are declared explicit
    and cannot be used for implicit conversions anymore.
  * Added a workaround for bug 16657 in the GNU C Library (glibc)
    affecting the debug version of tbb::mutex.
  * Fixed a crash in pool_identify() called for an object allocated in
    another thread.

-------------------------------------------------------------------
Mon Dec 12 09:04:36 UTC 2016 - idonmez@suse.com

- Update to 2017_20161128 release
  * Added template class gfx_factory to the flow graph API. It
    implements the Factory concept for streaming_node to offload
    computations to Intel processor graphics.
  * Fixed a possible deadlock caused by missed wakeup signals in
    task_arena::execute().

-------------------------------------------------------------------
Thu Nov  3 07:52:18 UTC 2016 - idonmez@suse.com

- Update to version 2017_20161004
  * Fixed the issue with task_arena::execute() not being processed
    when the calling thread cannot join the arena.

-------------------------------------------------------------------
Sat Sep 17 13:02:18 UTC 2016 - idonmez@suse.com

- Updated to version 2017_20160722
  * static_partitioner class is now a fully supported feature.
  * async_node class is now a fully supported feature.
  * For 64-bit platforms, quadrupled the worst-case limit on the amount
    of memory the Intel TBB allocator can handle.
  * Added TBB_USE_GLIBCXX_VERSION macro to specify the version of GNU
    libstdc++ when it cannot be properly recognized, e.g. when used
    with Clang on Linux* OS. Inspired by a contribution from David A.
  * Added graph/stereo example to demostrate tbb::flow::async_msg.
  * Removed a few cases of excessive user data copying in the flow graph.
  * Reworked split_node to eliminate unnecessary overheads.
  * Added support for C++11 move semantics to the argument of
    tbb::parallel_do_feeder::add() method.
  * Added C++11 move constructor and assignment operator to
    tbb::combinable template class.
  * Added tbb::this_task_arena::max_concurrency() function and
    max_concurrency() method of class task_arena returning the maximal
    number of threads that can work inside an arena.
  * Deprecated tbb::task_arena::current_thread_index() static method;
    use tbb::this_task_arena::current_thread_index() function instead.
- License changed to Apache-2.0
- Please see included CHANGES file for all changes.

-------------------------------------------------------------------
Wed Jun  8 10:50:50 UTC 2016 - idonmez@suse.com

- Update to version 44_20160526
  * Added a Python module which is able to replace Python's thread pool 
    class with the implementation based on Intel TBB task scheduler.
  * Fixed the implementation of 64-bit tbb::atomic for IA-32 architecture
    to work correctly with GCC 5.2 in C++11/14 mode.
  * Fixed a possible crash when tasks with affinity (e.g. specified via
    affinity_partitioner) are used simultaneously with task priority
    changes.

-------------------------------------------------------------------
Wed May  4 12:21:36 UTC 2016 - jengelh@inai.de

- Update group, and description of tbbmalloc.

-------------------------------------------------------------------
Wed May  4 10:53:51 UTC 2016 - idonmez@suse.com

- Update to version 44_20160128:
  * Lots of changes, see the CHANGES file.
- Drop tbb-4.0-cas.patch, fixed upstream.

-------------------------------------------------------------------
Sun Feb 17 19:42:08 UTC 2013 - asterios.dramis@gmail.com

- Update to version 41_20130116:
  * See CHANGES file for news.
- Removed tbb package which included only doc files (moved them to tbb-devel).
- Updated optflags.patch to make it apply correctly and also fix "File is
  compiled without RPM_OPT_FLAGS" rpm post build check warning.
- Added a patch "tbb-4.0-cas.patch" to fix build on PowerPC (taken from
  Fedora).

-------------------------------------------------------------------
Sun Jan 29 01:33:25 UTC 2012 - jengelh@medozas.de

- Remove redundant tags/sections per specfile guideline suggestions
- Parallel building using %_smp_mflags

-------------------------------------------------------------------
Sun Aug 14 23:35:15 UTC 2011 - crrodriguez@opensuse.org

- Update to version tbb30_20110704 

-------------------------------------------------------------------
Wed Sep 16 11:09:37 CEST 2009 - meissner@suse.de

- Reimport from Andi Kleens directory.

-------------------------------------------------------------------
Sat Sep  5 00:00:00 UTC 2009 - andi@firstfloor.org

- update to 22_20090809oss, install machine/* includes

-------------------------------------------------------------------
Thu Sep 11 00:00:00 UTC 2008 - skh@suse.de

- update to snapshot 21_20080825 (for details see CHANGES file in
  package)
- remove obsolete patch tbb-build.patch
- split off libtbb2 and libtbbmalloc2 subpackages

-------------------------------------------------------------------
Wed Aug 13 00:00:00 UTC 2008 - ro@suse.de

- add ExclusiveArch

-------------------------------------------------------------------
Mon Apr 28 00:00:00 UTC 2008 - skh@suse.de

- update to source version tbb20_20080408oss_src

-------------------------------------------------------------------
Wed Feb 13 00:00:00 UTC 2008 - dmueller@suse.de

- fix buildrequires

-------------------------------------------------------------------
Fri Feb  8 00:00:00 UTC 2008 - skh@suse.de

- initial package from version 2.0, source version
  tbb20_20080122oss_src
