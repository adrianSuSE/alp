#
# spec file for package python-pytest-xprocess
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


%define oldpython python
%define skip_python2 1
%{?!python_module:%define python_module() python-%{**} python3-%{**}}
Name:           python-pytest-xprocess
Version:        0.17.1
Release:        0
Summary:        A pytest plugin for managing processes across test runs.
License:        MIT
URL:            https://github.com/pytest-dev/pytest-xprocess
#!RemoteAsset: sha256:59c739edee7f3f2258e7c77989241698e356c552f5efb28bb46b478616888bf6
Source:         https://files.pythonhosted.org/packages/source/p/pytest-xprocess/pytest-xprocess-%{version}.tar.gz
BuildRequires:  %{python_module setuptools_scm}
BuildRequires:  python-rpm-macros
BuildRequires:  %{python_module psutil}
BuildRequires:  %{python_module pytest >= 2.8}
BuildRequires:  fdupes
Requires:       python-psutil
Requires:       python-pytest >= 2.8
BuildArch:      noarch
%python_subpackages

%description
This will provide a xprocess fixture which can be used to ensure that
external processes on which your application depends are up and running
during testing. You can also use it to start and pre-configure
test-specific databases (i.e. Postgres, Couchdb).

%prep
%setup -q -n pytest-xprocess-%{version}

%build
%python_build

%install
%python_install
%python_expand %fdupes %{buildroot}%{$python_sitelib}

%check
%pytest

%files %{python_files}
%doc README.rst
%license LICENSE
%{python_sitelib}/*

%changelog
