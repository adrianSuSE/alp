-------------------------------------------------------------------
Thu Dec 13 09:23:17 UTC 2018 - Thomas Bechtold <tbechtold@suse.com>

- update to 0.8.1:
  - Previous attempts to sanitize cassette names were incomplete.
    Sanitization has become more thorough which could have some affects on
    existing cassette files. **This may cause new cassettes to be generated.**
  - Fix bug where there may be an exception raised in a
    ``betamax.exceptions.BetamaxError`` repr.
- Use %license macro

-------------------------------------------------------------------
Tue May 16 15:57:27 UTC 2017 - dmueller@suse.com

- convert to singlespec packaging

-------------------------------------------------------------------
Tue Nov 15 10:34:38 UTC 2016 - dmueller@suse.com

- update to 0.8.0:
  - Add ``betamax_parametrized_recorder`` and ``betamax_parametrized_session``
    to our list of pytest fixtures so that users will have parametrized cassette
    names when writing parametrized tests with our fixtures. (I wonder if I can
    mention parametrization a bunch more times so I can say parametrize a lot in
    this bullet note.)
  - Add ``ValidationError`` and a set of subclasses for each possible validation
    error.
  - Raise ``InvalidOption`` on unknown cassette options rather than silently
    ignoring extra options.
  - Raise a subclass of ``ValidationError`` when a particular cassette option is
    invalid, rather than silently ignoring the validation failure.
  - Fix bug with query string matcher where query-strings without values (e.g.,
    ``?foo&bar`` as opposed to ``?foo=1&bar=2``) were treated as if there were
    no query string.
  - Fix issue #108 by effectively copying the items in the match_requests_on
    list into the match_options set on a Cassette instance

-------------------------------------------------------------------
Tue Jun  7 07:52:18 UTC 2016 - tbechtold@suse.com

- update to 0.7.0:
  - Add before_record and before_playback hooks
  - Allow per-cassette placeholders to be merged and override global placeholders
  - Fix bug where the QueryMatcher failed matching on high Unicode points
  - Add betamax_recorder pytest fixture
  - Change default behaviour to allow duplicate interactions to be recorded
    in single cassette
  - Add allow_playback_repeats to allow an interaction to be used more than once
    from a single cassette
  - Always return a new Response object from an Interaction to allow for a
    streaming response to be usable multiple times
  - Remove CI support for Pythons 2.6 and 3.2

-------------------------------------------------------------------
Thu Feb 11 14:44:26 UTC 2016 - tbechtold@suse.com

- Initial packaging (version 0.5.1)

