-------------------------------------------------------------------
Mon Jan 17 11:58:46 UTC 2022 - Martin Pluskal <mpluskal@suse.com>

- Update to version 1.6.1:
  * Creating release commit for v1.6.1
  * Destructor not returning is expected in some cases (#1316)
  * Address c4267 warning on MSVC (#1315)
  * Fix `-DBENCHMARK_ENABLE_INSTALL=OFF` (Fixes #1275) (#1305)
  * Avoid errors due to "default label in switch which covers all enumeration values" in Windows codepath (#1302)
  * update googletest to latest release tag 1.11.0 (#1301)
  * clang-tidy: readability-redundant and performance (#1298)
  * Update user_guide.md (#1296)
  * Fix broken link to Setup/Teardown section (#1291)
  * Add clang-tidy check (#1290)
  * compare.py: compute and print 'OVERALL GEOMEAN' aggregate (#1289)
  * remove long-defunct cli parameter
  * lose some build warnings
  * Disable clang-tidy (unused-using-decls) (#1287)
  * disable lint check where we know it'd fail (#1286)
  * Add Setup/Teardown option on Benchmark. (#1269)
  * Googletest renamed master to main today. (#1285)
  * Remove bazelbuild/rules_cc dependency (#1283)
  * Support for building with LLVM clang-10/clang-11 on Windows. (#1227)
  * Fix dependency typo and unpin cibuildwheel version in wheel building … (#1263)
  * check clang format on pull requests and merges (#1281)
  * format tests with clang-format (#1282)
  * clang-format Google on {src/,include/} (#1280)
  * Fix warning with MacOS (#1276)
  * Fixed typo in doc:  s/marcro/macro (#1274)
  * Fix error with Fix Werror=old-style-cast  (#1272)
  * Fix error Wshorten-64-to-32 (#1273)
  * [cleanup] Change `== ""` to `.empty()` on string to avoid clang-tidy warnings (#1271)
  * Fix un-initted error in test and fix change the API previously proposed to use std::string instead of raw char* (#1266)
  * use docker container for ubuntu-16.04 builds (#1265)
  * [RFC] Adding API for setting/getting benchmark_filter flag? (#1254)
  * Allow template arguments to be specified directly on the BENCHMARK macro (#1262)
  * Added Doxygen support. (#1228)
  * Fix -Wdeprecated-declarations warning once more. (#1256)
  * cmake: allow to use package config from build directory
  * cmake: make package config relocatable
  * GoogleTest.cmake.in: mention BENCHMARK_USE_BUNDLED_GTEST
  * GoogleTest.cmake.in: immediately error-out after failure message, Closes #1255
  * Introduce additional memory metrics (#1238)
  * Fix -Wdeprecated-declarations warning triggered by clang-cl. (#1245)
  * Cmake: options for controlling werror, disable werror for PGI compilers (#1246)
  * cmake: eliminate redundant `target_include_directories` (#1242)
  * Update policy to match reality
  * Fix mention of --benchmarks in comment (#1229)
  * Added support of packaged GTest for running unit tests. (#1226)
  * Optimized docs installation (#1225)
  * Remove unused parameter from lambda. (#1223)
  * add  to final releasing step
  * bump version to 1.6 in preparation for release
  * COnsole reporter: if statistic produces percents, format it as such (#1221)
  * Introduce Coefficient of variation aggregate (#1220)
  * Statistics: add support for percentage unit in addition to time (#1219)
  * report.py: adjust expected u-test values for tests
  * CMake: add forgotten include(FeatureSummary) into FindPFM.cmake to fix build
  * replace #warning with #pragma message  (#1216)
  * force cmake version to 3.5.1
  * [NFC] PFM: actually report package as found, and advertise description
  * Fix links to further doc in user_guide.md (#1215)
  * Introduce accessors for currently public data members (threads and thread_index) (#1208)
  * Fix a -Wunreachable-code-aggressive warning (#1214)
  * Set theme jekyll-theme-minimal
  * wrap things that look like tags but aren't with `{% raw %}`
  * install docs folder when installing library (#1212)
  * add .DS_Store to .gitignore
  * refactor the documentation to minimise `README.md` (#1211)
  * preparing v1.5.6 release
  * Set theme jekyll-theme-modernist
  * so much for googletest not failing any more
  * turn back on strict mode for googletest as it no longer breaks
  * Change the default value of `--benchmark_filter` from "." to <empty> (#1207)
  * Remove dead code from PredictNumItersNeeded (#1206)
  * downgrade warnings for googletest (#1203)
  * Add wheel and sdist building action to GH Workflow (#1180)
  * add g++ to sanitizer buildbots (#1197)
  * Don't return a reference when the callers all expect pointers.
  * fix clang-tidy warnings (#1195)
  * Fix typos (#1194)
  * Fix type warning on certain compilers (#1193)
  * Use C++11 atomic_signal_fence for ClobberMemory (#1190)
  * Bazel qnx (#1192)
  * Deduplicate test function name in python bindings example (#1189)
  * prefix VLOG (#1187)
  * prefix macros to avoid clashes (#1186)
  * Move flags inside the `benchmark` namespace (#1185)
  * Add missing trailing commas (#1182)
  * Provide helpers to create integer lists for the given ranges. (#1179)
  * benchmark_runner.h: Remove superfluous semi colon (#1178)

-------------------------------------------------------------------
Mon Sep 13 08:40:20 UTC 2021 - Martin Pluskal <mpluskal@suse.com>

- Update to version 1.5.5:
  * [NFCI] Drop warning to satisfy clang's -Wunused-but-set-variable diag (#1174)
  * Add support for new architecture loongarch (#1173)
  * Use modern clang/libc++ for sanitizers (#1171)
  * Enable various sanitizer builds in github actions (#1167)
  * Random interleaving of benchmark repetitions - the sequel (fixes #1051) (#1163)
  * Fix leak in test, and provide path to remove leak from library (#1169)
  * [NFCI] Make BenchmarkRunner non-internal to it's .cpp file
  * [NFCI] RunBenchmarks(): extract FlushStreams()/Report() functions
  * compare.py: sort the results (#1168)
  * Make 'complexity reports' cache per-family, not global (#1166)
  * Introduce per-family instance index (#1165)
  * Introduce "family index" field into JSON output (#1164)
  * BenchmarkFamilies::FindBenchmarks(): correctly use std::vector<>::reserve()
  * Ensure that we print repetition count even when it was specified via flag `--benchmark_repetitions=`
  * Revert "Implementation of random interleaving.  (#1105)" (#1161)
  * Fix perf counter argument parsing (#1160)
  * Run build-and-test on all branches
  * Un-disable github actions :]
  * Run build-and-test on all branches
  * Set theme jekyll-theme-hacker
  * bump version to v1.5.4
  * Removing freenode from README
  * Fix pedantic compilation flag violation (#1156)
  * fix version recorded in releases (#1047)
  * Implementation of random interleaving.  (#1105)
  * remove appveyor and add libera.chat as IRC resource
  * [PFM] Extend perf counter support to multi-threaded cases. (#1153)
  * Support -Wsuggest-override (#1059)
  * Refactor `BenchmarkInstance` (#1148)
  * Remove travis configs that are covered by actions (#1145)
  * remove done TODOs
  * add g++-6 to ubuntu-14.04 (#1144)
  * Add MSVC ARM64 support to cmake (#1090)
  * Add ubuntu-14.04 build and test workflow (#1131)
  * Clean -Wreserved-identifier instances (#1143)
  * Fix StrSplit empty string case (#1142)
  * cmake: Add explicit BENCHMARK_ENABLE_LIBPFM option (#1141)
  * Add API to benchmark allowing for custom context to be added (#1137)
  * Add `benchmark_context` flag that allows per-run custom context. (#1127)
  * Add multiple compiler support to build-and-test workflow (#1128)
  * enable markdown rendering on github pages
  * Support optional, user-directed collection of performance counters (#1114)

-------------------------------------------------------------------
Wed Apr 28 06:47:33 UTC 2021 - mpluskal@suse.com

- Update to version 1.5.3:
  * Be compliant and return 0 from main.
  * Fix windows warning on type conversion (#1121)
  * Add verbosity to CI logs (#1122)
  * fix cmake issue with referencing a non-existing function argument (#1118)
  * [tools] Fix dumb mistake in previous commit - print aggregates only means aggregates, not non-aggregates
  * [tools] Don't forget to print UTest when printing aggregates only
  * [sysinfo] Fix CPU Frequency reading on AMD Ryzen CPU's (#1117)
  * Use fewer ramp up repetitions when KeepRunningBatch is used (#1113)
  * Add bazel status to README
  * Re-enable bazel without bazelisk and with scoped build/test targets (#1109)
- Drop no longer needed 0001-src-benchmark_register.h-add-missing-limits-inclusio.patch

-------------------------------------------------------------------
Mon Feb  8 21:56:31 UTC 2021 - Christophe Giboudeaux <christophe@krop.fr>

- Add upstream patch to fix build with GCC 11 (boo#1181865):
  * 0001-src-benchmark_register.h-add-missing-limits-inclusio.patch 

-------------------------------------------------------------------
Tue Dec 29 15:45:59 UTC 2020 - Martin Pluskal <mpluskal@suse.com>

- Update to version 1.5.2:
  * Timestamps in output are now rfc3339-formatted #965
  * overflow warnings with timers fixed #980
  * Python dependencies are now covered by a requirements.txt #994
  * JSON output cleaned up when no CPU scaling is present (#1008)
  * CartesianProduct added for easier settings of multiple ranges (#1029)
  * Python bindings improvements:
    + Custom main functions (#993)
    + A rename to google_benchmark (#199
    + More state methods bound (#1037) with a builder interface (#1040)
  * Workflow additions in github include pylint (#1039) and bindings runs (#1041)

-------------------------------------------------------------------
Mon Nov 18 23:15:31 UTC 2019 - Simon Lees <sflees@suse.de>

- %make_jobs is depricated replaced by %cmake_build

-------------------------------------------------------------------
Thu Oct 17 14:33:14 UTC 2019 - Richard Brown <rbrown@suse.com>

- Remove obsolete Groups tag (fate#326485)

-------------------------------------------------------------------
Mon Jun 24 07:26:21 UTC 2019 - Martin Pluskal <mpluskal@suse.com>

- Update to version 1.5.0:
  * Bump CMake minimum version to 3.5.1 (see dependencies.md)
  * Add threads and repetitions to the JSON outputa
  * Memory management and reporting hooks
  * Documentation improvements
  * Miscellaneous build fixes
- Disable gtest part of tests untill new gtest is available

-------------------------------------------------------------------
Sat Feb 23 10:47:16 UTC 2019 - Luigi Baldoni <aloisio@gmx.com>

- Use arch-specific libdir in pkgconfig file

-------------------------------------------------------------------
Mon Feb 18 09:52:40 UTC 2019 - Luigi Baldoni <aloisio@gmx.com>

- Use arch-specific directories for support files

-------------------------------------------------------------------
Mon Jun 25 11:56:55 UTC 2018 - mpluskal@suse.com

- Enable lto
- Update dependencies

-------------------------------------------------------------------
Mon May 28 21:43:24 UTC 2018 - avindra@opensuse.org

- Update to version 1.4.1
  * Realign expectation that State::iterations() returns 0 before
    the main benchmark loop begins.
  * CMake error message fixes
  * Enscripten check fix
  * Bazel pthread linking
  * Negative regexes
  * gmock fix

-------------------------------------------------------------------
Wed Apr  4 22:20:14 UTC 2018 - avindra@opensuse.org

- Update to version 1.4.0
  * Removal of deprecated headers
  * Improved CPU cache info reporting
  * Support State::KeepRunningBatch()
  * Support int64_t for AddRange()
  * New platform support: NetBSD, s390x, Solaris
  * Bazel build support
  * Support googletest unit tests
  * Add assembler tests
  * Various warnings fixed
- Drop benchmark-s390.patch
  * fixed upstream (commit ff2c255af5bb2fc2e5cd3b3685f0c6283117ce73)

-------------------------------------------------------------------
Tue Mar 27 03:25:41 UTC 2018 - stefan.bruens@rwth-aachen.de

- Remove ExclusiveArch, it works on non-x86 as well

-------------------------------------------------------------------
Fri Mar 16 18:38:52 UTC 2018 - avindra@opensuse.org

- Update to version 1.3.0:
 * Ranged for loop optimization!
 * Make installation optional
 * Better stats including user-provided ones
 * JSON reporter format fixes
 * Documentation improvements
- Includes changes from 1.2.0:
 * User-defined counters
 * Single header library
 * Ability to clear benchmarks so the runtime registration is more flexible
 * Sample-based standard deviation
 * 32-bit build enabled
 * Bug fixes
- Cleanup with spec-cleaner
- Use %ctest macro for testing

-------------------------------------------------------------------
Fri Jan 27 14:46:11 UTC 2017 - mpluskal@suse.com

- Update to version 1.1.0:
  * ArgNames support
  * Fixes for OSX and Cygwin and MSVC builds
  * PauseTiming and ResumeTiming are per thread (#286)
  * Better Range and Arg specifications
  * Complexity reporting

-------------------------------------------------------------------
Fri Jan 27 14:28:26 UTC 2017 - bg@suse.com

- add support for s390x:
  * benchmark-s390.patch

-------------------------------------------------------------------
Sat Nov 12 13:43:21 CET 2016 - ro@suse.de

- add ExclusiveArch, package is not ported to anything but
  x86 and x86_64 

-------------------------------------------------------------------
Mon Jul  4 09:24:59 UTC 2016 - mpluskal@suse.com

- Disable lto as it does not seem to go well with -fPIE

-------------------------------------------------------------------
Sat May 14 17:15:41 UTC 2016 - mpluskal@suse.com

- Initial package for version 1.0.0

