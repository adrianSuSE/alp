-------------------------------------------------------------------
Tue Jan 19 21:29:19 UTC 2021 - Dirk Müller <dmueller@suse.com>

- update to 1.5.0:
  * dwarf: clang doesn't respect the static alias
  * Fixed a missing dependency in dwarf-eh.h 
  * x86_64: Fix tdep_init_done when built with libatomic_ops 
  * mips: make _step_n64 as a static function
  * Added braces to suppress empty if/else warnings
  * Delete hardcode of address size to support MIPS64.
  * Fix format specifier for int64_t:29
  * Add initial support for Solaris x86-64
  * x86_64: Add fixup code if previous RIP was invalid
  * x86-64: make `is_cached_valid_mem` functional
  * arm: clear ip thumb/arm mode bit before move to previous instruction
  * Fix compilation with -fno-common.
  * Fix off-by-one error in x86_64 stack frames
  * aarch64: Fix __sigset build issue on muslC
  * Make SHF_COMPRESSED use contingent on its existence 
- remove libunwind_U_dyn_info_list.patch (upstream)

-------------------------------------------------------------------
Tue Oct 20 20:01:36 UTC 2020 - Berthold Gunreben <azouhr@opensuse.org>

- Enable s390x for building

-------------------------------------------------------------------
Wed May 13 08:53:38 UTC 2020 - Pedro Monreal Gonzalez <pmonrealgonzalez@suse.com>

- Fix compilation with -fno-common [bsc#1171549]
- Add patch libunwind_U_dyn_info_list.patch

-------------------------------------------------------------------
Tue Apr 28 15:37:18 UTC 2020 - Pedro Monreal Gonzalez <pmonrealgonzalez@suse.com>

- Update to 1.4.0
  - Fix compilation with -fno-common.
  - arm: clear ip thumb/arm mode bit before move to previous instruction (#131)
  - tests: fix test-coredump-unwind without HAVE_EXECINFO_H (#165)
  - There are 20 not 9 failing tests on Solaris (#162)
  - change asm to __asm__ to support -std=c11 or similar (#149)
  - x86-64: make `is_cached_valid_mem` functional (#146)
  - Allow to build without weak `backtrace` symbol. (#142)
  - fix compile issue on SH platform (#137)
  - Add support for zlib compressed elf .debug_frame sections
  - README: add libc requirement description (#121)
  - Older systems (e.g. RHEL5) do not have pipe2(). (#122)
  - x86_64: Add fixup code if previous RIP was invalid (#120)
  - Fix format specifier for int64_t:29 (#117)
  - Delete hardcode of address size to support MIPS64. (#114)
  - Added braces to suppress empty if/else warnings (#112)
  - mips: make _step_n64 as a static function
  - x86_64: Fix tdep_init_done when built with libatomic_ops
  - x86_64: tsan clean (#109)
  - Fixed a missing dependency in dwarf-eh.h
  - dwarf: clang doesn't respect the static alias (#102)
- Update libunwind.keyring
- Remove libunwind-gcc10-build-fno-common.patch fixed upstream

-------------------------------------------------------------------
Wed Feb 19 09:24:20 UTC 2020 - Pedro Monreal Gonzalez <pmonrealgonzalez@suse.com>

- Fix build with GCC-10: [bsc#1160876]
  * In GCC-10, the default option -fcommon will change to -fno-common
- Add libunwind-gcc10-build-fno-common.patch
 
-------------------------------------------------------------------
Sun Jul  7 21:45:20 UTC 2019 - Jan Engelhardt <jengelh@inai.de>

- Ensure neutrality of description. Avoid name repetition in
  summaries.

-------------------------------------------------------------------
Mon Jul  1 09:10:50 UTC 2019 - Andreas Schwab <schwab@suse.de>

- Update to 1.3.1
  * Iteration of unwind register states support
  * Freebsd/Armv6 support
  * Many, many dwarf bugfixes
  * Mips remote unwind support
  * aarch64 ptrace support
- fix_versioning_libunwind_1.2.1.patch: removed

-------------------------------------------------------------------
Sun Feb 18 18:29:20 UTC 2018 - ronisbr@gmail.com

- Add patch `fix_versioning_libunwind_1.2.1.patch`.
  * This patch fixes the upstream bug gh#libunwind/libunwind#30.
    This bug was causing the julia build process to fail.
    NOTE: This patch shall be removed in the next version of
    libunwind.

-------------------------------------------------------------------
Fri Feb  2 11:52:05 UTC 2018 - tchvatal@suse.com

- Update keyring

-------------------------------------------------------------------
Wed Jan 24 04:05:11 UTC 2018 - avindra@opensuse.org

- Upgrade to 1.2.1
  * minor package fixes for tilegx, mips, others
  * fix missing include file issues
- cleanup with spec-cleaner
- do not ship static libraries

-------------------------------------------------------------------
Mon Jun 20 09:12:10 UTC 2016 - tchvatal@suse.com

- Enable minidebugs parsing with lzma

-------------------------------------------------------------------
Tue May 17 11:10:54 UTC 2016 - tchvatal@suse.com

- Version update to 1.2-rc1, should include all our patches:
  * 0001-Mark-run-ptrace-mapper-and-run-ptrace-misc-as-XFAIL-.patch
  * 0002-Mark-run-ptrace-mapper-and-run-ptrace-misc-as-XFAIL-.patch
  * libunwind-1.1-expected-fails.patch
  * libunwind-CVE-2015-3239.patch
  * libunwind-aarch64.patch
  * libunwind-ppc64le.patch
  * libunwind-1.1-tests.diff

-------------------------------------------------------------------
Sun Apr 24 16:25:21 UTC 2016 - matwey.kornilov@gmail.com

- Add patches for boo#976955:
  * 0001-Mark-run-ptrace-mapper-and-run-ptrace-misc-as-XFAIL-.patch
  * 0002-Mark-run-ptrace-mapper-and-run-ptrace-misc-as-XFAIL-.patch

-------------------------------------------------------------------
Thu Jul  2 12:47:57 UTC 2015 - tchvatal@suse.com

- Add patch for bnc#936786 CVE-2015-3239, off by one:
  * libunwind-CVE-2015-3239.patch

-------------------------------------------------------------------
Sun Mar 29 18:47:32 UTC 2015 - meissner@suse.com

- added baselibs.conf for -xxbit packages

-------------------------------------------------------------------
Wed Mar  4 12:02:48 UTC 2015 - mpluskal@suse.com

- Use url for source
- Add gpg signature

-------------------------------------------------------------------
Mon May 19 12:26:43 UTC 2014 - tchvatal@suse.com

- Clean up with spec-cleaner
- Add dependency over libatomic_opts that was missing
- Do not disable fortify-sources as it seems to work with it anyway

-------------------------------------------------------------------
Tue Dec 17 21:07:33 UTC 2013 - uweigand@de.ibm.com

- libunwind-ppc64le.patch: Add support for ppc64le

-------------------------------------------------------------------
Wed Dec 11 20:17:22 UTC 2013 - dvaleev@suse.com

- add ppc64le 

-------------------------------------------------------------------
Mon Aug 12 10:46:30 UTC 2013 - schwab@suse.de

- libunwind-aarch64.patch: Add support for AArch64 from upstream

-------------------------------------------------------------------
Sat Jan 26 08:19:34 UTC 2013 - dmueller@suse.com

- filelist fix for ARM

-------------------------------------------------------------------
Tue Dec 11 18:15:57 UTC 2012 - dvaleev@suse.com

- update to libunwind 1.1:
    coredump unwind support
    New arch: SuperH
    Improved support for PowerPC, ARM
    Lots of cleanups, perf tweaks
    pkg-config support

- drop all upstreamed patches
- refresh libunwind-1.1-tests.diff patch 
- enable ppc ppc64 build
- Mark some tests as expected to fail 
  libunwind-1.1-expected-fails.patch

-------------------------------------------------------------------
Mon Aug 20 23:37:17 UTC 2012 - agraf@suse.com

- add support for ARM

-------------------------------------------------------------------
Mon Mar  5 22:35:56 CET 2012 - dmueller@suse.de

- remove build from ARM 

-------------------------------------------------------------------
Thu Feb  9 15:53:49 UTC 2012 - jengelh@medozas.de

- Remove redundant tags/sections
- Fix dangling symlink /usr/lib/libunwind.so (which was
  pointing at "libunwind.so.8.*", verbatim asterisk)
- Throw out .la file

-------------------------------------------------------------------
Wed Nov 23 09:24:25 UTC 2011 - coolo@suse.com

- add libtool as buildrequire to avoid implicit dependency

-------------------------------------------------------------------
Mon Nov  7 19:12:14 CET 2011 - dmueller@suse.de

- disable testsuite when running under qemu

-------------------------------------------------------------------
Mon Oct 17 16:03:13 CEST 2011 - dmueller@suse.de

-  update to libunwind 1.0.1:
  * hundreds of fixes, see http://git.savannah.gnu.org/gitweb/?p=libunwind.git;a=shortlog
    for details
  * ARM support
  * Greatly improved x86-64 support thanks to Arun Sharma.
  * Support for PPC64 added by  Jose Flavio Aguilar Paulino.
  * Testing, stability and many fixes on x86 (Paul Pluzhnikov)
  * Improved local and remote unwinding on ARM (Ken Werner)
  * Fast unwind (rbp, rsp, rip only) on x86_64 with a fallback to slow code paths

-------------------------------------------------------------------
Sun Oct 31 12:37:02 UTC 2010 - jengelh@medozas.de

- Use %_smp_mflags

-------------------------------------------------------------------
Thu Jan 28 12:59:59 CET 2010 - rguenther@suse.de

- Build without _FORTIFY_SOURCE, recent glibc fortify longjmp which
  confuses libunwind a lot and breaks the build.

-------------------------------------------------------------------
Thu Oct 15 16:43:17 CEST 2009 - marek.belisko@open-nandra.com

- Fix typo in Gtest-dyn1.c (bnc#531705)

-------------------------------------------------------------------
Mon Jan 26 19:37:58 CET 2009 - schwab@suse.de

- Fix overlapping memcpy.

-------------------------------------------------------------------
Tue May 13 18:09:29 CEST 2008 - schwab@suse.de

- Update to libunwind 0.98.6.
  ** Fix address-leak triggered by invalid byte-order.  Fixed by Andreas Schwab.
  ** On ia64, get_static_proc_name() no longer uses a weak reference to
     _Uelf64_get_proc_name(), since that was causing problems with archive
     libraries and no longer served any apparent purpose.  Fixed by
     Curt Wohlgemuth.

-------------------------------------------------------------------
Fri Apr 18 17:26:49 CEST 2008 - rguenther@suse.de

- Fix build.

-------------------------------------------------------------------
Wed Jun  6 14:03:41 CEST 2007 - schwab@suse.de

- Fix memory leak.

-------------------------------------------------------------------
Thu Jan 26 01:59:01 CET 2006 - schwab@suse.de

- Run ldconfig.

-------------------------------------------------------------------
Wed Jan 25 21:30:26 CET 2006 - mls@suse.de

- converted neededforbuild to BuildRequires

-------------------------------------------------------------------
Mon Nov 14 01:36:47 CET 2005 - schwab@suse.de

- Undo last change.

-------------------------------------------------------------------
Mon Oct 31 18:57:27 CET 2005 - dmueller@suse.de

- build with nonexecutable stack 

-------------------------------------------------------------------
Fri Jul 22 16:08:32 CEST 2005 - schwab@suse.de

- Remove broken tests.

-------------------------------------------------------------------
Mon May  2 23:19:19 CEST 2005 - schwab@suse.de

- Fix broken test.

-------------------------------------------------------------------
Sat Apr 23 14:30:58 CEST 2005 - schwab@suse.de

- Only libunwind needs to be in /lib.
- There are no expected testsuite failures on ia64 any more.

-------------------------------------------------------------------
Thu Apr 21 12:11:04 CEST 2005 - schwab@suse.de

- Update to libunwind 0.98.5.

-------------------------------------------------------------------
Fri Feb 25 11:39:59 CET 2005 - schwab@suse.de

- Update to libunwind 0.98.4.

-------------------------------------------------------------------
Fri Feb  4 00:41:07 CET 2005 - schwab@suse.de

- Fix libdir.
- Fix warning.

-------------------------------------------------------------------
Tue Dec  7 16:13:52 CET 2004 - schwab@suse.de

- Update to libunwind 0.98.3.

-------------------------------------------------------------------
Mon Nov 15 00:38:29 CET 2004 - schwab@suse.de

- Don't move around libtool libraries.

-------------------------------------------------------------------
Sun Nov 14 16:50:54 CET 2004 - schwab@suse.de

- Split off libunwind-devel.
- Move library to /lib.
- Remove broken test.

-------------------------------------------------------------------
Fri Nov  5 14:24:02 CET 2004 - schwab@suse.de

- Update to libunwind 0.98.2.

-------------------------------------------------------------------
Thu Oct 28 17:36:30 CEST 2004 - schwab@suse.de

- Update to libunwind 0.98.1.

-------------------------------------------------------------------
Wed Sep 29 14:57:05 CEST 2004 - schwab@suse.de

- Fix installed headers.

-------------------------------------------------------------------
Thu Sep  9 23:21:14 CEST 2004 - schwab@suse.de

- Update to libunwind 0.98.
- Enable building on x86_64.

-------------------------------------------------------------------
Thu Aug 19 11:43:49 CEST 2004 - schwab@suse.de

- Update to libunwind 0.97.

-------------------------------------------------------------------
Sun Apr 18 19:26:44 CEST 2004 - schwab@suse.de

- Pacify autobuild.

-------------------------------------------------------------------
Fri Dec  5 14:24:28 CET 2003 - schwab@suse.de

- Update to libunwind 0.96.

-------------------------------------------------------------------
Thu Dec  4 15:38:42 CET 2003 - schwab@suse.de

- Update to libunwind 0.95.

-------------------------------------------------------------------
Fri Sep 26 21:54:40 CEST 2003 - schwab@suse.de

- Update to libunwind 0.93.

-------------------------------------------------------------------
Wed Jul 23 12:09:36 CEST 2003 - schwab@suse.de

- Fix typo in specfile.

-------------------------------------------------------------------
Thu Jul 11 11:13:03 CEST 2002 - schwab@suse.de

- Initial version.

