-------------------------------------------------------------------
Sat Jul 17 06:43:41 UTC 2021 - Dirk Müller <dmueller@suse.com>

- update to 0.31.0:
  - Add 'BLANK_LINES_BETWEEN_TOP_LEVEL_IMPORTS_AND_VARIABLES' to support setting
  a custom number of blank lines between top-level imports and variable
  definitions.
  - Ignore end of line `# copybara:` directives when checking line length.
  - Do not scan exlcuded directories. Prior versions would scan an exluded
  folder then exclude its contents on a file by file basis. Preventing the
  folder being scanned is faster.

-------------------------------------------------------------------
Fri May 29 09:31:01 UTC 2020 - Marketa Calabkova <mcalabkova@suse.com>

- update to 0.30.0
  * Added `SPACES_AROUND_LIST_DELIMITERS`, `SPACES_AROUND_DICT_DELIMITERS`,
    and `SPACES_AROUND_TUPLE_DELIMITERS` to add spaces after the opening
    and before the closing delimiters for lists, dicts, and tuples.
  * Adds `FORCE_MULTILINE_DICT` knob to ensure dictionaries always split,
    even when shorter than the max line length.
  * New knob `SPACE_INSIDE_BRACKETS` to add spaces inside brackets, braces, and
    parentheses.
  * New knob `SPACES_AROUND_SUBSCRIPT_COLON` to add spaces around the subscript /
    slice operator.
  * Renamed "chromium" style to "yapf". Chromium will now use PEP-8 directly.
  * `CONTINUATION_ALIGN_STYLE` with `FIXED` or `VALIGN-RIGHT` now works with
    space indentation.
  * Don't over-indent a parameter list when not needed. But make sure it is
    properly indented so that it doesn't collide with the lines afterwards.
  * Don't split between two-word comparison operators: "is not", "not in", etc.
- Replace nose with pytest
  * not WLOG, some tests get skipped because of capturing stdin

-------------------------------------------------------------------
Tue Feb  4 15:55:15 UTC 2020 - Marketa Calabkova <mcalabkova@suse.com>

- update to 0.29.0
  * Add the `--quiet` flag to suppress output. The return code is 1 if there are
    changes, similarly to the `--diff` flag.
  * Catch and report `UnicodeDecodeError` exceptions.
  * Few bugfixes, see CHANGELOG

-------------------------------------------------------------------
Mon Jul 22 11:28:10 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Update to 0.28.0:
  * Bugfix release, see CHANGELOG

-------------------------------------------------------------------
Tue Apr  9 11:51:52 UTC 2019 - Marketa Calabkova <mcalabkova@suse.com>

- update to version 0.27.0
  * mostly bugfix release, see CHANGELOG for details

-------------------------------------------------------------------
Thu Feb 14 04:13:37 UTC 2019 - John Vandenberg <jayvdb@gmail.com>

- Remove optional dependency from BuildRequires as no tests fail
  or are skipped when it is missing.
- Remove tests from package
- Update to v0.26.0
  * Many changes to behaviour; see CHANGELOG for details
- from v0.25.0
  * Added `INDENT_BLANK_LINES` knob
  * Support additional file exclude patterns in .yapfignore file
  * Correctly determine if a scope is the last in line

-------------------------------------------------------------------
Wed Oct 24 13:23:45 UTC 2018 - Tomáš Chvátal <tchvatal@suse.com>

- Version update to 0.24.0:
  * Support for python 3.7

-------------------------------------------------------------------
Wed Aug 29 11:37:58 UTC 2018 - tchvatal@suse.com

- Version update to 0.23.0:
  * Many changes to behaviour based on CHANGELOG
  * Add various documents/license to distributed tarball
 
-------------------------------------------------------------------
Sun Mar  4 11:06:23 UTC 2018 - jengelh@inai.de

- Trim description and focus on the package, not the process.

-------------------------------------------------------------------
Fri Oct 20 16:19:54 UTC 2017 - toddrme2178@gmail.com

- initial version
