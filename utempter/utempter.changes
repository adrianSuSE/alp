-------------------------------------------------------------------
Mon Sep 28 20:25:57 UTC 2020 - Stefan Brüns <stefan.bruens@rwth-aachen.de>

- Fix License, libutempter has been LGPL-2.1+ for the last 19 years
- Fix utempter.8 man page to match real parameters
- Remove erroneous links to utempter.8 man page
- Clean up spec file

-------------------------------------------------------------------
Thu Sep  3 15:52:03 UTC 2020 - Marcus Meissner <meissner@suse.com>

- fixed utempter location after libexecdir change (bsc#1175925)

-------------------------------------------------------------------
Wed May  6 12:15:39 UTC 2020 - pgajdos@suse.com

- version update to 1.2.0
  * maintenance work:
    https://github.com/altlinux/libutempter/commits/
- modified patches
  % utempter-no-staticlib.patch (refreshed)
- modified sources
  % utempter.keyring
- set now libexecdir=%{_libexecdir} as utemper binary complies FHS
  definition

-------------------------------------------------------------------
Fri Feb 23 08:35:22 UTC 2018 - fvogt@suse.com

- Use %license (boo#1082318)

-------------------------------------------------------------------
Wed Sep 13 19:43:32 UTC 2017 - schwab@linux-m68k.org

- Pre-require group(%{utmpGroup})

-------------------------------------------------------------------
Thu Nov 13 16:13:17 UTC 2014 - tchvatal@suse.com

- remove /sbin/ldconfig -n $DESTDIR%{_libdir} as it should be not
  needed in install phase
- Remove Requires: %name as there is no name and adjust obsolete
  as there exist version 0.5.5.6 which is larger than the provides
- Remove sle10 obsolete for ppc64 utempter-64bit
- Clean-up with spec-cleaner

-------------------------------------------------------------------
Thu Aug 14 15:59:02 UTC 2014 - fcrozat@suse.com

- Obsoletes utempter-32bit in libutempter0-32bit.

-------------------------------------------------------------------
Fri Jun 21 07:17:10 UTC 2013 - meissner@suse.com

- remove gpg offline checking to avoid buildloops. will be
  done by source service.

-------------------------------------------------------------------
Wed Jun  5 11:23:54 UTC 2013 - meissner@suse.com

- updated to 1.1.6 upstream (bnc#823302)
  - new apis (old are staying)
- utempter-ppc64.patch: is upstream, removed
- utempter-0.5.5-pie.diff: done differently, removed
- utempter-no-staticlib.patch: no static library wanted
- gpg source tarball checking
- hooked up permissions framework correctly

-------------------------------------------------------------------
Fri Apr  5 10:01:05 UTC 2013 - idonmez@suse.com

- Add Source URL, see https://en.opensuse.org/SourceUrls 

-------------------------------------------------------------------
Sat Oct 15 08:59:16 UTC 2011 - andrea.turrini@gmail.com

- fixed typo in utempter.spec

-------------------------------------------------------------------
Tue Oct  4 09:03:15 UTC 2011 - uli@suse.com

- cross-build fix: use %__cc macro

-------------------------------------------------------------------
Thu Nov 11 09:09:07 UTC 2010 - aj@suse.de

- Use group utmp for utempter (bnc#652877).

-------------------------------------------------------------------
Sun Oct 31 12:37:02 UTC 2010 - jengelh@medozas.de

- Use %_smp_mflags

-------------------------------------------------------------------
Thu Sep  9 15:00:50 UTC 2010 - aj@suse.de

- Split up devel package.

-------------------------------------------------------------------
Mon May 10 15:52:56 CEST 2010 - meissner@suse.de

- handle ppc64 utmp entries with 32bit timeval correctly. bnc#602489

-------------------------------------------------------------------
Wed Dec 16 00:17:51 CET 2009 - jengelh@medozas.de

- add baselibs.conf as a source
- enable parallel building

-------------------------------------------------------------------
Wed Jan  7 12:34:56 CET 2009 - olh@suse.de

- obsolete old -XXbit packages (bnc#437293)

-------------------------------------------------------------------
Thu Apr 10 12:54:45 CEST 2008 - ro@suse.de

- added baselibs.conf file to build xxbit packages
  for multilib support

-------------------------------------------------------------------
Sat May 27 22:15:27 CEST 2006 - schwab@suse.de

- Don't strip binaries.

-------------------------------------------------------------------
Wed Jan 25 21:31:03 CET 2006 - mls@suse.de

- converted neededforbuild to BuildRequires

-------------------------------------------------------------------
Mon Jun 20 13:21:30 CEST 2005 - kukuk@suse.de

- Compile with -fpie/-pie

-------------------------------------------------------------------
Thu Aug  5 11:38:05 CEST 2004 - okir@suse.de

- Updated to latest upstream version

-------------------------------------------------------------------
Tue Apr 20 10:48:40 CEST 2004 - okir@suse.de

- Fix incorrect check for /../ in path names (#39169)

-------------------------------------------------------------------
Fri Jan 16 11:52:12 CET 2004 - thomas@suse.de

- added man-page utempter.8 (EAL3)

-------------------------------------------------------------------
Sun Jan 11 09:29:00 CET 2004 - adrian@suse.de

- add %run_ldconfig

-------------------------------------------------------------------
Fri Apr 12 09:20:27 CEST 2002 - okir@suse.de

- well, if the files list says %_libdir, one should make sure
  the shared lib really goes to in %_libdir :)

-------------------------------------------------------------------
Fri Mar  8 13:14:24 CET 2002 - kukuk@suse.de

- Create libutemtper.so.0 link and include it in RPM [Bug #14672]

-------------------------------------------------------------------
Tue Jan 29 10:38:33 CET 2002 - okir@suse.de

- %files list uses %_libdir rather than /usr/lib

-------------------------------------------------------------------
Fri Jan 25 16:23:24 CET 2002 - okir@suse.de

- Initial package.

