-------------------------------------------------------------------
Mon Feb 14 15:46:00 UTC 2022 - Tony Jones <tonyj@suse.com>

- Resolve gcc compilation error reported against glib2 and libvirt
    New patch: sys-sdt.h-fp-constraints-arm32.patch
    New patch: sys-sdt.h-fp-constraints-x86_64.patch
    New patch: sys-sdt.h-fp-constraints-aarch64-s390.patch

-------------------------------------------------------------------
Thu Jan 27 23:26:36 UTC 2022 - Tony Jones <tonyj@suse.com>

- Upgrade to version 4.6. See systemtap.spec for changelog

-------------------------------------------------------------------
Fri Dec  6 17:32:56 UTC 2019 - Tony Jones <tonyj@suse.com>

- Upgrade to version 4.2. See systemtap.spec for changelog

-------------------------------------------------------------------
Fri Aug 9 20:06:57 UTC 2019 - Tony Jones <tonyj@suse.com>

- Upgrade to version 4.1. See systemtap.spec for changelog

-------------------------------------------------------------------
Fri Aug  3 16:50:41 UTC 2018 - sschricker@suse.de

- Upgrade to version 3.3:

  Changelog: https://lwn.net/Articles/757030/

  eBPF backend extensions, easier access to examples, adapting to
  meltdown/spectre complications, real-time / high-cpu-count
  concurrency fixes

- Remove patches, because the issues were fixed upstream:
    systemtap-change-extra_cflags-escape-processing.patch 

- Adjust patch for version 3.3:
    systemtap-build-source-dir.patch

- Added public key of "Frank Ch. Eigler <fche@elastic.org>",
  since he signed the new package

-------------------------------------------------------------------
Fri Feb 16 15:20:50 UTC 2018 - matz@suse.com

- Add Conflicts with systemtap-sdt-devel, as that one again
  provides the headers as well.

-------------------------------------------------------------------
Thu Feb  8 15:26:17 UTC 2018 - matz@suse.com

- Created new specfile systemtap-headers.spec, so bootstrap cycles
  packages can use them without blowing up the cycle.
  [fate#324969]
