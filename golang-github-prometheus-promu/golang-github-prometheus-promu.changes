-------------------------------------------------------------------
Mon Mar  8 11:03:45 UTC 2021 - Witek Bedyk <witold.bedyk@suse.com>

- Build requires Go 1.15

-------------------------------------------------------------------
Fri Feb  7 17:13:26 UTC 2020 - Witek Bedyk <witold.bedyk@suse.com>

- Update to 0.5.0
  + Features:
    * Add support for aix/ppc64. #151
    * Fallback to git describe output if no VERSION. #130
    * Make extldflags extensible by configuration. #125
  + Enhancements:
    * cmd/release: add --timeout option. #142
    * cmd/release: create release in GitHub if none exists. #148
    * Avoid bind-mounting to allow building with a remote docker engine #95
  + Bug Fixes:
    * cmd/tarball: restore --prefix flag. #133
    * cmd/release: don't leak credentials in case of error. #136

- Use obs-service-go_modules

-------------------------------------------------------------------
Fri Feb  8 11:05:48 UTC 2019 - Jan Fajerski <jan.fajerski@suse.com>

- Update to 0.2.0
  + Features:
    * Adding changes to support s390x
    * Add option to disable static linking
    * Add support for 32bit MIPS.
    * Added check_licenses Command to Promu
  + Enhancements:
    * Allow to customize nested options via env variables
    * Bump Go version to 1.11
    * Add warning if promu info is unable to determine repo info
  + Bug Fixes:
    * Fix build on SmartOS by not setting gcc's -static flag
    * Fix git repository url parsing

-------------------------------------------------------------------
Thu Jan 25 10:13:29 UTC 2018 - kkaempf@suse.com

- Update to 0.1.0

-------------------------------------------------------------------
Wed Mar 29 09:16:48 UTC 2017 - moio@suse.com

- Initial version
