#
# spec file for package grep
#
# Copyright (c) 2022 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           grep
Version:        3.7
Release:        0
Summary:        Print lines matching a pattern
License:        GPL-3.0-or-later
Group:          Productivity/Text/Utilities
URL:            https://www.gnu.org/software/grep/
#!RemoteAsset: sha256:5c10da312460aec721984d5d83246d24520ec438dd48d7ab5a05dbc0d6d6823c
Source0:        https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
#!RemoteAsset: sha256:d79a0137eb803938ff47dc366825d05d1a042457f74acc264a361a84428a5de7
Source2:        https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz.sig
#!RemoteAsset: sha256:719a35f28dcc2b8116703c97bd177a7876e8e28dd8f8cd6e10939d9b3fd97c3f
Source3:        https://savannah.gnu.org/project/memberlist-gpgkeys.php?group=grep&download=1#/%{name}.keyring
Source4:        profile.sh
Source5:        %{name}-rpmlintrc
BuildRequires:  fdupes
BuildRequires:  glibc-locale
BuildRequires:  makeinfo
BuildRequires:  pcre-devel
Provides:       base:%{_bindir}/grep

%description
The grep command searches one or more input files for lines containing a
match to a specified pattern.  By default, grep prints the matching lines.

%lang_package

%prep
%autosetup -p1

%build
%configure \
  --disable-silent-rules \
  %{nil}
%if 0%{?do_profiling}
  %make_build CFLAGS="%{optflags} %{cflags_profile_generate}"
  sh %{SOURCE4} # profiling run
  %make_build clean
  %make_build CFLAGS="%{optflags} %{cflags_profile_feedback}"
%else
  %make_build
%endif

%check
%if 0%{?qemu_user_space_build}
echo exit 77 > tests/stack-overflow
echo exit 77 > tests/pcre-jitstack
echo exit 77 > gnulib-tests/test-c-stack.sh
echo 'int main() { return 77; }' > gnulib-tests/test-sigsegv-catch-stackoverflow1.c
echo 'int main() { return 77; }' > gnulib-tests/test-sigsegv-catch-stackoverflow2.c
%endif
%make_build check

%install
%make_install
%if !0%{?usrmerged}
install -d %{buildroot}/bin
ln -sf %{_bindir}/egrep %{buildroot}/bin/egrep
ln -sf %{_bindir}/fgrep %{buildroot}/bin/fgrep
ln -sf %{_bindir}/grep %{buildroot}/bin/grep
%endif
%fdupes -s %{buildroot}
%find_lang %{name}

%files
%license COPYING
%doc README AUTHORS NEWS THANKS TODO ChangeLog*
%if !0%{?usrmerged}
/bin/egrep
/bin/fgrep
/bin/grep
%endif
%{_bindir}/egrep
%{_bindir}/fgrep
%{_bindir}/grep
%{_mandir}/man1/egrep.1%{?ext_man}
%{_mandir}/man1/fgrep.1%{?ext_man}
%{_mandir}/man1/grep.1%{?ext_man}
%{_infodir}/grep.info%{?ext_info}

%files lang -f %{name}.lang

%changelog
