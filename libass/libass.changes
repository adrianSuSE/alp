-------------------------------------------------------------------
Sun Sep 19 17:06:27 UTC 2021 - Oleg Oshmyan <chortos@inbox.lv>

- Update to version 0.15.2
  * Fix some format string bugs, which could lead to
    garbled log messages or on some platforms crashes
  * Fix ass_set_fonts_dir failing to load long paths
  * Better handling of control characters that have no associated font glyphs
- Update Requires and BuildRequires to actual pkgconfig versions

-------------------------------------------------------------------
Mon May  3 15:58:23 UTC 2021 - Ferdinand Thiessen <rpm@fthiessen.de>

- Update to version 0.15.1
  * Bugfix release, no significant API or ABI changes
  * Fix crashing on some ASS files with embedded fonts
  * Fix total font fallback failure with some fonts on Core Text
  * Fix embedded and memory fonts not actually being used unless
    ass_set_fonts was called after they have been added
  * Fix events assuming the largest xbord-value of a preceding event
    of the same frame during collision handling
  * Fix shift in vertical positioning of some right-to-left events
    with \an4-6, \pos, \move or Scroll effects
  * Fix imprecise output for \be with huge argument
  * Overhaul x86 assembly for better performance
  * Reduce temporary allocation in rendering
  * DirectWrite: request fonts on demand for better performance and
    improved compatibility with external on-demand font activation
  * Accept pointers to const as arguments to ass_add_font
  * Improve VSFilter compatibility:
    * Disregard zero area events in collision handling
    * Do not apply \fsp to vector drawings
    * Fix bug preventing cutting out glyphs from border & shadow
      with abnormal \fad values

-------------------------------------------------------------------
Sat Nov 28 15:28:46 UTC 2020 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 0.15.0
  * Fix backwards/VSFilter-incompatible font sizes with FreeType
    2.10+
  * Improve speed via better caching
  * Require HarfBuzz unconditionally to ensure good shaping for
    complex scripts
  * ass_set_use_margins(true) now simply places text on the whole
    screen instead of attempting to tie it to video positioning
    (set by the margin values) and failing in various ways when
    margins are used to implement pan & scan in a video player
  * Add ass_track_set_feature() & catch-all
    ASS_FEATURE_INCOMPATIBLE_EXTENSIONS
  * Add ASS_FEATURE_BIDI_BRACKETS to enable Unicode 6.3+ bracket
    matching when applying the Unicode Bidirectional Algorithm
    (incompatible with VSFilter; requires libass built with
    FriBidi 1.0+)
  * Fix stack overflow on deeply nested \t tags
  * Fix positioning of events with leading line breaks
  * Fix small but nonzero \bord becoming \bord0 (regression in
    0.14.0)
  * Measure BorderStyle=4 box padding from glyph border, not
    from glyph fill
  * Scale everything from script resolution if storage size is
    not set (including borders and shadows regardless of
    ScaledBorderAndShadow)
  * Fix the default aspect ratio calculation when neither
    ass_set_pixel_aspect() nor ass_set_aspect_ratio() is called
  * Multiple fixes for karaoke override tags
  * Handle memory allocation failures better:
    avoid some crashes and produce images closer to truth
  * Avoid some integer overflows
  * Add internal infrastructure for regression testing
  * Improve VSFilter compatibility:
    + Treat invalid nested \t tags like VSFilter
    + Make \t(T,T,...) at time exactly T use the post-transition
      values
    + Make lines stack more like they do in VSFilter
    + Default ScaledBorderAndShadow to 0 (like VSFilter),
      except for subtitles that were produced by old FFmpeg/Libav
    + Make shadow positioning with 3D transforms match VSFilter's
    + Cut out glyphs from border & shadow in all the same cases
      as VSFilter
    + Match VSFilter on animated color tags with negative
      acceleration
    + Fix parsing of some files that VSFilter accepts but libass
      didn't by ignoring leading whitespace in each line of an
      ASS file or CodecPrivate
    + Improve font selection with CoreText
    + Stop faux-bolding fonts that are too bold to get faux bold
      in VSFilter
    + Ignore leading/trailing spaces when calculating height of
      nonblank lines
    + Match VSFilter on \fade with large alpha value arguments
    + Stop splitting bitmaps on font substitution
    + Multiple fixes for Banner and Scroll effects
    + Multiple fixes for karaoke override tags

-------------------------------------------------------------------
Sun Dec  3 11:07:13 UTC 2017 - mpluskal@suse.com

- Update to version 0.14.0:
  * Brand new, faster and better outline stroker (replaces FreeType
    stroker)
  * Remove option to use the FreeType rasterizer
  * Fix spots of missing border around self-intersecting shapes
  * Switch from Yasm to NASM for building hand-written assembler
    code
  * Clear font cache in ass_set_fonts(). This fixes potentially
    incorrect font choices and an occasional crash if this function
    is called midway through rendering a file.

-------------------------------------------------------------------
Tue Jun  6 10:32:46 UTC 2017 - idonmez@suse.com

- Update to version 0.13.7
  * Fix invalid memory accesses with BorderStyle=4
  * Fix change detection bug on frame resizes
  * Fix cache bugs with border size
  * Reduce precision of border width in font outline caching
  * Don't treat :;<=>? as hexadecimal digits in color headers
  * Fix parsing of unusual Alignment values in ASS style definitions
  * Fix potential truncation in timestamp parsing
  * Treat negative PlayResX/PlayResY like VSFilter
  * Fixes to parsing of embedded fonts
  * Remove arbitrary bitmap limit (fixes issues with subtitle rendering at 4K)
  * Allow using shadow offset to adjust size of background with BorderStyle=4

-------------------------------------------------------------------
Tue Feb 21 15:06:56 UTC 2017 - mpluskal@suse.com

- Update to version 0.13.6:
  * Add Justify style override that changes text justification
    (left/right/center) without affecting event positioning. This
    breaks ABI compatibility with prior releases.
  * Fix ass_set_cache_limits() to affect total bitmap cache size
    including composite bitmap cache.
  * Number parsing fixes
    + Fix illegal read when parsing some numbers in scientific
      notation with huge exponents.
    + Correctly evaluate numbers in scientific notation with large
      exponents.
    + Correctly evaluate numbers with many leading zeros.
  * Bug fixes found with fuzzing
    + Fix a small memory leak in the parser.
    + Fix illegal read in the parser on specially crafted input
      with \t tags.

-------------------------------------------------------------------
Wed Oct  5 08:22:48 UTC 2016 - tchvatal@suse.com

- Update 0.13.4:
  * Fuzzing fixes bnc#1002982 CVE-2016-7969, CVE-2016-7970 and
    CVE-2016-7972
  * Improve compatibility/portablility of build system, should fix e.g. compilation on Solaris.
  * Fix memory leak in DirectWrite font provider.
  * Fix the rasterizer when rendering some large outlines.
  * Remove hack that forced RTL base direction depending on font encoding.
  * Greatly improve the internal caches with refcounting and gradual clearing.

-------------------------------------------------------------------
Thu Feb 25 16:22:23 UTC 2016 - mpluskal@suse.com

- Update to 0.13.2
  * Add ass_set_check_readorder() API function to disable use of 
    the ReadOrder field for duplicate checking in 
    ass_process_chunk().
  * ass_step_sub(track, now, 0) now finds the start of the subtitle 
    at now.
  * Bug fixes:
    + Fix an issue with the new duplicate checking, which could 
      lead to missing subtitles after seeking.
    + Fix a crash with Core Text under specific circumstances.

-------------------------------------------------------------------
Thu Dec 17 11:42:06 UTC 2015 - idonmez@suse.com

- Update to 0.13.1
  * Much faster duplicate event check in ass_process_chunk. 
  * Interpret negative timestamp components as actual negative numbers.
  * Look up fonts that contain PostScript outlines by their PostScript
    name instead of their full names. Family names continue to be used for all fonts.
  * Keep spaces in font names
  * Drop support for Fontconfig < 2.10.92 to ensure correct font matching
  * Fix some small memory leaks, potential crashes and bad data uses 

-------------------------------------------------------------------
Wed Dec 16 16:37:26 UTC 2015 - jengelh@inai.de

- Update summary/description

-------------------------------------------------------------------
Mon Dec  7 20:26:25 UTC 2015 - jengelh@inai.de

- Update RPM groups

-------------------------------------------------------------------
Sun Nov 22 22:00:00 UTC 2015 - mpluskal@suse.com

- Update to 0.13.0
  * Add native font selection backends for OSX (CoreText) and 
		Windows (DirectWrite). You can now run libass without 
		fontconfig on these platforms. This fixes problems with 
		fontconfig behaving badly on these platforms (it could take 
		minutes to scan all system fonts). Even on Linux, this speeds 
		up loading of embedded fonts (such as provided by 
		ass_add_font()). The DirectWrite backend only works on Windows 
		Vista and later. On XP, fontconfig is still needed. libass can 
		be compiled with both DirectWrite and fontconfig, and then it 
		will fallback to fontconfig automatically if DirectWrite is not 
		available at runtime.
	* Add ass_get_available_font_providers() API function.
	* Change the 4th parameter of ass_set_fonts(). This now selects 
		the font provider. This is somewhat backwards compatible with 
		the old behavior, but if you ever passed values other than 0 
		or 1, your application might break with this libass release.
	* The ass_fonts_update() function now does nothing. It's kept 
		for backward compatibility only.
	* Much faster gaussian blur. This can bring a large speedup with 
		big blurred signs and such.
	* Drop ENCA support
	* Bug fixes
		+ Fix compilation of the freetype rasterizer (disabled by 
			default)
		+ Fix rendering with some cases of consecutive line breaks 
			(\N\N).
		+ Fix some memory allocation failure checks
		+ Avoid system locale dependent behavior by reinventing some 
			standard C functions
		+ Fix rendering errors with strikes (GH #193)
		+ MSVC compilation fixes
- Enable harfbuzz and yasm build requires

-------------------------------------------------------------------
Tue Jul 14 11:20:05 UTC 2015 - mpluskal@suse.com

- Update to 0.12.3
  * VSFilter blur compatibility changes
  * VSFilter color/alpha parsing compatibility changes
  * Bugfixes
    + Fix some potential memory leaks and crashes
    + Fix large timestamps (larger than about 600 hours)
    + Fix a potential crash with the new rasterizer and complex 
      fonts
    + Do not apply user-configured line position to non-dialog
      events
    + Strictly clip non-dialog events to the video rectangle
- Changes for 0.12.2
  * Add extern "C" guards to the public headers for C++ 
    compatibility
  * Improvements to style override API and implementation
  * Bugfixes
    + Fix some rasterizer bugs of unknown severity
    + Fix a broken memset() of unknown severity
    + Make timestamp parsing more lenient, which makes libass 
      accept invalid
    + files accepted by most other ASS parsers
    + Increase compatibility with broken ASS drawings accepted by 
      VSFilter

-------------------------------------------------------------------
Fri Jan 30 12:18:00 UTC 2015 - p.drouand@gmail.com

- Update to version 0.12.1 (bnc#921212)
  * Make ASS drawings with an extremely high number of control points work
    This change increases compatibility with VSFilter.
  * Bugfixes
    - Fix a crash when using newer harfbuzz versions (GH #155)
    - Load embedded memory fonts correctly
    - Large shadow offsets rendered incorrectly (GH #142)
    - Fix incorrect closing of last drawing contour
    - Fix issues with undefined behavior in shifts
    - Lots of of malloc() error checking
- Update home and download source Url to new project home
- Add baselibs.conf in sources list for Factory

-------------------------------------------------------------------
Sun Oct  5 10:37:03 UTC 2014 - olaf@aepfle.de

- add versioned Requires to devel pkg, enca is required by pkgconfig

-------------------------------------------------------------------
Thu Oct 17 19:17:09 UTC 2013 - tchvatal@suse.com

- Version bup to 0.10.2
  * API additions and some ABI changes, bumped SONAME
  * Expose header field for VSFilter color mangling (GC #87)
  * Add functions for explicit scaling hints (GC #6)
  * Add 'type' field to ASS_Image (GC #31)
  * Workaround Freetype's font size grid-fitting where appropriate
  * Apply blur to both glyph and border when using opaque box
  * Parser bugfixes
    * Parse angles in style as number
    * Fix fallback event formats
    * Make closing ')' optional for some tags
    * Fix crazy VSFilter behaviour for \move (GC #90)
    * Make \r fall back to line style (GC #104)
    * Parse style name like VSFilter
    * Ignore junk in nested \t tags
    * Make \clip tag arguments mandatory
  * Properly handle script and language in the HarfBuzz shaper
  * Allow ASS_Track and ASS_Renderer to have different ASS_Library
  * Fix stroking in some rare cases
  * Correctly handle @font advance (GC #94)
  * Fix ascent/descent for some fonts (GC #106)
  * Fix ass_step_sub to not require sorted events
  * Fix blur scaling
  * Don't mutate input strings (GC #88)
  * Remove/change some error messages
  * Fix various small memory leaks

-------------------------------------------------------------------
Tue Oct 23 22:07:26 UTC 2012 - reddwarf@opensuse.org

- Update to version 0.10.1
  * Fix letter spacing
  * Add \rSTYLENAME syntax support (GC #43)
  * Fix border generation and border style reset (GC #56)
  * Fix various issues with bug-for-bug compatibility of
    transformations (\fax, \fay, etc.) to VSFilter (GC #46, GC #42)
  * Fix drawing parsing (GC #47)
  * Various fixes to shaper (GC #54, GC #55, GC#59)
  * Fix change detection
  * Add ass_set_line_position API to set a vertical subtitle offset
  * Fix scaling of drawing baseline offset (\pbo) values
  * Fix skipping of zero-width characters for FriBiDi shaper
  * Use LTR text base direction by default, similar to VSFilter

-------------------------------------------------------------------
Sat Oct 29 19:01:04 UTC 2011 - reddwarf@opensuse.org

- Update to version 0.10.0
  + Bidirectional layout and Arabic shaping via FriBidi (GC #13)
  + Add API for shaper configuration
  + Add support for `Language' Script Info property, this can be
    used for hinting the text language
  + Vertical layout improvements
    * Use `vert' and `vkna' OpenType features for vertical glyph
      variants
    * Position rotated glyphs onto baseline
  + Parse font encoding property for base text direction hinting
  + Refactor cache system
  + Use generic outlines in place of FreeType glyphs
  + Direct outline bitmap rendering
  + Fix whitespace trimming (GC #35)
  + Do not render border if there's no shadow or glyph (GC #29)
  + Adjust spacing after a italic to non-italic style change (GC #37)
  + Fix fade timing
  + Fix x positioning with borders (GC #27)
- Use pkgconfig() BuildRequires
- Parameterize the soname number

-------------------------------------------------------------------
Thu Jun 30 11:53:23 CEST 2011 - dimstar@opensuse.org

- Update to version 0.9.12:
  + Switch to permissive (ISC) license
  + Support \fs+ and \fs- syntax for modifying font size
  + Fix word-wrapping
  + Improved charmap fallback matching
  + Handle a few more VSFilter quirks correctly
  + Add a sensible default style
  + Fix compilation against libpng 1.5
- Add xz BuildRequires, as the new tarballs are xz compressed
- Change License tag from GPLv2+ to ISC, as upstream changed to
  this license.
- Do not use source service, as Factory is moving away from it in
  favor of a different implementation to verify authenticity of
  tarballs. Consequently add a valid URL as Source.

-------------------------------------------------------------------
Fri Apr 14 13:38:21 UTC 2011 - toddrme2178@gmail.com

- added 32bit compatibility libraries
- updated description to official description (fix for RPMLINT warning)

-------------------------------------------------------------------
Wed Dec  8 20:09:08 UTC 2010 - cristian.rodriguez@opensuse.org

- fix -devel package dependencies 

-------------------------------------------------------------------
Fri Sep 10 23:58:47 UTC 2010 - reddwarf@opensuse.org

- Update to version 0.9.11
 * Fix serious memory leaks
 * Reduce frame/drawing initialization overhead
 * Basic (incorrect, but working) support for @font vertical text layout
 * Fix multiple faces per font attachment
 * charmap selection fixes
 * Add ass_flush_events API function
 * Improve fullname font matching
 * Better PAR correction if text transforms are used
 * Calculate drawing bounding box like VSFilter
 * Performance improvements
   * Cache vector clip masks
   * Avoid unnecessary glyph copies
 * Various rendering fixes
 * Parse numbers in a locale-independent way
 * Remove support for freetype < 2.2.1, fontconfig < 2.4.1; this especially
   means libass will not extract fonts into the file system anymore
 * Disable script file size limit
 * Match fonts against the full name ("name for humans")
 * Reset clip mode after \iclip
 * Improve VSFilter compatibility
 * Update API documentation
 * A couple of smaller fixes and cleanups
 * Support \q override tag
 * Support wrap style 1 (i.e. wrap, but do not equalize line lengths)
 * Support border style 3 (opaque box)
 * Use the event bounding box (instead of vertical position and height) for
   collision detection
 * Embold glyphs if no bold variant is available, but was requested
 * Modify \fax to be similar to VSFilter
 * Trim spaces after line wrapping
 * Fix border/shadow overlap combining in some cases
 * Disable kerning by default.  Use "Kerning=yes" style override or
   "Kerning: yes" in [Script Info] to enable it
 * Slight bitmap handling optimizations
 * Various bugfixes

-------------------------------------------------------------------
Sat Sep 26 15:05:54 UTC 2009 - cmorve69@yahoo.es

- Remove unneeded autoreconf
- Correct license tag
- Make build more verbose so checking tools can do its work

-------------------------------------------------------------------
Tue Aug 11 19:17:31 CEST 2009 - sbrabec@suse.cz

- New SuSE package, version 0.9.7.
- Name and split according to shared library packaging policy.

-------------------------------------------------------------------
Sat Mar 21 12:00:00 UTC 2009 - cmorve69@yahoo.es

- and now to the OBS
- update to 0.9.6

-------------------------------------------------------------------
Wed Sep 18 12:00:00 UTC 2008 - detlef@links2linux.de

- initial build for PackMan to be compatible with vlc-repo

-------------------------------------------------------------------
Tue Aug 26 12:00:00 UTC 2008 - dominique-rpm@leuenberger.net

- Initial package for version 0.9.5

