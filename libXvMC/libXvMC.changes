-------------------------------------------------------------------
Sun Oct 20 18:24:27 UTC 2019 - Stefan Brüns <stefan.bruens@rwth-aachen.de>

- Add conflicts for old xorgproto-devel, X11/extensions/vldXvMC.h
  was moved to libXvMC-devel.

-------------------------------------------------------------------
Mon Sep 30 09:54:48 UTC 2019 - Stefan Dirsch <sndirsch@suse.com>

- Update to version 1.0.12
  * This release fixes the pkgconfig data to not refer to libXv, 
    adds a pkgconfig file for libXvMCW, and prepares for a future
    xorgproto release. There should be no functional changes.
- n_bring-back-libXv-dep.patch
  * fixes build of Mesa's xvmc gallium driver, since that 
    references in XvMCCreateContex also XvQueryAdaptors, 
    XvFreeAdaptorInfo and XvFreeAdaptorInfo

-------------------------------------------------------------------
Fri Mar 22 11:21:33 UTC 2019 - Stefan Dirsch <sndirsch@suse.com>

- Update to version 1.0.11
  * autogen: add default patch prefix
  * autogen.sh: use quoted string variables
  * autogen.sh: use exec instead of waiting for configure to finish
  * Update configure.ac bug URL for gitlab migration
  * Need to check for -1, not 0, to determine if shmat() failed
  * Fix sign comparison warnings for loop indexes
  * Fix handling of shmKey in XvMCGetDRInfo
  * Update README for gitlab migration

-------------------------------------------------------------------
Fri Oct 28 23:51:46 UTC 2016 - tobias.johannes.klausmann@mni.thm.de

- Update to version 1.0.10:
  + Avoid buffer underflow on empty strings.

-------------------------------------------------------------------
Sat Mar 21 20:31:13 UTC 2015 - sndirsch@suse.com

- added baselibs.conf as source to specfile

-------------------------------------------------------------------
Fri Mar 20 21:13:07 UTC 2015 - tobias.johannes.klausmann@mni.thm.de

- Update to version 1.0.9:
  + Require ANSI C89 pre-processor, drop pre-C89 token pasting support
  + Rename local err variable in XW_RSYM macro to avoid shadow warnings
  + Remove fallback for _XEatDataWords, require libX11 1.6 for it
  + Fix linking with -Wl,--no-undefined on Linux

-------------------------------------------------------------------
Fri Jun 14 12:28:11 UTC 2013 - tobias.johannes.klausmann@mni.thm.de

- Update to version 1.0.8:
  This bug fix release provides the fixes for the recently announced security
  issues CVE-2013-1990 & CVE-2013-1999, and the fixes for the bugs introduced
  in the initial set of patches for those security issues.

-------------------------------------------------------------------
Sun Feb 17 17:21:53 UTC 2013 - jengelh@inai.de

- Use more robust make install call

-------------------------------------------------------------------
Wed Apr 11 15:45:33 UTC 2012 - vuntz@opensuse.org

- Update to version 1.0.7:
  + Janitorial cleanups
  + Build configuration improvements

-------------------------------------------------------------------
Tue Feb  7 22:17:49 UTC 2012 - jengelh@medozas.de

- Split xorg-x11-libs into separate packages
