-------------------------------------------------------------------
Wed Jun  2 08:42:23 UTC 2021 - Christophe Giboudeaux <christophe@krop.fr>

- Fix the %doc files. README.rst is a symlink pointing to
  docs/intro.rst.

-------------------------------------------------------------------
Mon Oct 12 14:12:58 UTC 2020 - Marketa Calabkova <mcalabkova@suse.com>

- Remove nonsense keyword (breaks pytest)

-------------------------------------------------------------------
Fri Jul 17 07:47:05 UTC 2020 - Dirk Mueller <dmueller@suse.com>

- update to 0.2.5
  * Do not depend on pkg_resources module for list_versions() function.

-------------------------------------------------------------------
Sun Jun 14 09:00:42 UTC 2020 - Dirk Mueller <dmueller@suse.com>

- update to 0.2.4
  * minor "bugfix" to avoid using pkg_resources module on import, 7918f58
  * may help xonsh xonsh/xonsh#3607

-------------------------------------------------------------------
Mon Jun  8 13:45:57 UTC 2020 - Dirk Mueller <dmueller@suse.com>

- update to 0.2.3
  * add tests

-------------------------------------------------------------------
Mon Jun  8 07:13:36 UTC 2020 - Tomáš Chvátal <tchvatal@suse.com>

- Add missing python2 dependency for Leap to work

-------------------------------------------------------------------
Tue Jun  2 06:52:39 UTC 2020 - Marketa Calabkova <mcalabkova@suse.com>

- Update to version 0.2.2
  * Enhancement: Unicode version may be selected by exporting the 
    Environment variable UNICODE_VERSION, such as 13.0, or 6.3.0. 
    See the jquast/ucs-detect CLI utility for automatic detection.
  * Enhancement: API Documentation is published to readthedocs.org.
  * Updated tables for all Unicode Specifications with files published 
    in a programmatically consumable format.

-------------------------------------------------------------------
Wed May  6 02:25:29 UTC 2020 - Steve Kowalik <steven.kowalik@suse.com>

- Convert to multibuild to break a self-requires loop. 

-------------------------------------------------------------------
Wed Mar 25 14:31:41 UTC 2020 - pgajdos@suse.com

- version update to 0.1.9
  * Performance optimization by @avylove , PR #35.
  * Updated tables to Unicode Specification 13.0.0.

-------------------------------------------------------------------
Thu Feb  6 14:56:55 UTC 2020 - Marketa Calabkova <mcalabkova@suse.com>

- update to 0.1.8
  * Unicode v12 and proj. maintenence
  * remove static analysis
  * EastAsianWidth v9 -> v12
  * README and tox.ini

-------------------------------------------------------------------
Wed Oct  2 04:39:59 UTC 2019 - John Vandenberg <jayvdb@gmail.com>

- Re-active test suite
- Remove test suite from runtime package
- Run fdupes

-------------------------------------------------------------------
Thu Mar  7 11:18:22 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Fix test macro expansion

-------------------------------------------------------------------
Thu Sep  6 10:10:27 UTC 2018 - Tomáš Chvátal <tchvatal@suse.com>

- Drop devel dependency
- Install license file

-------------------------------------------------------------------
Mon Apr  3 14:32:58 UTC 2017 - toddrme2178@gmail.com

- Fix source URL.

-------------------------------------------------------------------
Sat Apr  1 21:00:22 UTC 2017 - toddrme2178@gmail.com

- Update to version 0.1.7
  * **Updated** tables to Unicode Specification 9.0.0. (`PR #18`_).
- Update to version 0.1.6
0.1.6 *2016-01-08 Production/Stable*
  * ``LICENSE`` file now included with distribution.
- Update to version 0.1.5
  * **Bugfix**:
    Resolution of "combining_ character width" issue, most especially
    those that previously returned -1 now often (correctly) return 0.
    resolved by `Philip Craig`_ via `PR #11`_.
  * **Deprecated**:
    The module path ``wcwidth.table_comb`` is no longer available,
    it has been superseded by module path ``wcwidth.table_zero``.
- Implement single-spec version
    
-------------------------------------------------------------------
Mon Jan 30 21:50:06 UTC 2017 - rjschwei@suse.com

- Include in SLE 12 (bsc#1002895, FATE#321630)

-------------------------------------------------------------------
Wed May  6 15:17:14 UTC 2015 - toddrme2178@gmail.com

- Initial version
  + Version 0.1.4
