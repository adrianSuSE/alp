#
# RPM macros for gconf applications.
#
# (c) 2006-2007 SUSE LINUX
# Author: Stanislav Brabec <sbrabec@suse.cz>
#
# Usage:
#
# Preamble:
# %gconf_schemas_requires
# (note: %gconf_schemas_prereq works, but is deprecated)
#
# %install
# ...
# %find_gconf_schemas
# or
# %def_gconf_schemas subpackage1
# %add_gconf_schemas schemas_name1
# %add_gconf_schemas schemas_name2
# %def_gconf_schemas subpackage2
# %add_gconf_schemas schemas_name3
# %add_gconf_schemas schemas_name4
# %end_gconf_schemas
# schemas names are without path and suffix
#
# %pre -f %{name}.schemas_pre
#
# %preun -f %{name}.schemas_preun
#
# %posttrans -f %{name}.schemas_posttrans
#
# %files -f %{name}.schemas_list
# or
# %files -f subpackage.schemas_list
#
#
# In case of more -f needed for one %files:
#
# %install
# ...
# %find_gconf_schemas
# %find_lang %{name}
#
# %files -f %{name}.schemas_list -f %{name}.lang
#

%gconf_schemas_requires \
Requires(pre): /usr/bin/gconftool-2 coreutils diffutils \
Requires(preun): /usr/bin/gconftool-2 coreutils diffutils \
Requires(posttrans): /usr/bin/gconftool-2 coreutils diffutils

# This macro is deprecated. Use %gconf_schemas_requires instead.
%gconf_schemas_prereq PreReq: /usr/bin/gconftool-2 coreutils diffutils

%end_gconf_schemas() \
echo 'if test "x$UNINSTALL_SCHEMA_FILES" != "x"; then\
    /usr/bin/gconftool-2 --makefile-uninstall-rule $UNINSTALL_SCHEMA_FILES >/dev/null\
    rm -f $UNINSTALL_SCHEMA_FILES\
fi\
if test "x$INSTALL_SCHEMA_FILES" != "x"; then\
    /usr/bin/gconftool-2 --makefile-install-rule $INSTALL_SCHEMA_FILES >/dev/null\
fi\
rmdir /usr/share/GConf/schemas/outdated 2>/dev/null || true' >>$GCONF_SCHEMAS_NAME.schemas_posttrans\
echo '    rmdir /usr/share/GConf/schemas/outdated 2>/dev/null || true\
fi' >>$GCONF_SCHEMAS_NAME.schemas_preun\
%{nil}

%def_gconf_schemas() \
if test -n "$GCONF_SCHEMAS_NAME" ; then\
    %end_gconf_schemas\
fi\
GCONF_SCHEMAS_NAME=%1\
echo '%defattr (644, root, root, 755)' >%1.schemas_list\
echo 'mkdir -p /usr/share/GConf/schemas/outdated' >%1.schemas_pre\
echo 'export GCONF_CONFIG_SOURCE=`/usr/bin/gconftool-2 --get-default-source`\
INSTALL_SCHEMA_FILES=\
UNINSTALL_SCHEMA_FILES=' >%1.schemas_posttrans\
echo 'if test "$1" = "0"; then\
    if test -x /usr/bin/gconftool-2 ; then\
        export GCONF_CONFIG_SOURCE=`/usr/bin/gconftool-2 --get-default-source`\
    fi' >%1.schemas_preun\
%{nil}

%_add_gconf_schemas() \
echo $SCHEMAS >>$GCONF_SCHEMAS_NAME.schemas_list\
echo "if test -f ${SCHEMAS} ; then\
    cp -f ${SCHEMAS} /usr/share/GConf/schemas/outdated/\
elif test -f /usr/share/gconf/schemas/${SCHEMAS#/usr/share/GConf/schemas/} ; then\
    # Migration from /usr/share/gconf/schemas to /usr/share/GConf/schemas. Can be removed for openSUSE 11.4+3\
    cp -f /usr/share/gconf/schemas/${SCHEMAS#/usr/share/GConf/schemas/} /usr/share/GConf/schemas/outdated/\
elif test -f /etc/gconf/schemas/${SCHEMAS#/usr/share/GConf/schemas/} ; then\
    # Migration from /etc/gconf/schemas to /usr/share/GConf/schemas. Can be removed for openSUSE 12.2\
    cp -f /etc/gconf/schemas/${SCHEMAS#/usr/share/GConf/schemas/} /usr/share/GConf/schemas/outdated/\
fi" >>$GCONF_SCHEMAS_NAME.schemas_pre\
echo "if test -f /usr/share/GConf/schemas/outdated/${SCHEMAS#/usr/share/GConf/schemas/}; then\
    cmp --quiet /usr/share/GConf/schemas/outdated/${SCHEMAS#/usr/share/GConf/schemas/} ${SCHEMAS}\
    if test \\\$? != 0; then\
        UNINSTALL_SCHEMA_FILES=\\\"\\\$UNINSTALL_SCHEMA_FILES /usr/share/GConf/schemas/outdated/${SCHEMAS#/usr/share/GConf/schemas/}\\\"\
        INSTALL_SCHEMA_FILES=\\\"\\\$INSTALL_SCHEMA_FILES ${SCHEMAS}\\\"\
    else\
        rm -f /usr/share/GConf/schemas/outdated/${SCHEMAS#/usr/share/GConf/schemas/}\
    fi\
else\
  INSTALL_SCHEMA_FILES=\\\"\\\$INSTALL_SCHEMA_FILES ${SCHEMAS}\\\"\
fi" >>$GCONF_SCHEMAS_NAME.schemas_posttrans\
echo "    if test -x /usr/bin/gconftool-2 ; then\
        if test -f /usr/share/GConf/schemas/outdated/${SCHEMAS#/usr/share/GConf/schemas/} ; then\
            /usr/bin/gconftool-2 --makefile-uninstall-rule /usr/share/GConf/schemas/outdated/${SCHEMAS#/usr/share/GConf/schemas/} >/dev/null\
        elif test -f ${SCHEMAS} ; then\
            /usr/bin/gconftool-2 --makefile-uninstall-rule ${SCHEMAS} >/dev/null\
        fi\
    fi\
    rm -f /usr/share/GConf/schemas/outdated/${SCHEMAS#/usr/share/GConf/schemas/}" >>$GCONF_SCHEMAS_NAME.schemas_preun\
%{nil}

%add_gconf_schemas() \
mkdir -p $RPM_BUILD_ROOT/usr/share/GConf/schemas\
SCHEMAS=/usr/share/GConf/schemas/%1.schemas\
mv $RPM_BUILD_ROOT/etc/gconf/schemas/%1.schemas ${RPM_BUILD_ROOT}${SCHEMAS}\
%_add_gconf_schemas\
%{nil}

%find_gconf_schemas() \
test -d $RPM_BUILD_ROOT/etc/gconf/schemas\
mkdir -p $RPM_BUILD_ROOT/usr/share/GConf/schemas\
%def_gconf_schemas %{name}\
for SCHEMAS in $RPM_BUILD_ROOT/etc/gconf/schemas/*.schemas ; do\
    test -f $SCHEMAS\
    SCHEMASFILENAME=${SCHEMAS#$RPM_BUILD_ROOT/etc/gconf/schemas/}\
    NEWSCHEMAS=/usr/share/GConf/schemas/$SCHEMASFILENAME\
    mv $SCHEMAS ${RPM_BUILD_ROOT}${NEWSCHEMAS}\
    SCHEMAS=$NEWSCHEMAS\
    %_add_gconf_schemas\
done\
%end_gconf_schemas\
%{nil}
