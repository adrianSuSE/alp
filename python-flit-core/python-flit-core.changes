-------------------------------------------------------------------
Sun Jan  9 14:53:21 UTC 2022 - Ben Greiner <code@bnavigator.de>

- Back to bootstrap without pip

-------------------------------------------------------------------
Sun Jan  9 11:38:59 UTC 2022 - Ben Greiner <code@bnavigator.de>

- Update to version 3.6
  * flit_core now bundles the tomli TOML parser library (version
    1.2.3) to avoid a circular dependency between flit_core and
    tomli (:ghpull:`492`). This means flit_core now has no
    dependencies except Python itself, both at build time and at
    runtime, simplifying :doc:`bootstrapping <bootstrap>`.
- The above is not fully applicale to openSUSE, we debundle.
- Release notes for Version 3.5.1
  * Fix development installs with flit install --symlink and
    --pth-file, which were broken in 3.5.0, especially for packages
    using a src folder (:ghpull:`472`).
- Release notes for Version 3.5
  * You can now use Flit to distribute a module or package inside a
    namespace package (as defined by PEP 420). To do this, specify
    the import name of the concrete, inner module you are packaging
    - e.g. name = "sphinxcontrib.foo" - either in the [project]
    table, or under [tool.flit.module] if you want to use a
    different name on PyPI (:ghpull:`468`).
  * Flit no longer generates a setup.py file in sdists (.tar.gz
    packages) by default (:ghpull:`462`). Modern packaging tools
    don't need this. You can use the --setup-py flag to keep adding
    it for now, but this will probably be removed at some point in
    the future.
  * Fixed how flit init handles authors' names with non-ASCII
    characters (:ghpull:`460`).
  * When flit init generates a LICENSE file, the new pyproject.toml
    now references it (:ghpull:`467`).
- Do not package tests
- Make it work with old python-rpm-macros

-------------------------------------------------------------------
Fri Oct 15 19:27:43 UTC 2021 - Ben Greiner <code@bnavigator.de>

- Break build cycle by using upstream recommended bootstrap method
  https://flit.readthedocs.io/en/latest/bootstrap.html

-------------------------------------------------------------------
Fri Oct 15 17:32:51 UTC 2021 - Ben Greiner <code@bnavigator.de>

- Update to Version 3.4
  * Python 3.6 or above is now required, both for flit and
    flit_core.
  * Add a --setup-py option to flit build and flit publish,
    and a warning when neither this nor --no-setup-py are
    specified (PR #431). A future version will stop
    generating setup.py files in sdists by default.
  * Add support for standardised editable installs - pip
    install -e - according to PEP 660 (PR #400).
  * Add a --pypirc option for flit publish to specify an
    alternative path to a .pypirc config file describing
    package indexes (PR #434).
  * Fix installing dependencies specified in a [project]
    table (PR #433)
  * Fix building wheels when SOURCE_DATE_EPOCH (see
    Reproducible builds) is set to a date before 1980 (PR
    #448).
  * Switch to using the tomli TOML parser, in common with
    other packaging projects (PR #438). This supports TOML
    version 1.0.
  * Add a document on Bootstrapping (PR #441).
- Release Version 3.3
  * PKG-INFO files in sdists are now generated the same way
    as METADATA in wheels, fixing some issues with sdists
    (PR #410).
  * flit publish now sends SHA-256 hashes, fixing uploads
    to GitLab package repositories (PR #416).
  * The [project] metadata table from PEP 621 is now fully
    supported and documented. Projects using this can now
    specify requires = ["flit_core >=3.2,<4"] in the
    [build-system] table.
- Multibuild: break another depcycle with
  pytest/importlib-metadata/tomli

-------------------------------------------------------------------
Wed Apr 14 19:29:23 UTC 2021 - Matthias Bach <marix@marix.org>

- Update to version 3.2
  * Experimental support for specifying metadata in a [project] in
    pyproject.toml table as specified by PEP-621.
  * Fix writing METADATA file with multi-line information in
    certain fields such as Author.
  * Fix building wheel when a directory such as LICENSES appears
    in the project root directory.
  * Switch from the deprecated pytoml package to toml.

-------------------------------------------------------------------
Mon Oct 26 22:38:40 UTC 2020 - Benjamin Greiner <code@bnavigator.de>

- Update to version 3.0
  * flit_core once again requires Python 3 (>=3.4). Packages that
    support Python 2 can still be built by flit_core 2.x, but can't
    rely on new features (:ghpull:`342`).
- Kill dephell dependency

-------------------------------------------------------------------
Mon Mar 23 18:13:44 UTC 2020 - Matej Cepl <mcepl@suse.com>

- Add macro %dephell_genspec to generate setup.py

-------------------------------------------------------------------
Sun Mar 15 10:01:34 UTC 2020 - Tomáš Chvátal <tchvatal@suse.com>

- Fix generating with new dephell

-------------------------------------------------------------------
Wed Dec 18 23:33:33 UTC 2019 - John Vandenberg <jayvdb@gmail.com>

- Remove dephell build workaround

-------------------------------------------------------------------
Mon Dec 16 12:53:01 PM UTC 2019 - John Vandenberg <jayvdb@gmail.com>

- Initial spec for v2.1.0
