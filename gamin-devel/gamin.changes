-------------------------------------------------------------------
Tue Sep 28 17:11:06 UTC 2021 - Michael Gorse <mgorse@suse.com>

- Fix source URI.

-------------------------------------------------------------------
Thu Jan  9 13:23:06 UTC 2020 - Dominique Leuenberger <dimstar@opensuse.org>

- Disable python bindings (python2-only):
  + Drop python-devel BuildRequires.
  + Drop gamin-fix_python_main.patch: no longer needed.
  + No longer build python-gamin sub-package.

-------------------------------------------------------------------
Mon Aug  6 09:33:25 UTC 2018 - dimstar@opensuse.org

- Add baselibs.conf to the file source list.

-------------------------------------------------------------------
Wed Jul 25 16:26:53 UTC 2018 - dimstar@opensuse.org

- Replace deprecated macro py_sitedir with python_sitearch.

-------------------------------------------------------------------
Fri Jan 10 10:08:35 UTC 2014 - adrian@suse.de

- split lib* packages into own spec file to avoid a build cycle
- found a testsuite, run it, but ignore result since it show
  breakages. Should be revisited.

-------------------------------------------------------------------
Tue Sep 24 21:33:01 UTC 2013 - hrvoje.senjan@gmail.com

- Added 0001-Poll-files-on-nfs4.patch (bgo#693006) and 
  gamin-0.1.11-double-lock.patch (bgo#669292)

-------------------------------------------------------------------
Fri Apr 26 11:01:11 UTC 2013 - coolo@suse.com

- add conflict with libfam0-32bit

-------------------------------------------------------------------
Thu Jan  5 11:07:49 UTC 2012 - toddrme2178@gmail.com

- Spec file cleanups
- Provide and obsolete libgamin in case of sonum changes
- Have the -devel package provide -devel-static as well since it has .a files
- Changed the baselibs.conf to use the libs per openSUSE policy
- Added duplicate files check

-------------------------------------------------------------------
Wed Jan  4 18:47:50 UTC 2012 - jengelh@medozas.de

- Use exact EVR for Provide: symbols

-------------------------------------------------------------------
Wed Jan  4 13:59:21 UTC 2012 - toddrme2178@gmail.com
  
- Added version for fam-server provides
- Added obsoletes for fam-server
- Added obsoletes for gamin-python
- Switched to original gzipped tar
- Changed source to include upstream source URL to comply with new
  packaging guidelines

-------------------------------------------------------------------
Sun Oct  2 11:17:22 UTC 2011 - toddrme2178@gmail.com

- Added patch to fix building in factory
- Cleaned up spec file formatting with spec-cleaner
- Added proper license header to spec file
- Split documentation package (fix for RPMLINT warning)
- Add fam-server provides to gamin-server (other gaming packages provide their fam counterpart)

-------------------------------------------------------------------
Thu Sep 22 11:36:52 UTC 2011 - jengelh@medozas.de

- Implement shlib package (libfam0-gamin)
- Resolve build error due to source disabling deprecated contructs
  that it used

-------------------------------------------------------------------
Tue Sep  1 00:00:00 UTC 2009 - pascal.bleser@opensuse.org

- add gamin-32bit package through baselibs.conf to fix KDE 32bit compatibility libs (thanks to Martin Vogt for reporting), which requires splitting %{_libexecdir}/gam_server into its own subpackage (gamin-server) to avoid file conflicts when both gamin and gamin-32bit are installed -- note that gamin and gamin-32bit both require the package gamin-server, so nothing needs to be done on the user end


-------------------------------------------------------------------
Sat Jul 25 00:00:00 UTC 2009 - pascal.bleser@opensuse.org

- add patch to be completely ABI compatible with fam, in order to avoid warnings and crashes caused by "Symbol `FamErrlist' has different size in shared object, consider re-linking"; references:
  * http://lists.opensuse.org/yast-devel/2009-02/msg00000.html
  * http://www.nabble.com/Bug-437307:-lighttpd-fails-to-restart-after-update-td12107383.html


-------------------------------------------------------------------
Thu Mar 12 00:00:00 UTC 2009 - detlef@links2linux.de

- add return.patch

-------------------------------------------------------------------
Sat Jan  3 00:00:00 UTC 2009 - detlef@links2linux.de

- new upstream version <0.1.10>

-------------------------------------------------------------------
Wed Jul 16 00:00:00 UTC 2008 - detlef@links2linux.de

- initial build for PackMan

-------------------------------------------------------------------
Thu Oct 27 00:00:00 UTC 2005 - veillard@redhat.com

- hopefully fixes gam_server crashes
- some portability fixes
- removed a minor leak

-------------------------------------------------------------------
Thu Sep  8 00:00:00 UTC 2005 - veillard@redhat.com

- revamp of the inotify back-end
- memory leak fix
- various fixes and cleanups

-------------------------------------------------------------------
Tue Aug  9 00:00:00 UTC 2005 - veillard@redhat.com

- Improvement of configuration, system wide configuration files and
  per filesystem type default
- Rewrite of the inotify back-end, reduce resources usage, tuning in
  case of busy resources
- Documentation updates
- Changes to compile inotify back-end on various architectures
- Debugging output improvements

-------------------------------------------------------------------
Tue Aug  2 00:00:00 UTC 2005 - veillard@redhat.com

- Fix to compile on older gcc versions
- Inotify back-end changes and optimizations
- Debug ouput cleanup, pid and process name reports
- Dropped kernel monitor bugfix
- Removed the old glist copy used for debugging
- Maintain mounted filesystems knowledge, and per fstype preferences

-------------------------------------------------------------------
Wed Jul 13 00:00:00 UTC 2005 - veillard@redhat.com

- inotify back end patches, ready for the new inotify support in kernel
- lot of server code cleanup patches
- fixed an authentication problem

-------------------------------------------------------------------
Fri Jun 10 00:00:00 UTC 2005 - veillard@redhat.com

- gamin_data_conn_event fix
- crash from bug gnome #303932
- Inotify and mounted media #171201
- mounted media did not show up on Desktop #159748
- write may not be atomic
- Monitoring a directory when it is a file
- Portability to Hurd/Mach and various code cleanups
- Added support for ~ as user home alias in .gaminrc

-------------------------------------------------------------------
Thu May 12 00:00:00 UTC 2005 - veillard@redhat.com

- Close inherited file descriptors on exec of gam_server
- Cancelling a monitor send back a FAMAcknowledge
- Fixed for big files > 2GB
- Bug when monitoring a non existing directory
- Make client side thread safe
- Unreadable directory fixes
- Better flow control handling
- Updated to latest inotify version: 0.23-6

-------------------------------------------------------------------
Tue Mar 15 00:00:00 UTC 2005 - veillard@redhat.com

- Fix an include problem showing up with gcc4</li>
- Fix the crash on failed tree assert bug #150471 based on patch from Dean Brettle
- removed an incompatibility with SGI FAM #149822

-------------------------------------------------------------------
Tue Mar  1 00:00:00 UTC 2005 - veillard@redhat.com

- Fix a configure problem reported by Martin Schlemmer
- Fix the /media/* and /mnt/* mount blocking problems from 0.0.24 e.g. #142637
- Fix the monitoring of directory using poll and not kernel

-------------------------------------------------------------------
Fri Feb 18 00:00:00 UTC 2005 - veillard@redhat.com

- more documentation
- lot of serious bug fixes including Gnome Desktop refresh bug
- extending the framework for more debug (configure --enable-debug-api)
- extending the python bindings for watching the same resource multiple times
  and adding debug framework support
- growing the regression tests a lot based on python bindings
- inotify-0.19 patch from John McCutchan
- renamed python private module to _gamin to follow Python PEP 8


-------------------------------------------------------------------
Tue Feb  8 00:00:00 UTC 2005 - veillard@redhat.com

- memory corruption fix from Mark on the client side
- extending the protocol and API to allow skipping Exists and EndExists
  events to avoid deadlock on reconnect or when they are not used.


-------------------------------------------------------------------
Mon Jan 31 00:00:00 UTC 2005 - veillard@redhat.com

- bit of python bindings improvements, added test
- fixed 3 bugs


-------------------------------------------------------------------
Wed Jan 26 00:00:00 UTC 2005 - veillard@redhat.com

- Added Python support
- Updated for inotify-0.18


-------------------------------------------------------------------
Thu Jan  6 00:00:00 UTC 2005 - veillard@redhat.com

- Frederic Crozat seems to have found the GList corruption which may fix
- Frederic Crozat also fixed poll only mode


-------------------------------------------------------------------
Fri Dec  3 00:00:00 UTC 2004 - veillard@redhat.com

- still chasing the loop bug, made another pass at checking GList,
  added own copy with memory poisonning of GList implementation.
- fixed a compile issue when compiling without debug


-------------------------------------------------------------------
Fri Nov 26 00:00:00 UTC 2004 - veillard@redhat.com

- still chasing the loop bug, checked and cleaned up all GList use
- patch from markmc to minimize load on busy apps


-------------------------------------------------------------------
Wed Oct 20 00:00:00 UTC 2004 - veillard@redhat.com

- chasing #132354, lot of debugging, checking and testing and a bit
  of refactoring


-------------------------------------------------------------------
Sat Oct 16 00:00:00 UTC 2004 - veillard@redhat.com

- workaround to detect loops and avoid the nasty effects, see RedHat bug #132354


-------------------------------------------------------------------
Sun Oct  3 00:00:00 UTC 2004 - veillard@redhat.com

- Found and fixed the annoying bug where update were not received
  should fix bugs ##132429, #133665 and #134413
- new mechanism to debug on-the-fly by sending SIGUSR2 to client or server
- Added documentation about internals


-------------------------------------------------------------------
Fri Oct  1 00:00:00 UTC 2004 - veillard@redhat.com

- applied portability fixes
- hardened the code while chasing a segfault


-------------------------------------------------------------------
Thu Sep 30 00:00:00 UTC 2004 - veillard@redhat.com

- potential fix for a hard to reproduce looping problem.


-------------------------------------------------------------------
Mon Sep 27 00:00:00 UTC 2004 - veillard@redhat.com

- update to the latest version of inotify
- inotify support compiled in by default
- fix ABI FAM compatibility problems #133162


-------------------------------------------------------------------
Tue Sep 21 00:00:00 UTC 2004 - veillard@redhat.com

- more documentation
- Added support for a configuration file $HOME/.gaminrc
- fixes FAM compatibility issues with FAMErrno and FamErrlist #132944


-------------------------------------------------------------------
Wed Sep  1 00:00:00 UTC 2004 - veillard@redhat.com

- fix crash with konqueror #130967
- exclude kernel (dnotify) monitoring for /mnt//* /media//*


-------------------------------------------------------------------
Thu Aug 26 00:00:00 UTC 2004 - veillard@redhat.com

- Fixes crashes of the gam_server
- try to correct the kernel/poll switching mode


-------------------------------------------------------------------
Tue Aug 24 00:00:00 UTC 2004 - veillard@redhat.com

- add support for both polling and dnotify simultaneously
- fixes monitoring of initially missing files
- load control on very busy resources #124361, desactivating
  dnotify and falling back to polling for CPU drain


-------------------------------------------------------------------
Thu Aug 19 00:00:00 UTC 2004 - veillard@redhat.com

- fixes simple file monitoring should close RH #129974
- relocate gam_server in $(libexec)


-------------------------------------------------------------------
Thu Aug  5 00:00:00 UTC 2004 - veillard@redhat.com

- Fix a crash when the client binary forks the gam_server and an
  atexit handler is run.


-------------------------------------------------------------------
Wed Aug  4 00:00:00 UTC 2004 - veillard@redhat.com

- should fix KDE build problems

