From f2ada8ea7939bc8f607d88a10c92d5bb96a700b8 Mon Sep 17 00:00:00 2001
From: Werner Fink <werner@suse.de>
Date: Fri, 7 May 2021 16:36:28 +0200
Subject: [PATCH] Use new statx(2) system call to avoid hangs on NFS

Signed-off-by: Werner Fink <werner@suse.de>
---
 Makefile.am   |   4 +-
 configure.ac  |  38 ++----
 src/fuser.c   | 102 ++++----------
 src/statx.c   | 149 ++++++++++++++++++++
 src/statx.h   |  68 ++++++++++
 src/timeout.c | 369 --------------------------------------------------
 src/timeout.h |  45 ------
 7 files changed, 260 insertions(+), 515 deletions(-)
 create mode 100644 src/statx.c
 create mode 100644 src/statx.h
 delete mode 100644 src/timeout.c
 delete mode 100644 src/timeout.h

diff --git Makefile.am Makefile.am
index 8067689..ea44203 100644
--- Makefile.am
+++ Makefile.am
@@ -65,8 +65,8 @@ src_fuser_SOURCES = \
 		    src/fuser.h \
 		    src/lists.h
 
-if WANT_TIMEOUT_STAT
-src_fuser_SOURCES += src/timeout.c src/timeout.h
+if HAVE_SYSCALL_STATX
+src_fuser_SOURCES += src/statx.c src/statx.h
 endif
 src_fuser_LDADD = @LIBINTL@
 src_killall_SOURCES = src/killall.c src/comm.h src/signals.c src/signals.h src/i18n.h
diff --git configure.ac configure.ac
index d4dee51..eda0e62 100644
--- configure.ac
+++ configure.ac
@@ -46,20 +46,6 @@ if test "$enable_selinux" = "yes"; then
 fi
 AC_SUBST([DL_LIB])
 
-# Call fork before all stat calls to stop hanging on NFS mounts
-AC_SUBST([WITH_TIMEOUT_STAT])
-AC_ARG_ENABLE([timeout_stat],
-  [AS_HELP_STRING([--enable-timeout-stat], [Use a timeout on stat calls (optional with argument "static" for a static background process)])],
-  [enable_timeout_stat=$enableval],
-  [enable_timeout_stat="no"])
-if test "$enable_timeout_stat" = "yes"; then
-  AC_DEFINE([WITH_TIMEOUT_STAT], [1], [Use timeout on stat calls])
-fi
-if test "$enable_timeout_stat" = "static"; then
-  AC_DEFINE([WITH_TIMEOUT_STAT], [2], [Use timeout on stat calls])
-fi
-AM_CONDITIONAL([WANT_TIMEOUT_STAT], [test "$enable_timeout_stat" = "static"])
-
 # Use /proc/self/mountinfo if available
 if test -e /proc/self/mountinfo ; then
   AC_DEFINE([HAS_MOUNTINFO], [1], [System has /proc/self/mountinfo which can used instead /proc(/self)/mounts])
@@ -73,16 +59,20 @@ fi
 AC_CHECK_FUNC([name_to_handle_at],[
   AC_DEFINE([HAS_NAME_TO_HANDLE_AT], [1], [System has name_to_handle_at(2) system call])])
 
-# Use string search for network based file systems but only if the system
-# has /proc/self/mountinfo
-AC_SUBST([WITH_MOUNTINFO_LIST])
-AC_ARG_ENABLE([mountinfo_list],
-  [AS_HELP_STRING([--enable-mountinfo-list], [Use the list in /proc/self/mountinfo to replace stat(2) syscall on network file systems shares])],
-  [enable_mountinfo_list="yes"],
-  [enable_mountinfo_list="no"])
-if test "$enable_mountinfo_list" = "yes" -a -e /proc/self/mountinfo ; then
-  AC_DEFINE([WITH_MOUNTINFO_LIST], [1], [Use list in /proc/self/mountinfo to replace stat calls])
-fi
+AC_CHECK_HEADERS([sys/syscall.h])
+AC_CHECK_DECLS([SYS_statx],
+  [has_syscall_statx="yes"],
+  [has_syscall_statx="no"],
+  [[#include <sys/syscall.h>]]
+)
+AC_CHECK_FUNCS([statx])
+# Check for linux specific statx(2) system call
+AC_SUBST([HAS_SYSCALL_STATX])
+AC_ARG_ENABLE([disable_statx],
+  [AS_HELP_STRING([--disable-statx], [Do not use linux specific statx(2) system call as replacement for stat(2), lstat(2), and fstat(2)])],
+  [enable_syscall_statx="no"],
+  [enable_syscall_statx=$has_syscall_statx])
+AM_CONDITIONAL([HAVE_SYSCALL_STATX], [test "$enable_syscall_statx" = "yes"])
 
 # Enable hardened compile and link flags
 AC_ARG_ENABLE([harden_flags],
diff --git src/fuser.c src/fuser.c
index dd928d2..d08f7a9 100644
--- src/fuser.c
+++ src/fuser.c
@@ -65,7 +65,7 @@
 #include "fuser.h"
 #include "signals.h"
 #include "i18n.h"
-#include "timeout.h"
+#include "statx.h"
 #include "comm.h"
 
 //#define DEBUG 1
@@ -127,10 +127,6 @@ static int get_fdinfo(const pid_t pid, const char *fd, struct fdinfo *info);
 static int get_mountid(const char *path);
 #endif
 static int find_mountpoint(const char *path, mntinfo_t **mountinfo);
-#if defined(WITH_MOUNTINFO_LIST)
-static int mntstat(const char *path, struct stat *buf);
-#endif
-static stat_t thestat = stat;
 static char *expandpath(const char *path);
 static struct unixsocket_list *unixsockets = NULL;
 static struct names *names_head = NULL, *names_tail = NULL;
@@ -518,7 +514,7 @@ int parse_file(struct names *this_name, struct inode_list **ino_list,
 			free(this_name->filename);
 		this_name->filename = strdup(new);
 	}
-	if (timeout(thestat, this_name->filename, &(this_name->st), 5) != 0 ||
+	if (statn(this_name->filename, STATX_INO|STATX_TYPE, &(this_name->st)) != 0 ||
 	    find_mountpoint(this_name->filename, &mountinfo) != 0) {
 		if (errno == ENOENT)
 			fprintf(stderr,
@@ -1252,9 +1248,7 @@ int main(int argc, char *argv[])
 					opts |= OPT_INTERACTIVE;
 					break;
 				case 'I':
-#if defined(WITH_MOUNTINFO_LIST)
 					opts |= OPT_ALWAYSSTAT;
-#endif
 					break;
 				case 'k':
 					opts |= OPT_KILL;
@@ -1320,10 +1314,11 @@ int main(int argc, char *argv[])
 			continue;
 		}
 
-#if defined(WITH_MOUNTINFO_LIST)
-		if ((opts & (OPT_MOUNTS|OPT_ALWAYSSTAT)) == OPT_MOUNTS)
-			thestat = mntstat;
+#if defined(HAVE_DECL_SYS_STATX) && HAVE_DECL_SYS_STATX == 1
+		if ((opts & OPT_ALWAYSSTAT))
+			stat_flags = 0;		/* Triggers sync with e.g. remote NFS server even on autofs */
 #endif
+
 		/* an option */
 		/* Not an option, must be a file specification */
 		if ((this_name = malloc(sizeof(struct names))) == NULL)
@@ -1643,7 +1638,7 @@ static struct stat *get_pidstat(const pid_t pid, const char *filename, int *id)
 	if ((st = (struct stat *)malloc(sizeof(struct stat))) == NULL)
 		return NULL;
 	snprintf(pathname, PATH_MAX-1, "/proc/%d/%s", pid, filename);
-	if (timeout(thestat, pathname, st, 5) != 0) {
+	if (statn(pathname, STATX_UID|STATX_INO|STATX_TYPE, st) != 0) {
 		free(st);
 		return NULL;
 	}
@@ -1690,7 +1685,7 @@ check_dir(const pid_t pid, const char *dirname, struct device_list *dev_head,
 		snprintf(filepath, sizeof filepath - 1, "/proc/%d/%s/%s",
 			 pid, dirname, direntry->d_name);
 
-		if (timeout(thestat, filepath, &st, 5) != 0) {
+		if (statn(filepath, STATX_INO, &st) != 0) {
 			if (errno != ENOENT && errno != ENOTDIR) {
 				fprintf(stderr, _("Cannot stat file %s: %s\n"),
 					filepath, strerror(errno));
@@ -1767,7 +1762,7 @@ check_dir(const pid_t pid, const char *dirname, struct device_list *dev_head,
 				if (thedev != ino_tmp->device)
 					continue;
 				if (!st.st_ino
-				    && timeout(thestat, filepath, &st, 5) != 0) {
+				    && statn(filepath, STATX_INO, &st) != 0) {
 					fprintf(stderr,
 						_("Cannot stat file %s: %s\n"),
 						filepath, strerror(errno));
@@ -1865,11 +1860,11 @@ static uid_t getpiduid(const pid_t pid)
 
 	if (asprintf(&pathname, "/proc/%d", pid) < 0)
 		return 0;
-	if (timeout(thestat, pathname, &st, 5) != 0) {
-        free(pathname);
+	if (statn(pathname, STATX_UID, &st) != 0) {
+		free(pathname);
 		return 0;
-    }
-    free(pathname);
+	}
+	free(pathname);
 	return st.st_uid;
 }
 
@@ -1907,7 +1902,7 @@ void fill_unix_cache(struct unixsocket_list **unixsocket_head)
 		path = scanned_path;
 		if (*scanned_path == '@')
 			scanned_path++;
-		if (timeout(thestat, scanned_path, &st, 5) < 0) {
+		if (statn(scanned_path, STATX_INO, &st) < 0) {
 			free(path);
 			continue;
 		}
@@ -2042,7 +2037,7 @@ static dev_t find_net_dev(void)
 		fprintf(stderr, _("Cannot open a network socket.\n"));
 		return -1;
 	}
-	if (fstat(skt, &st) != 0) {
+	if (fstatn(skt, STATX_INO, &st) != 0) {
 		fprintf(stderr, _("Cannot find socket's device number.\n"));
 		close(skt);
 		return -1;
@@ -2079,7 +2074,7 @@ scan_knfsd(struct names *names_head, struct inode_list *ino_head,
 		if ((find_space = strpbrk(line, " \t")) == NULL)
 			continue;
 		*find_space = '\0';
-		if (timeout(thestat, line, &st, 5) != 0) {
+		if (statn(line, STATX_INO, &st) != 0) {
 			continue;
 		}
 		/* Scan the devices */
@@ -2113,7 +2108,7 @@ scan_mounts(struct names *names_head, struct inode_list *ino_head,
 		mntinfo_t *mnt = list_entry(ptr, mntinfo_t);
 		const char *find_mountp = mnt->mpoint;
 
-		if (timeout(thestat, find_mountp, &st, 5) != 0)
+		if (statn(find_mountp, STATX_INO, &st) != 0)
 			continue;
 
 		/* Scan the devices */
@@ -2163,7 +2158,7 @@ scan_swaps(struct names *names_head, struct inode_list *ino_head,
 			if (*find_space == '\0')
 				continue;
 		}
-		if (timeout(thestat, line, &st, 5) != 0) {
+		if (statn(line, STATX_INO, &st) != 0) {
 			continue;
 		}
 		/* Scan the devices */
@@ -2260,10 +2255,10 @@ init_mntinfo(void)
 		&mid, &parid, &maj, &min, &mpoint[0], &type[0], &devname[0]) == 7) {
 		struct stat st;
 		mntinf = add_mntinfo(mpoint, type, mid, parid, makedev(maj, min));
-		if (mntinf && strncmp(devname, "/dev/", 5) == 0 && stat(devname, &st) == 0) {
+		if (mntinf && strncmp(devname, "/dev/", 5) == 0 && statn(devname, 0, &st) == 0) {
 			if (st.st_rdev != 0 && mntinf->dev != st.st_rdev) {
 				mntinf->vol = st.st_rdev;
-				stat(mpoint, &st);
+				statn(mpoint, 0, &st);
 				mntinf->dev = st.st_dev;	/* stat(2) on binary does not see subvol dev */
 			}
 		}
@@ -2275,7 +2270,7 @@ init_mntinfo(void)
 	parid = -1;
 	while (fscanf (mnt, "%s %s %s %*[^\n]", &devname[0], &mpoint[0], &type[0]) == 3) {
 		struct stat st;
-		if (stat(mpoint, &st) != 0) {
+		if (statn(mpoint, 0, &st) != 0) {
 			if (errno != EACCES) {
 				fprintf(stderr, _("Cannot stat %s: %s\n"),
 					mpoint, strerror(errno));
@@ -2284,7 +2279,7 @@ init_mntinfo(void)
 			st.st_dev = (dev_t)-1;
 		}
 		mntinf = add_mntinfo(mpoint, type, mid++, parid, st.st_dev);
-		if (mntinf && strncmp(devname, "/dev/", 5) == 0 && stat(devname, &st) == 0) {
+		if (mntinf && strncmp(devname, "/dev/", 5) == 0 && statn(devname, 0, &st) == 0) {
 			if (st.st_rdev != 0 && mntinf->dev != st.st_rdev)
 				mntinf->vol = st.st_rdev;
 		}
@@ -2308,7 +2303,7 @@ get_fdinfo(const pid_t pid, const char *fd, struct fdinfo *info)
 	struct stat st;
  
 	if (!mynamespace) {
-		if (stat("/proc/self/ns/mnt", &st) != 0) {
+		if (statn("/proc/self/ns/mnt", STATX_INO, &st) != 0) {
 			fprintf(stderr, _("Cannot stat %s: %s\n"),
 				"/proc/self/ns/mnt", strerror(errno));
 			exit(1);
@@ -2343,7 +2338,7 @@ get_fdinfo(const pid_t pid, const char *fd, struct fdinfo *info)
 out:
 # if defined(HAS_NAME_TO_HANDLE_AT)
 	snprintf(pathname, 256, "/proc/%d/ns/mnt", pid);
-	if (stat(pathname, &st) == 0 && mynamespace != st.st_ino) {
+	if (statn(pathname, STATX_INO, &st) == 0 && mynamespace != st.st_ino) {
 		char *realname;
 		snprintf(pathname, 256, "/proc/%d/fd/%s", pid, fd);
 		realname = expandpath(pathname);
@@ -2358,7 +2353,7 @@ out:
 		struct stat lst;
 
 		snprintf(pathname, 256, "/proc/%d/fd/%s", pid, fd);
-		if (!flags && lstat(pathname, &lst) == 0) {
+		if (!flags && lstatn(pathname, STATX_MODE, &lst) == 0) {
 			if (lst.st_mode & S_IWUSR)
 				info->flags |= O_WRONLY;
 			ret++;
@@ -2431,7 +2426,7 @@ find_mountpoint(const char *path, mntinfo_t **mountinfo)
 
 			/* could be a chroot or a container */
 
-			if (stat(path, &st) != 0) {
+			if (statn(path, 0, &st) != 0) {
 				if (errno != EACCES) {
 					fprintf(stderr, _("Cannot stat %s: %s\n"),
 						path, strerror(errno));
@@ -2463,7 +2458,7 @@ find_mountpoint(const char *path, mntinfo_t **mountinfo)
 
 					/* could be a chroot or a container */
 
-					if (stat(use, &st) != 0) {
+					if (statn(use, 0, &st) != 0) {
 						if (errno != EACCES) {
 							fprintf(stderr, _("Cannot stat %s: %s\n"),
 								use, strerror(errno));
@@ -2501,49 +2496,6 @@ out:
 	return ret;
 }
 
-#if defined(WITH_MOUNTINFO_LIST)
-/*
- * Determine device of links below /proc/
- */
-static int mntstat(const char *path, struct stat *buf)
-{
-	char name[PATH_MAX + 1];
-	const char *use;
-	mntinfo_t *mnt;
-
-	if ((use = realpath(path, name)) == NULL || *use != '/')
-	{
-		if (errno == ENOENT)
-			return -1;
-		/*
-		 * Could be a special file (socket, pipe, inotify)
-		 */
-		errno = 0;
-		return stat(path, buf);
-	}
-	if (strncmp("/dev/", use, 5) == 0) {
-		/*
-		 * Could be a special file (socket, pipe, inotify)
-		 */
-		errno = 0;
-		return stat(path, buf);
-	}
-
-	if (find_mountpoint(use, &mnt) < 0) {
-		errno = ENOENT;
-		return -1;
-	}
-
-	if (mnt->isremote) {
-		buf->st_dev = mnt->dev;
-		buf->st_ino = mnt->id;	    /* inode substitute */
-		return 0;		    /* found on NFS */
-	}
-
-	return stat(path, buf);
-}
-#endif				/* WITH_MOUNTINFO_LIST */
-
 /*
  * Somehow the realpath(3) glibc function call, nevertheless
  * it avoids lstat(2) system calls.
diff --git src/statx.c src/statx.c
new file mode 100644
index 0000000..a598c89
--- /dev/null
+++ src/statx.c
@@ -0,0 +1,149 @@
+/*
+ * statx.c - Map modern statx(2) system call to older stat(2), lstat(2),
+ *	     and fstat(2) replacements named {,l,f}statn()
+ *
+ * Copyright (C) 2018 Werner Fink
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2 of the License, or
+ * (at your option) any later version.
+ *
+ *  This program is distributed in the hope that it will be useful,
+ *  but WITHOUT ANY WARRANTY; without even the implied warranty of
+ *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ *  GNU General Public License for more details.
+ *
+ *  You should have received a copy of the GNU General Public License
+ *  along with this program; if not, write to the Free Software
+ *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
+ */
+#ifndef _GNU_SOURCE
+#define _GNU_SOURCE
+#endif
+
+#ifdef HAVE_CONFIG_H
+#include <config.h>
+#endif
+
+#ifndef HAVE_STATX
+# define _ASM_GENERIC_FCNTL_H	/* Avoid collisions between asm/fcntl.h and bits/fcntl.h ! */
+# include <linux/fcntl.h>	/* Definition of AT_* and AT_STATX_* constants ! */
+#endif
+#include <fcntl.h>		/* Definition of AT_* constants */
+#include <sys/stat.h>
+#ifndef HAVE_STATX
+# ifndef STATX_TYPE
+#  include <linux/stat.h>	/* Provides 'struct statx' and STATX_* ! */
+# endif
+#endif
+#include <sys/sysmacros.h>
+#include <sys/syscall.h>
+#include <sys/types.h>
+#include <unistd.h>
+
+int stat_flags = AT_NO_AUTOMOUNT|AT_STATX_DONT_SYNC;
+
+int statn(const char *pathname, unsigned int mask, struct stat *st)
+{
+    int flags = stat_flags;
+    int dirfd = pathname && *pathname == '/' ? 0 : AT_FDCWD;
+    int ret;
+    struct statx stx;
+
+#ifndef HAVE_STATX
+    ret = syscall(SYS_statx, dirfd, pathname, flags, mask, &stx);
+#else
+    ret = statx(dirfd, pathname, flags, mask, &stx);
+#endif
+    if (ret >= 0) {
+	st->st_dev  = makedev(stx.stx_dev_major,  stx.stx_dev_minor);
+	st->st_rdev = makedev(stx.stx_rdev_major, stx.stx_rdev_minor);
+
+	st->st_ino  = stx.stx_ino;
+	st->st_mode = stx.stx_mode;
+	st->st_nlink = stx.stx_nlink;
+	st->st_uid = stx.stx_uid;
+	st->st_gid = stx.stx_gid;
+	st->st_size = stx.stx_size;
+	st->st_blksize = stx.stx_blksize;
+	st->st_blocks = stx.stx_blocks;
+
+	st->st_atim.tv_sec = stx.stx_atime.tv_sec;
+	st->st_atim.tv_nsec = stx.stx_atime.tv_nsec;
+	st->st_mtim.tv_sec = stx.stx_mtime.tv_sec;
+	st->st_mtim.tv_nsec = stx.stx_mtime.tv_nsec;
+	st->st_ctim.tv_sec = stx.stx_ctime.tv_sec;
+	st->st_ctim.tv_nsec = stx.stx_ctime.tv_nsec;
+    }
+    return ret;
+}
+
+int fstatn(int fd, unsigned int mask, struct stat *st)
+{
+    int flags = AT_EMPTY_PATH|stat_flags;
+    int ret;
+    struct statx stx;
+
+#ifndef HAVE_STATX
+    ret = syscall(SYS_statx, fd, "", flags, mask, &stx);
+#else
+    ret = statx(fd, "", flags, mask, &stx);
+#endif
+    if (ret >= 0) {
+	st->st_dev  = makedev(stx.stx_dev_major,  stx.stx_dev_minor);
+	st->st_rdev = makedev(stx.stx_rdev_major, stx.stx_rdev_minor);
+
+	st->st_ino  = stx.stx_ino;
+	st->st_mode = stx.stx_mode;
+	st->st_nlink = stx.stx_nlink;
+	st->st_uid = stx.stx_uid;
+	st->st_gid = stx.stx_gid;
+	st->st_size = stx.stx_size;
+	st->st_blksize = stx.stx_blksize;
+	st->st_blocks = stx.stx_blocks;
+
+	st->st_atim.tv_sec = stx.stx_atime.tv_sec;
+	st->st_atim.tv_nsec = stx.stx_atime.tv_nsec;
+	st->st_mtim.tv_sec = stx.stx_mtime.tv_sec;
+	st->st_mtim.tv_nsec = stx.stx_mtime.tv_nsec;
+	st->st_ctim.tv_sec = stx.stx_ctime.tv_sec;
+	st->st_ctim.tv_nsec = stx.stx_ctime.tv_nsec;
+    }
+    return ret;
+}
+
+int lstatn(const char *pathname, unsigned int mask, struct stat *st)
+{
+    int flags = AT_SYMLINK_NOFOLLOW|stat_flags;
+    int dirfd = pathname && *pathname == '/' ? 0 : AT_FDCWD;
+    int ret;
+    struct statx stx;
+
+#ifndef HAVE_STATX
+    ret = syscall(SYS_statx, dirfd, pathname, flags, mask, &stx);
+#else
+    ret = statx(dirfd, pathname, flags, mask, &stx);
+#endif
+    if (ret >= 0) {
+	st->st_dev  = makedev(stx.stx_dev_major,  stx.stx_dev_minor);
+	st->st_rdev = makedev(stx.stx_rdev_major, stx.stx_rdev_minor);
+
+	st->st_ino  = stx.stx_ino;
+	st->st_mode = stx.stx_mode;
+	st->st_nlink = stx.stx_nlink;
+	st->st_uid = stx.stx_uid;
+	st->st_gid = stx.stx_gid;
+	st->st_size = stx.stx_size;
+	st->st_blksize = stx.stx_blksize;
+	st->st_blocks = stx.stx_blocks;
+
+	st->st_atim.tv_sec = stx.stx_atime.tv_sec;
+	st->st_atim.tv_nsec = stx.stx_atime.tv_nsec;
+	st->st_mtim.tv_sec = stx.stx_mtime.tv_sec;
+	st->st_mtim.tv_nsec = stx.stx_mtime.tv_nsec;
+	st->st_ctim.tv_sec = stx.stx_ctime.tv_sec;
+	st->st_ctim.tv_nsec = stx.stx_ctime.tv_nsec;
+    }
+    return ret;
+}
diff --git src/statx.h src/statx.h
new file mode 100644
index 0000000..fdd0137
--- /dev/null
+++ src/statx.h
@@ -0,0 +1,68 @@
+/*
+ * statx.h - Map modern statx(2) system call to older stat(2), lstat(2),
+ *           and fstat(2) replacements named {,l,f}statn()
+ *
+ * Copyright (C) 2018 Werner Fink
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2 of the License, or
+ * (at your option) any later version.
+ *
+ *  This program is distributed in the hope that it will be useful,
+ *  but WITHOUT ANY WARRANTY; without even the implied warranty of
+ *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ *  GNU General Public License for more details.
+ *
+ *  You should have received a copy of the GNU General Public License
+ *  along with this program; if not, write to the Free Software
+ *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
+ */
+
+#ifndef _STATX_H
+#define _STATX_H
+
+extern int stat_flags;
+#if defined(HAVE_DECL_SYS_STATX) && HAVE_DECL_SYS_STATX == 1
+# ifndef HAVE_STATX
+#  define _ASM_GENERIC_FCNTL_H	/* Avoid collisions between asm/fcntl.h and bits/fcntl.h ! */
+#  include <linux/fcntl.h>	/* Definition of AT_* and AT_STATX_* constants ! */
+#  ifndef STATX_TYPE
+#   include <linux/stat.h>	/* Provides 'struct statx' and STATX_* ! */
+#  endif
+# endif
+extern int statn(const char*, unsigned int, struct stat*);
+extern int fstatn(int, unsigned int, struct stat*);
+extern int lstatn(const char*, unsigned int, struct stat*);
+#else
+extern inline int
+statn(const char *path, unsigned int mask __attribute__((unused)), struct stat *st)
+{
+    return stat(path, st);
+}
+extern inline int
+fstatn(int fd, unsigned int mask __attribute__((unused)), struct stat *st)
+{
+    return fstat(fd, st);
+}
+extern inline int
+lstatn(const char *path, unsigned int mask __attribute__((unused)), struct stat *st)
+{
+    return lstat(path, st);
+}
+#define STATX_TYPE		0
+#define STATX_MODE		0
+#define STATX_NLINK		0
+#define STATX_UID		0
+#define STATX_GID		0
+#define STATX_ATIME		0
+#define STATX_MTIME		0
+#define STATX_CTIME		0
+#define STATX_INO		0
+#define STATX_SIZE		0
+#define STATX_BLOCKS		0
+#define STATX_BASIC_STATS	0
+#define STATX_BTIME		0
+#define STATX_ALL		0
+#endif
+#endif
diff --git src/timeout.c src/timeout.c
deleted file mode 100644
index a3c7055..0000000
--- src/timeout.c
+++ /dev/null
@@ -1,369 +0,0 @@
-/*
- * timout.c	Advanced timeout handling for file system calls
- *		to avoid deadlocks on remote file shares.
- *
- * Version:	0.2 11-Dec-2012 Fink
- *
- * Copyright 2011,2012 Werner Fink, 2011,2012 SUSE LINUX Products GmbH, Germany.
- *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU General Public License as published by
- * the Free Software Foundation; either version 2 of the License, or
- * (at your option) any later version.
- *
- * Author:	Werner Fink <werner@suse.de>, 2011
- */
-
-#ifndef _GNU_SOURCE
-# define _GNU_SOURCE
-#endif
-
-#ifdef _FEATURES_H
-# error Include local config.h before any system header file
-#endif
-#include "config.h"
-
-#ifndef WITH_TIMEOUT_STAT
-# define WITH_TIMEOUT_STAT	0
-#endif
-
-#ifndef USE_SOCKETPAIR
-# define USE_SOCKETPAIR		1
-#endif
-
-#include <errno.h>
-#include <setjmp.h>
-#include <signal.h>
-#include <stdlib.h>
-#include <string.h>
-#include <sys/mman.h>
-#include <sys/time.h>
-#include <sys/types.h>
-#include <sys/select.h>
-#include <sys/stat.h>
-#include <unistd.h>
-#if USE_SOCKETPAIR
-# include <sys/socket.h>
-# include <netdb.h>
-# include <netinet/in.h>
-#  ifndef SHUT_RD
-#   define SHUT_RD	0
-# endif
-# ifndef SHUT_WR
-#  define SHUT_WR	1
-# endif
-# undef pipe
-# define pipe(v)	(((socketpair(AF_UNIX,SOCK_STREAM,0,v) < 0) || \
-			(shutdown((v)[1],SHUT_RD) < 0) || (shutdown((v)[0],SHUT_WR) < 0)) ? -1 : 0)
-#endif
-#include <wait.h>
-
-#include "timeout.h"
-
-#if !defined(__STDC_VERSION__) || (__STDC_VERSION__ < 199901L)
-# ifndef  destructor
-#  define destructor		__destructor__
-# endif
-# ifndef  constructor
-#  define constructor		__constructor__
-# endif
-# ifndef  inline
-#  define inline		__inline__
-# endif
-# ifndef  unused
-#  define unused		__unused__
-# endif
-# ifndef  volatile
-#  define volatile		__volatile__
-# endif
-#endif
-#ifndef  attribute
-# define attribute(attr)	__attribute__(attr)
-#endif
-
-#if defined __GNUC__
-# undef strcpy
-# define strcpy(d,s)		__builtin_strcpy((d),(s))   /* Without boundary check please */
-#endif
-
-#if WITH_TIMEOUT_STAT
-static sigjmp_buf jenv;
-static void sigjump(int sig attribute((unused)))
-{
-	siglongjmp(jenv, 1);
-}
-#endif
-
-#if WITH_TIMEOUT_STAT == 2
-/*
- * The structure used for communication between the processes
- */
-typedef struct _handle {
-	int errcode;
-	struct stat argument;
-	stat_t function;
-	size_t len;
-	char path[0];
-} handle_t;
-
-/*
- * Using a forked process for doing e.g. stat(2) system call as this
- * allows us to send e.g. SIGKILL to this process if it hangs in `D'
- * state on a file share due a stalled NFS server.  This does not work
- * with (p)threads as SIGKILL would kill all threads including main.
- */
-
-static volatile pid_t active;
-static int pipes[4] = {-1, -1, -1, -1};
-static handle_t *restrict handle;
-static const size_t buflen = PATH_MAX+sizeof(handle_t)+1;
-
-static void sigchild(int sig attribute((unused)))
-{
-	pid_t pid = waitpid(active, NULL, WNOHANG|WUNTRACED);
-	if (pid <= 0)
-		return;
-	if (errno == ECHILD)
-		return;
-	active = 0;
-}
-
-static void attribute((constructor)) start(void)
-{
-	sigset_t sigset, oldset;
-	struct sigaction act;
-	char sync[1];
-	ssize_t in;
-
-	if (pipes[1] >= 0) close(pipes[1]);
-	if (pipes[2] >= 0) close(pipes[2]);
-
-	if (pipe(&pipes[0]))
-		goto error;
-	if (pipe(&pipes[2]))
-		goto error;
-
-	memset(&act, 0, sizeof(act));
-	sigemptyset(&act.sa_mask);
-	act.sa_flags = SA_RESTART;
-	act.sa_handler = sigchild;
-	sigaction(SIGCHLD, &act, 0);
-
-	if (!handle)
-		handle = mmap(NULL, buflen, PROT_READ|PROT_WRITE,
-			      MAP_ANONYMOUS|MAP_SHARED, -1, 0);
-	if (handle == MAP_FAILED)
-		goto error;
-
-	if ((active = fork()) < 0)
-		goto error;
-
-	if (active) {
-		close(pipes[0]);
-		close(pipes[3]);
-		pipes[0] = pipes[3] = -1;
-		return;
-	}
-
-	sigemptyset(&sigset);
-	sigaddset(&sigset, SIGALRM);
-	sigprocmask(SIG_BLOCK, &sigset, &oldset);
-
-	act.sa_handler = SIG_DFL;
-	sigaction(SIGCHLD, &act, 0);
-
-	close(pipes[1]);
-	close(pipes[2]);
-	dup2(pipes[0], STDIN_FILENO);
-	dup2(pipes[3], STDOUT_FILENO);
-	close(pipes[0]);
-	close(pipes[3]);
-	pipes[1] = pipes[2] = -1;
-	pipes[0] = pipes[3] = -1;
-
-	while ((in = read(STDIN_FILENO, &sync, sizeof(sync))) != 0) {
-		ssize_t out;
-		if (in < 0) {
-			if (errno == EINTR)
-				continue;
-			break;
-		}
-		if (!handle)
-			break;
-		if (handle->function(handle->path, &handle->argument) < 0)
-				handle->errcode = errno;
-		do
-			out = write(STDOUT_FILENO, &sync, sizeof(sync));
-		while (out < 0 && errno == EINTR);
-	}
-
-	sigprocmask(SIG_SETMASK, &oldset, NULL);
-	exit(0);
-error:
-	if (pipes[0] >= 0) close(pipes[0]);
-	if (pipes[1] >= 0) close(pipes[1]);
-	if (pipes[2] >= 0) close(pipes[2]);
-	if (pipes[3] >= 0) close(pipes[3]);
-	if (handle && handle != MAP_FAILED)
-		munmap(handle, buflen);
-	handle = NULL;
-}
-
-static void /* attribute((destructor)) */ stop(void)
-{
-	if (active && waitpid(active, NULL, WNOHANG|WUNTRACED) == 0)
-		kill(active, SIGKILL);
-}
-
-/*
- * External routine
- *
- * Execute stat(2) system call with timeout to avoid deadlock
- * on network based file systems.
- *
- */
-int
-timeout(stat_t function, const char *path, struct stat *restrict argument, time_t seconds)
-{
-	struct sigaction alrm_act, pipe_act, new_act;
-	sigset_t sigset, oldset;
-	char sync[1] = "x";
-
-	if (active <= 0)	/* Oops, last one failed therefore clear status and restart */
-		start();
-	if (!handle)		/* No shared memory area */
-		return function(path, argument);
-	memset(handle, 0, sizeof(handle_t));
-	handle->len = strlen(path) + 1;
-	if (handle->len >= PATH_MAX) {
-		errno = ENAMETOOLONG;
-		goto error;
-	}
-	handle->errcode = 0;
-	handle->argument = *argument;
-	handle->function = function;
-	strcpy(handle->path, path);
-
-	sigemptyset(&sigset);
-	sigaddset(&sigset, SIGALRM);
-	sigaddset(&sigset, SIGPIPE);
-	sigprocmask(SIG_UNBLOCK, &sigset, &oldset);
-
-	memset(&new_act, 0, sizeof(new_act));
-	sigemptyset(&new_act.sa_mask);
-	new_act.sa_flags = SA_RESETHAND;
-
-	if (sigsetjmp(jenv, 1))
-		goto timed;
-
-	new_act.sa_handler = sigjump;
-	sigaction(SIGALRM, &new_act, &alrm_act);
-	sigaction(SIGPIPE, &new_act, &pipe_act);
-	alarm(seconds);
-
-	write(pipes[1], &sync, sizeof(sync));
-	read(pipes[2], &sync, sizeof(sync));
-
-	alarm(0);
-	sigaction(SIGPIPE, &pipe_act, NULL);
-	sigaction(SIGALRM, &alrm_act, NULL);
-
-	if (handle->errcode) {
-		errno = handle->errcode;
-		goto error;
-	}
-
-	*argument = handle->argument;
-	sigprocmask(SIG_SETMASK, &oldset, NULL);
-
-	return 0;
-timed:
-	(void) alarm(0);
-	sigaction(SIGPIPE, &pipe_act, NULL);
-	sigaction(SIGALRM, &alrm_act, NULL);
-	sigprocmask(SIG_SETMASK, &oldset, NULL);
-	stop();
-	errno = ETIMEDOUT;
-error:
-	return -1;
-}
-#elif WITH_TIMEOUT_STAT == 1
-/*
- * External routine
- *
- * Execute stat(2) system call with timeout to avoid deadlock
- * on network based file systems.
- *
- */
-int
-timeout(stat_t function, const char *path, struct stat *restrict argument, time_t seconds)
-{
-	struct sigaction alrm_act, pipe_act, new_act;
-	sigset_t sigset, oldset;
-	int ret = 0, pipes[4];
-	pid_t pid = 0;
-	ssize_t len;
-
-	if (pipe(&pipes[0]) < 0)
-		goto error;
-	switch ((pid = fork())) {
-	case -1:
-		close(pipes[0]);
-		close(pipes[1]);
-		goto error;
-	case 0:
-		new_act.sa_handler = SIG_DFL;
-		sigaction(SIGALRM, &new_act, NULL);
-		close(pipes[0]);
-		if ((ret = function(path, argument)) == 0)
-			do
-				len = write(pipes[1], argument, sizeof(struct stat));
-			while (len < 0 && errno == EINTR);
-		close(pipes[1]);
-		exit(ret);
-	default:
-		close(pipes[1]);
-
-		sigemptyset(&sigset);
-		sigaddset(&sigset, SIGALRM);
-		sigaddset(&sigset, SIGPIPE);
-		sigprocmask(SIG_UNBLOCK, &sigset, &oldset);
-
-		memset(&new_act, 0, sizeof(new_act));
-		sigemptyset(&new_act.sa_mask);
-
-		if (sigsetjmp(jenv, 1))
-			goto timed;
-
-		new_act.sa_handler = sigjump;
-		sigaction(SIGALRM, &new_act, &alrm_act);
-		sigaction(SIGPIPE, &new_act, &pipe_act);
-		alarm(seconds);
-		if (read(pipes[0], argument, sizeof(struct stat)) == 0) {
-			errno = EFAULT;
-			ret = -1;
-		}
-		(void)alarm(0);
-		sigaction(SIGPIPE, &pipe_act, NULL);
-		sigaction(SIGALRM, &alrm_act, NULL);
-
-		close(pipes[0]);
-		waitpid(pid, NULL, 0);
-		break;
-	}
-	return ret;
-timed:
-	(void)alarm(0);
-	sigaction(SIGPIPE, &pipe_act, NULL);
-	sigaction(SIGALRM, &alrm_act, NULL);
-	if (waitpid(0, NULL, WNOHANG) == 0)
-		kill(pid, SIGKILL);
-	errno = ETIMEDOUT;
-error:
-	return -1;
-}
-#endif
-
-/*
- * End of timeout.c
- */
diff --git src/timeout.h src/timeout.h
deleted file mode 100644
index f372297..0000000
--- src/timeout.h
+++ /dev/null
@@ -1,45 +0,0 @@
-/*
- * timout.h	Advanced timeout handling for file system calls
- *		to avoid deadlocks on remote file shares.
- *
- * Version:	0.1 07-Sep-2011 Fink
- *
- * Copyright 2011 Werner Fink, 2011 SUSE LINUX Products GmbH, Germany.
- *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU General Public License as published by
- * the Free Software Foundation; either version 2 of the License, or
- * (at your option) any later version.
- *
- * Author:	Werner Fink <werner@suse.de>, 2011
- */
-
-#ifndef _TIMEOUT_H
-#define _TIMEOUT_H
-
-#include "config.h"
-
-#ifndef WITH_TIMEOUT_STAT
-# define WITH_TIMEOUT_STAT 0
-#endif
-
-#include <sys/types.h>
-#include <sys/stat.h>
-#include <time.h>
-#include <limits.h>
-
-#if !defined(__STDC_VERSION__) || (__STDC_VERSION__ < 199901L)
-# ifndef  restrict
-#  define restrict		__restrict__
-# endif
-#endif
-
-typedef int (*stat_t)(const char *, struct stat *);
-
-#if WITH_TIMEOUT_STAT > 0
-extern int timeout(stat_t, const char *, struct stat *restrict, time_t);
-#else
-# define timeout(func,path,buf,dummy)	(func)((path),(buf))
-#endif
-
-#endif
-- 
2.26.2

