-------------------------------------------------------------------
Mon Jul 15 09:45:31 UTC 2019 - Stefan Dirsch <sndirsch@suse.com>

- Update to version 1.0.10
  * This release provides a fix for CVE-2017-2626 for platforms 
    which don't have arc4random_buf() in their default libraries
    but do have getentropy(), such as Linux platforms with a kernel
    version of 3.17 or newer and a glibc version of 2.25 or newer.
    (libICE 1.0.9 already ensured that arc4random_buf() is used on
    platforms that have it to provide sufficient entropy in ICE
    key generation, but left other platforms with the weaker methods.
    Linux platforms could also have linked against libbsd to use
    arc4random_buf() with libICE 1.0.9 for stronger keys.)
- supersedes U_Use-getentropy-if-arc4random_buf-is-not-available.patch

-------------------------------------------------------------------
Sun Jun 11 18:00:24 UTC 2017 - sndirsch@suse.com

- U_Use-getentropy-if-arc4random_buf-is-not-available.patch
  * Use getentropy() if arc4random_buf() is not available
    (bnc#1025068, CVE-2017-2626)
- tagged baselibs.conf as source in specfile

-------------------------------------------------------------------
Tue Jun 10 15:32:39 UTC 2014 - sndirsch@suse.com

- Update to version 1.0.9
  * This release fixes a number of issues found by static analysis and
    compiler warnings, and other minor code cleanups.  On systems with
    arc4random() in either libc or libbsd, it will now use that function
    for generating authentication cookies.

-------------------------------------------------------------------
Sun Feb 17 17:21:53 UTC 2013 - jengelh@inai.de

- Use more robust make install call

-------------------------------------------------------------------
Wed Apr 11 15:03:16 UTC 2012 - vuntz@opensuse.org

- Update to version 1.0.8:
  + Fix a number of issues found by static analysis and compiler
    warnings
  + Large set of cleanups and improvements to the DocBook format
    specs for the protocol and docs for the API.

-------------------------------------------------------------------
Sat Feb 11 18:23:28 UTC 2012 - jengelh@medozas.de

- Fix typo in baselibs.conf: should be libICE, not libFS
- Provide package descriptions and update homepage URL
- Add Obsoletes/Provides to baselibs.conf as well

-------------------------------------------------------------------
Fri Feb 10 15:57:48 UTC 2012 - sndirsch@suse.com

- back to previous provides/obsoletes for xorg-x11-libICE(-devel) 

-------------------------------------------------------------------
Fri Feb 10 14:22:37 UTC 2012 - sndirsch@suse.com

- provide/obsolete xorg-x11-libICE(-devel), no matter which version
  number

-------------------------------------------------------------------
Thu Feb  9 18:26:26 UTC 2012 - jengelh@medozas.de

- Rename xorg-x11-libICE to libICE and utilize shlib policy

-------------------------------------------------------------------
Sun Nov 20 06:48:18 UTC 2011 - coolo@suse.com

- add libtool as buildrequire to avoid implicit dependency

-------------------------------------------------------------------
Tue Dec 21 02:41:53 UTC 2010 - sndirsch@novell.com

- bumped version number to 7.6 

-------------------------------------------------------------------
Wed Dec  1 09:29:44 CET 2010 - jslaby@suse.de

- revert 'export only public API symbols'
- it breaks everything

-------------------------------------------------------------------
Tue Nov 30 14:48:53 UTC 2010 - cristian.rodriguez@opensuse.org

- export only public API symbols
- disable silent rules, defeat the purpose of post build checks  

-------------------------------------------------------------------
Wed Oct 20 20:59:47 UTC 2010 - sndirsch@novell.com

- libICE-1.0.7 

-------------------------------------------------------------------
Sun Apr  4 15:29:28 CEST 2010 - sndirsch@suse.de

- libICE 1.0.6
- bumped version number to 7.5 

-------------------------------------------------------------------
Mon Dec 14 23:57:26 CET 2009 - jengelh@medozas.de

- add baselibs.conf as a source

-------------------------------------------------------------------
Sat May  2 14:42:17 CEST 2009 - eich@suse.de

- revert static library and .la file removal
  for SUSE versions <= 11.1.

-------------------------------------------------------------------
Tue Apr 21 21:41:13 CEST 2009 - crrodriguez@suse.de

- remove static libraries 

-------------------------------------------------------------------
Tue Mar  3 15:44:39 CET 2009 - sndirsch@suse.de

- libICE 1.0.5

-------------------------------------------------------------------
Thu Sep 11 14:20:43 CEST 2008 - sndirsch@suse.de

- bumped release number to 7.4 

-------------------------------------------------------------------
Thu Apr 10 12:54:45 CEST 2008 - ro@suse.de

- added baselibs.conf file to build xxbit packages
  for multilib support

-------------------------------------------------------------------
Tue Jan  1 23:21:28 CET 2008 - crrodriguez@suse.de

- fix library-without-ldconfig-* error
- add missing PreReq coreutils
 

-------------------------------------------------------------------
Sat Sep 29 12:22:28 CEST 2007 - sndirsch@suse.de

- bumped version to 7.3 

-------------------------------------------------------------------
Fri Aug 24 15:43:39 CEST 2007 - sndirsch@suse.de

- libICE 1.0.4
  * Coverity #1085: Double free of pointer "*listenObjsRet"
  * Coverity #1086: Double free of pointer "*listenObjsRet"
  * Convert authutil.c static helpers to ANSI C prototypes to clear
    sparse warnings
  * Provide ANSI C prototypes for more static functions
  * Replace many malloc(strlen()); strcpy() pairs with strdup()

-------------------------------------------------------------------
Sat Dec 16 06:03:37 CET 2006 - sndirsch@suse.de

- update to release 1.0.3
  * Makefile.am: make ChangeLog hook safer 

-------------------------------------------------------------------
Tue Oct 24 17:55:34 CEST 2006 - dmueller@suse.de

- strip .la file

-------------------------------------------------------------------
Sat Oct 14 06:03:36 CEST 2006 - sndirsch@suse.de

- update to X.Org 7.2RC1 

-------------------------------------------------------------------
Wed Aug  2 16:11:56 CEST 2006 - sndirsch@suse.de

- fix setup line 

-------------------------------------------------------------------
Fri Jul 28 14:44:10 CEST 2006 - sndirsch@suse.de

- use "-fno-strict-aliasing" 

-------------------------------------------------------------------
Thu Jul 27 11:40:28 CEST 2006 - sndirsch@suse.de

- use $RPM_OPT_FLAGS
- remove existing /usr/include/X11 symlink in %pre

-------------------------------------------------------------------
Thu Jun 22 21:23:38 CEST 2006 - sndirsch@suse.de

- created package 

