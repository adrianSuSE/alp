-------------------------------------------------------------------
Thu Jan 20 04:52:06 UTC 2022 - William Brown <william.brown@suse.com>

- bsc#1194767 - CVE-2022-21658 - Resolve race condition in std::fs::remove_dir_all
  - 0002-Fix-CVE-2022-21658-for-UNIX-like.patch
  - 0003-Fix-CVE-2022-21658-for-WASI.patch

-------------------------------------------------------------------
Wed Dec 15 08:11:08 UTC 2021 - Guillaume GARDET <guillaume.gardet@opensuse.org>

- Relax constraints for armv6/7

-------------------------------------------------------------------
Fri Dec 10 00:38:42 UTC 2021 - William Brown <william.brown@suse.com>

- Update 0001-Resolve-error-in-bootstrap-where-id-not-used.patch to account for
  an issue in test execution.

-------------------------------------------------------------------
Fri Dec  3 03:26:32 UTC 2021 - William Brown <william.brown@suse.com>

- Add fix_bootstrap_vendor.patch to keep the current .cargo/config file
- Add 0001-Resolve-error-in-bootstrap-where-id-not-used.patch to resolve build warning
- Carry over ignore-Wstring-conversion.patch

-------------------------------------------------------------------
Fri Dec  3 01:29:12 UTC 2021 - William Brown <william.brown@suse.com>

Version 1.57.0 (2021-12-02)
==========================

Language
--------

- [Macro attributes may follow `#[derive]` and will see the original (pre-`cfg`) input.][87220]
- [Accept curly-brace macros in expressions, like `m!{ .. }.method()` and `m!{ .. }?`.][88690]
- [Allow panicking in constant evaluation.][89508]

Compiler
--------

- [Create more accurate debuginfo for vtables.][89597]
- [Add `armv6k-nintendo-3ds` at Tier 3\*.][88529]
- [Add `armv7-unknown-linux-uclibceabihf` at Tier 3\*.][88952]
- [Add `m68k-unknown-linux-gnu` at Tier 3\*.][88321]
- [Add SOLID targets at Tier 3\*:][86191] `aarch64-kmc-solid_asp3`, `armv7a-kmc-solid_asp3-eabi`, `armv7a-kmc-solid_asp3-eabihf`

\* Refer to Rust's [platform support page][platform-support-doc] for more
   information on Rust's tiered platform support.

Libraries
---------

- [Avoid allocations and copying in `Vec::leak`][89337]
- [Add `#[repr(i8)]` to `Ordering`][89507]
- [Optimize `File::read_to_end` and `read_to_string`][89582]
- [Update to Unicode 14.0][89614]
- [Many more functions are marked `#[must_use]`][89692], producing a warning
  when ignoring their return value. This helps catch mistakes such as expecting
  a function to mutate a value in place rather than return a new value.

Stabilised APIs
---------------

- [`[T; N]::as_mut_slice`][`array::as_mut_slice`]
- [`[T; N]::as_slice`][`array::as_slice`]
- [`collections::TryReserveError`]
- [`HashMap::try_reserve`]
- [`HashSet::try_reserve`]
- [`String::try_reserve`]
- [`String::try_reserve_exact`]
- [`Vec::try_reserve`]
- [`Vec::try_reserve_exact`]
- [`VecDeque::try_reserve`]
- [`VecDeque::try_reserve_exact`]
- [`Iterator::map_while`]
- [`iter::MapWhile`]
- [`proc_macro::is_available`]
- [`Command::get_program`]
- [`Command::get_args`]
- [`Command::get_envs`]
- [`Command::get_current_dir`]
- [`CommandArgs`]
- [`CommandEnvs`]

These APIs are now usable in const contexts:

- [`hint::unreachable_unchecked`]

Cargo
-----

- [Stabilize custom profiles][cargo/9943]
