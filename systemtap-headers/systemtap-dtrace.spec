#
# spec file for package systemtap-dtrace
#
# Copyright (c) 2022 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


%if ! %{defined _rundir}
%define _rundir %{_localstatedir}/run
%endif
Name:           systemtap-dtrace
Version:        4.6
Release:        0
Summary:        SystemTap dtrace utility
License:        GPL-2.0-or-later
Group:          Development/Tools/Debuggers
URL:            http://sourceware.org/systemtap/
#!RemoteAsset: sha256:80fb7309232b21349db3db2eea6f1561795225e79c1364c4ada8a66e4412faae
Source0:        http://sourceware.org/systemtap/ftp/releases/systemtap-%{version}.tar.gz
#!RemoteAsset: sha256:a81766ac49e8fbdac1141c3fbb79f86d25679550b293bfa166e1d389b117ff8f
Source1:        http://sourceware.org/systemtap/ftp/releases/systemtap-%{version}.tar.gz.asc
Source2:        systemtap.keyring
Source3:        README-BEFORE-ADDING-PATCHES
Source4:        README-KEYRING
Patch1:         systemtap-build-source-dir.patch
Patch2:         sys-sdt.h-fp-constraints-arm32.patch
Patch3:         sys-sdt.h-fp-constraints-x86_64.patch
Patch4:         sys-sdt.h-fp-constraints-aarch64-s390.patch

BuildArch:      noarch

%description
SystemTap is an instrumentation system for systems running Linux.
This package contains the dtrace utility to build provider and probe
definitions.

%prep
%setup -q -n systemtap-%{version}
%autopatch -p1

%build
# Our binutils always support '?' in the section characters on all
# architectures, no need for configure tests
sed s=@preferred_python@=%{_bindir}/python3= dtrace.in |sed s=@prefix@=%{_prefix}= >dtrace

%install
mkdir -p %{buildroot}%{_bindir}
install -m 755 dtrace %{buildroot}%{_bindir}

%files
%defattr(-,root,root)
%{_bindir}/dtrace

%changelog
