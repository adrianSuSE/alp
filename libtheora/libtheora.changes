-------------------------------------------------------------------
Mon Jun 21 18:06:32 UTC 2021 - Matej Cepl <mcepl@suse.com>

- Remove completely unnecessary python BR

-------------------------------------------------------------------
Sat Aug 30 11:29:11 UTC 2014 - jengelh@inai.de

- Split libtheoradec/enc from libtheora0 as they have different
  SO numbers
- Trim huge description; improve on RPM group classificaiton

-------------------------------------------------------------------
Thu May 23 14:27:51 UTC 2013 - idonmez@suse.com

- Update descriptions, thanks to Perry Werneck

-------------------------------------------------------------------
Sun Nov 20 06:25:00 UTC 2011 - coolo@suse.com

- add libtool as buildrequire to avoid implicit dependency

-------------------------------------------------------------------
Mon Aug 29 19:11:23 UTC 2011 - crrodriguez@opensuse.org

- remove examples that fail to build, also SDL and png
  are only needed for those, so remove from buildrequires.

-------------------------------------------------------------------
Wed Jul 27 23:53:06 UTC 2011 - crrodriguez@opensuse.org

- remove fno-strict-aliasing from CFLAGS as it is no longer
  needed and will slow down things. 

-------------------------------------------------------------------
Mon May 23 19:15:11 UTC 2011 - crrodriguez@opensuse.org

- Disable doxygen documentation to avoid build dates in 
  -devel packages.
- add missing BuildRequires libpng-devel

-------------------------------------------------------------------
Fri Dec 18 17:25:50 CET 2009 - jengelh@medozas.de

- add baselibs.conf as a source

-------------------------------------------------------------------
Wed Oct  7 08:11:26 UTC 2009 - adrian@suse.de

- update to version 1.1.1
  * minor bugfixes

-------------------------------------------------------------------
Sat Sep 26 13:13:49 UTC 2009 - adrian@suse.de

- update to version 1.1.0
  * minor fixes since beta 3

-------------------------------------------------------------------
Thu Aug 27 07:29:25 UTC 2009 - adrian@suse.de

- update to version 1.1 beta 3
  * Much better encoder 
    (faster and more details at same compressions level)
  * Playback received speed improvements, but bitstream format is 
    untouched
- no package split yet for dec/enc/legacy libs due to 11.2 freeze

-------------------------------------------------------------------
Fri Jul 17 11:02:49 CEST 2009 - adrian@suse.de

- update to version 1.0 final
  * new additional encoder and decoder libs with new api.

-------------------------------------------------------------------
Wed Jan  7 12:34:56 CET 2009 - olh@suse.de

- obsolete old -XXbit packages (bnc#437293)

-------------------------------------------------------------------
Wed May 21 20:43:00 CEST 2008 - cthiel@suse.de

- fix baselibs.conf

-------------------------------------------------------------------
Thu Apr 10 12:54:45 CEST 2008 - ro@suse.de

- added baselibs.conf file to build xxbit packages
  for multilib support

-------------------------------------------------------------------
Thu Dec 13 01:57:07 CET 2007 - crrodriguez@suse.de

- fix package version numbers 1.0beta1 --> 1.0.beta2
- libtheora 1.0.beta2
  - Fix a crash bug on char-is-unsigned architectures (PowerPC)
  - Fix a buffer sizing issue that caused rare encoder crashes
  - Fix a buffer alignment issue
  - Improved format documentation. 
- removed unneeded patch, use --with-pic configure option instead.

-------------------------------------------------------------------
Thu Nov  8 09:57:32 CET 2007 - adrian@suse.de

- fix compiling with gcc 4.3 on ia32

-------------------------------------------------------------------
Wed Sep 26 13:46:14 CEST 2007 - adrian@suse.de

- update to 1.0beta1
 From official changelog:
 * Granulepos scheme modified to match other codecs. This bumps
   the bitstream revision to 3.2.1. Bitstreams marked 3.2.0 are
   handled correctly by this decoder. Older decoders will show
   a one frame sync error in the less noticable direction.
 * Switch to new spec compliant decoder from theora-exp branch.
   Written by Dr. Timothy Terriberry.
 * Add support to the encoder for using quantization settings
   provided by the application.
 * more assembly optimizations

-------------------------------------------------------------------
Wed Aug 15 13:50:33 CEST 2007 - coolo@suse.de

- fixing upgrade (#293401)

-------------------------------------------------------------------
Sun Aug 12 00:06:27 CEST 2007 - crrodriguez@suse.de

- fix build in x86_64
- use library packaging policy
- run make check in the check section
- add missing call to ldconfig 

-------------------------------------------------------------------
Wed Mar 28 18:34:27 CEST 2007 - sbrabec@suse.cz

- Updated to version 1.0alpha7:
  * Enable mmx assembly by default
  * Avoid some relocations that caused problems on SELinux
  * Other build fixes
  * time testing mode (-f) for the dump_video example
  * Merge theora-mmx simd acceleration (x86_32 and x86_64)
  * Major RTP payload specification update
  * Minor format specification updates
  * Fix some spurious calls to free() instead of _ogg_free()
  * Fix invalid array indexing in PixelLineSearch()
  * Improve robustness against invalid input
  * General warning cleanup
  * The offset_y member meaning fix.
- Use incremental versioning scheme.
- Documentation repackaged.
- Use less vague names for binaries.

-------------------------------------------------------------------
Tue Aug  1 03:21:15 CEST 2006 - dmueller@suse.de

- Reenable test suite run with valgrind.

-------------------------------------------------------------------
Fri Jul 28 14:43:59 CEST 2006 - aj@suse.de

- Disable test suite run with valgrind.

-------------------------------------------------------------------
Fri Mar 10 15:55:10 CET 2006 - bk@suse.de

- libtheora-devel: add libogg-devel to Requires (found by .la check)

-------------------------------------------------------------------
Mon Feb  6 10:36:07 CET 2006 - adrian@suse.de

- add -fstack-protector
- enable test suite run with valgrind

-------------------------------------------------------------------
Sun Jan 29 17:41:50 CET 2006 - aj@suse.de

- Fix BuildRequires.

-------------------------------------------------------------------
Wed Jan 25 21:37:46 CET 2006 - mls@suse.de

- converted neededforbuild to BuildRequires

-------------------------------------------------------------------
Wed Oct 19 10:15:30 CEST 2005 - adrian@suse.de

- update to version 1.0 alpha 5
- enable test suite
- generate API documentation with doxygen

-------------------------------------------------------------------
Thu Apr 14 17:17:06 CEST 2005 - sbrabec@suse.cz

- Added audiofile-devel to neededforbuild.

-------------------------------------------------------------------
Wed Jan  5 14:33:42 CET 2005 - adrian@suse.de

- update to version 1.0 alpha 4

-------------------------------------------------------------------
Tue Oct 26 16:17:00 CEST 2004 - adrian@suse.de

- remove .svn directories

-------------------------------------------------------------------
Mon Oct 18 08:58:16 CEST 2004 - adrian@suse.de

- update to current cvs to get pc file

-------------------------------------------------------------------
Thu Aug 19 10:32:43 CEST 2004 - adrian@suse.de

- create -devel package

-------------------------------------------------------------------
Tue Jun 29 21:52:48 CEST 2004 - adrian@suse.de

- do not install the libtool scripts ...

-------------------------------------------------------------------
Sat Apr 24 13:45:35 CEST 2004 - adrian@suse.de

- use xorg-x11 packages

-------------------------------------------------------------------
Wed Apr 21 12:21:07 CEST 2004 - adrian@suse.de

- compile with -fno-strict-aliasing

-------------------------------------------------------------------
Sun Mar 21 17:36:51 CET 2004 - adrian@suse.de

- update to alpha 3 release
  on disc format is still not frozen, so this remain an internal package

-------------------------------------------------------------------
Wed Feb  4 09:50:06 CET 2004 - adrian@suse.de

- remove binaries from example dir (they get installed anyway)

-------------------------------------------------------------------
Fri Jan 30 18:04:02 CET 2004 - adrian@suse.de

- initial package of current snapshot (post alpha2)
- internal package only atm

