-------------------------------------------------------------------
Sat Jun 19 07:34:21 UTC 2021 - Michael Ströder <michael@stroeder.com>

- update to version 2.0.1
  * Version 2.0.1
    - Mark top-level names as exported so type checking understands imports 
      in user projects. #240
    - The salt argument to Serializer and Signer can be None again. #237
  * Version 2.0.0
    - Drop support for Python 2 and 3.5.
    - JWS support (JSONWebSignatureSerializer, 
      TimedJSONWebSignatureSerializer) is deprecated. Use a dedicated JWS/JWT 
      library such as authlib instead. #129
    - Importing itsdangerous.json is deprecated. Import Python’s json module instead. #152
    - Simplejson is no longer used if it is installed. To use a different 
      library, pass it as Serializer(serializer=...). #146
    - datetime values are timezone-aware with timezone.utc. Code using 
      TimestampSigner.unsign(return_timestamp=True) or 
      BadTimeSignature.date_signed may need to change. #150
    - If a signature has an age less than 0, it will raise SignatureExpired 
      rather than appearing valid. This can happen if the timestamp offset is 
      changed. #126
    - BadTimeSignature.date_signed is always a datetime object rather than an 
      int in some cases. #124
    - Added support for key rotation. A list of keys can be passed as 
      secret_key, oldest to newest. The newest key is used for signing, all 
      keys are tried for unsigning. #141
    - Removed the default SHA-512 fallback signer from default_fallback_signers. #155
    - Add type information for static typing tools. #186

-------------------------------------------------------------------
Tue Jun 11 07:52:22 UTC 2019 - Marketa Calabkova <mcalabkova@suse.com>

- update to version 1.1.0 
  * Drop support for Python 2.6 and 3.3. 
  * Optimize how timestamps are serialized and deserialized.
  * base64_decode raises BadData when it is passed invalid data.
  * More compact JSON dumps for unicode strings.
  * Use the full timestamp rather than an offset, allowing dates 
    before 2011. To retain compatibility with signers from previous 
    versions, consider using shim from issue #120.

-------------------------------------------------------------------
Tue Dec  4 12:49:25 UTC 2018 - Matej Cepl <mcepl@suse.com>

- Remove superfluous devel dependency for noarch package, but
  keep testsuite passing.

-------------------------------------------------------------------
Tue Apr  4 15:53:02 UTC 2017 - jmatejek@suse.com

- update for singlespec

-------------------------------------------------------------------
Thu Nov 17 13:03:00 UTC 2016 - rjschwei@suse.com

- Include in SLE 12 (FATE#320818, bsc#979331)

-------------------------------------------------------------------
Fri Jul 18 15:08:08 UTC 2014 - toddrme2178@gmail.com

- Update to 0.24
  - Added a `BadHeader` exception that is used for bad headers
    that replaces the old `BadPayload` exception that was reused
    in those cases.

-------------------------------------------------------------------
Tue Sep  3 13:23:20 UTC 2013 - cfarrell@suse.com

- license update: BSD-3-Clause
  See the LICENSE file

-------------------------------------------------------------------
Mon Sep  2 14:55:53 UTC 2013 - speilicke@suse.com

- Initial version

