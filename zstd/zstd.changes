-------------------------------------------------------------------
Sun Jan 30 22:59:24 UTC 2022 - Dirk Müller <dmueller@suse.com>

- enable zlib/gzip compatible backend, as zlib is significantly
  (50%-100%) faster than gzip
- add zstd-gzip compatibility subpackage which can be used
  as a drop in compatible replacement for gzip
- small spec file cleanups

-------------------------------------------------------------------
Fri Jan 21 04:53:08 UTC 2022 - Bernhard Wiedemann <bwiedemann@suse.com>

- update to 1.5.2:
  * correct a performance regression
  * smaller improvements and fixes
  * See https://github.com/facebook/zstd/releases/tag/v1.5.2
- Refresh pzstd.1.patch
- Drop upstream noexecstack.patch

-------------------------------------------------------------------
Wed Jan  5 13:12:22 UTC 2022 - Dirk Müller <dmueller@suse.com>

- add noexecstack.patch (bsc#1194337) 

-------------------------------------------------------------------
Mon Jan  3 09:18:49 UTC 2022 - Dirk Müller <dmueller@suse.com>

- fix pkgconfig pc file settings by passing in right prefix during build

-------------------------------------------------------------------
Mon Dec 27 14:59:02 UTC 2021 - Dirk Müller <dmueller@suse.com>

- update to 1.5.1:
  * perf: rebalanced compression levels, to better match the intended speed/level curve
  * perf: faster huffman decoder, using x64 assembly
  * perf: slightly faster high speed modes (strategies fast & dfast)
  * perf: improved binary size and faster compilation times
  * perf: new row64 mode, used notably in level 12
  * perf: faster mid-level compression speed in presence of highly repetitive patterns
  * perf: minor compression ratio improvements for small data at high levels
  * perf: reduced stack usage (mostly useful for Linux Kernel)
  * perf: faster compression speed on incompressible data
  * perf: on-demand reduced ZSTD_DCtx state size, using build macro ZSTD_DECODER_INTERNAL_BUFFER, at a small cost of performance
  * build: allows hiding static symbols in the dynamic library, using build macro
  * build: support for m68k (Motorola 68000's)
  * build: improved AIX support
  * build: improved meson unofficial build
  * cli : custom memory limit when training dictionary (#2925)
  * cli : report advanced parameters information when compressing in very verbose mode (``-vv`)

-------------------------------------------------------------------
Fri May 14 20:34:25 UTC 2021 - Ismail Dönmez <ismail@i10z.com>

- Update to version 1.5.0
  * https://github.com/facebook/zstd/releases/tag/v1.5.0
  * Improved Middle-Level Compression Speed
  * Improved High-Level Compression Ratio
  * Faster Decompression Speed
  * Dynamic Library Supports Multithreading by Default
- Drop zstd-1.4.9-Avoid-SIGBUS-on-armv6.patch, merged upstream

-------------------------------------------------------------------
Tue May 11 18:28:45 UTC 2021 - Bernhard Wiedemann <bwiedemann@suse.com>

- Add zstd-1.4.9-Avoid-SIGBUS-on-armv6.patch
  to fix crashes when running armv6 userspace on armv8 64-bit kernels

-------------------------------------------------------------------
Thu Mar  4 01:36:14 UTC 2021 - Bernhard Wiedemann <bwiedemann@suse.com>

- Update to version 1.4.9
  * https://github.com/facebook/zstd/releases/tag/v1.4.9
  * >2x Faster Long Distance Mode
  * New Experimental Decompression Feature: ZSTD_d_refMultipleDDicts
  * bug: Use umask() to Constrain Created File Permissions
  * bug: Make Simple Single-Pass Functions Ignore Advanced Parameters
  * cli: Fix --output-dir-mirror's Rejection of ..-Containing Paths
  * cli: Allow Input From Console When -f/--force is Passed  

-------------------------------------------------------------------
Fri Jan 22 14:34:28 UTC 2021 - Bernhard Wiedemann <bwiedemann@suse.com>

- Add libzstd-devel-32bit (boo#1181272)

-------------------------------------------------------------------
Sat Dec 19 04:01:08 UTC 2020 - Bernhard Wiedemann <bwiedemann@suse.com>

- Update to version 1.4.8 to fix i586+s390x

-------------------------------------------------------------------
Thu Dec 17 04:04:42 UTC 2020 - Bernhard Wiedemann <bwiedemann@suse.com>

- Update to version 1.4.7
  * Improved --long mode
  * --long now automatically enabled for any window size >= 128MB
  * Faster decompression of small blocks
  * CLI improvements
    + accept parameter through environment variable ZSTD_NBTHREADS
    + new command --output-dir-mirror
    + more accurate warning and error messages
  * New experimental features
    + Shared Thread Pool
    + Faster Dictionary Compression
    + New Sequence Ingestion API
  * Drop upstream fix-lib-build.patch

-------------------------------------------------------------------
Wed Jul  8 18:33:04 UTC 2020 - Martin Liška <mliska@suse.cz>

- Remove not needed exports in %install section.

-------------------------------------------------------------------
Wed Jul  8 17:47:50 UTC 2020 - Martin Liška <mliska@suse.cz>

- Use %make_build macro.
- Add upstream patch fix-lib-build.patch which fixes lib-mt target.

-------------------------------------------------------------------
Fri May 22 08:05:28 UTC 2020 - Ismail Dönmez <idonmez@suse.com>

- Update to version 1.4.5
  * perf: Improved decompression speed (x64 >+5%, ARM >+15%)
  * perf: Automatically downsizes ZSTD_DCtx when too large for too
  * perf: Improved fast compression speed on aarch64 (#2040, ~+3%)
  * perf: Small level 1 compression speed gains (depending on compiler)
  * fix: Compression ratio regression on huge files (> 3 GB) using high
    levels (--ultra) and multithreading
  * api: ZDICT_finalizeDictionary() is promoted to stable
  * api: new experimental parameter ZSTD_d_stableOutBuffer
  * cli: New --patch-from command, create and apply patches from files
  * cli: --filelist= : Provide a list of files to operate upon from a file
  * cli: -b can now benchmark multiple files in decompression mode
  * cli: New --no-content-size command
  * cli: New --show-default-cparams command
  * misc: new diagnosis tool, checked_flipped_bits, in contrib/
  * misc: Extend largeNbDicts benchmark to compression
  * misc: experimental edit-distance match finder in contrib/

-------------------------------------------------------------------
Tue Nov  5 18:39:50 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Update to version 1.4.4
  * perf: Improved decompression speed, by > 10%
  * perf: Better compression speed when re-using a context
  * perf: Fix compression ratio when compressing large files with
    small dictionary
  * perf: zstd reference encoder can generate RLE blocks
  * perf: minor generic speed optimization
  * api: new ability to extract sequences from the parser for analysis
  * api: fixed decoding of magic-less frames
  * api: fixed ZSTD_initCStream_advanced() performance with fast modes
  * cli: Named pipes support
  * cli: short tar's extension support
  * cli: command --output-dir-flat=DIE , generates target files into
    requested directory
  * cli: commands --stream-size=# and --size-hint=#
  * cli: command --exclude-compressed
  * cli: faster -t test mode
  * cli: improved some error messages
  * cli: fix rare deadlock condition within dictionary builder
  * misc: Improved documentation : ZSTD_CLEVEL, DYNAMIC_BMI2,
    ZSTD_CDict, function deprecation, zstd format
  * misc: fixed educational decoder : accept larger literals section,
    and removed UNALIGNED() macro
- Refresh pzstd.1.patch

-------------------------------------------------------------------
Tue Aug 20 07:25:49 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Update to version 1.4.3
  * bug: Fix Dictionary Compression Ratio Regression (#1709)
  * bug: Fix Buffer Overflow in v0.3 Decompression (#1722)
  * build: Add support for IAR C/C++ Compiler for Arm (#1705)
  * misc: Add NULL pointer check in util.c (#1706)

-------------------------------------------------------------------
Tue Jul 30 13:43:02 UTC 2019 - Bernhard Wiedemann <bwiedemann@suse.com>

- Add disk _constraints to fix ppc64le build

-------------------------------------------------------------------
Fri Jul 26 13:06:18 UTC 2019 - Bernhard Wiedemann <bwiedemann@suse.com>

- Update to version 1.4.2:
  * bug: Fix bug in zstd-0.5 decoder by @terrelln (#1696)
  * bug: Fix seekable decompression in-memory API by @iburinoc (#1695)
  * bug: Close minor memory leak in CLI by @LeeYoung624 (#1701)
  * misc: Validate blocks are smaller than size limit by @vivekmig (#1685)
  * misc: Restructure source files by @ephiepark (#1679)

-------------------------------------------------------------------
Mon Jul 22 10:58:28 UTC 2019 - Martin Pluskal <mpluskal@suse.com>

- Update to version 1.4.1:
  * bug: Fix data corruption in niche use cases by @terrelln (#1659)
  * bug: Fuzz legacy modes, fix uncovered bugs by @terrelln (#1593, #1594, #1595)
  * bug: Fix out of bounds read by @terrelln (#1590)
  * perf: Improve decode speed by ~7% @mgrice (#1668)
  * perf: Slightly improved compression ratio of level 3 and 4 (ZSTD_dfast) by @cyan4973 (#1681)
  * perf: Slightly faster compression speed when re-using a context by @cyan4973 (#1658)
  * perf: Improve compression ratio for small windowLog by @cyan4973 (#1624)
  * perf: Faster compression speed in high compression mode for repetitive data by @terrelln (#1635)
  * api: Add parameter to generate smaller dictionaries by @tyler-tran (#1656)
  * cli: Recognize symlinks when built in C99 mode by @felixhandte (#1640)
  * cli: Expose cpu load indicator for each file on -vv mode by @ephiepark (#1631)
  * cli: Restrict read permissions on destination files by @chungy (#1644)
  * cli: zstdgrep: handle -f flag by @felixhandte (#1618)
  * cli: zstdcat: follow symlinks by @vejnar (#1604)
  * doc: Remove extra size limit on compressed blocks by @felixhandte (#1689)
  * doc: Fix typo by @yk-tanigawa (#1633)
  * doc: Improve documentation on streaming buffer sizes by @cyan4973 (#1629)
  * build: CMake: support building with LZ4 @leeyoung624 (#1626)
  * build: CMake: install zstdless and zstdgrep by @leeyoung624 (#1647)
  * build: CMake: respect existing uninstall target by @j301scott (#1619)
  * build: Make: skip multithread tests when built without support by @michaelforney (#1620)
  * build: Make: Fix examples/ test target by @sjnam (#1603)
  * build: Meson: rename options out of deprecated namespace by @lzutao (#1665)
  * build: Meson: fix build by @lzutao (#1602)
  * build: Visual Studio: don't export symbols in static lib by @scharan (#1650)
  * build: Visual Studio: fix linking by @absotively (#1639)
  * build: Fix MinGW-W64 build by @myzhang1029 (#1600)
  * misc: Expand decodecorpus coverage by @ephiepark (#1664)

-------------------------------------------------------------------
Thu Jul 11 08:17:23 UTC 2019 - Dominique Leuenberger <dimstar@opensuse.org>

- Add baselibs.conf: libarchive gained zstd support and provides
  -32bit libraries. This means, zstd also needs to provide -32bit
  libs.

-------------------------------------------------------------------
Fri Jun 14 11:52:32 UTC 2019 - Ismail Dönmez <idonmez@suse.com>

- Remove googletest-devel BuildRequires and pzstd-global-gtest.patch
  because we want zstd in ring0 (due to rpm's zstd payload support)
  Since googletest is only used in build time (it's for testing after
  all), this should be fine.

-------------------------------------------------------------------
Sun Jun  2 10:11:25 UTC 2019 - Jan Engelhardt <jengelh@inai.de>

- Update description with 1.4.0 statistics.

-------------------------------------------------------------------
Fri Apr 26 11:10:57 UTC 2019 - Martin Liška <mliska@suse.cz>

- Use FAT LTO objects in order to provide proper static library (boo#1133297).

-------------------------------------------------------------------
Wed Apr 17 11:30:30 UTC 2019 - Jan Engelhardt <jengelh@inai.de>

- Update to new upstream release 1.4.0
  * perf: level 1 compression speed was improved by ~6–8%
  * cli: added --[no-]compress-literals flag to enable or disable
    literal compression
- Reword "real-time" in description by some actual statistics,
  because 603MB/s (lowest zstd level) is not "real-time" for
  quite some applications.

-------------------------------------------------------------------
Sat Jan  5 13:57:58 UTC 2019 - astieger@suse.com

- zstd 1.3.8:
  * better decompression speed on large files (+7%) and cold
    dictionaries (+15%)
  * slightly better compression ratio at high compression modes
  * new --rsyncable mode
  * support decompression of empty frames into NULL (used to be an
    error)
  * support ZSTD_CLEVEL environment variable
  * --no-progress flag, preserving final summary
  * various CLI fixes
  * fix race condition in one-pass compression functions that could
    allow out of bounds write (CVE-2019-11922, boo#1142941)

-------------------------------------------------------------------
Wed Oct 24 08:29:38 UTC 2018 - astieger@suse.com

- zstd 1.3.7:
  * fix ratio for dictionary compression at levels 9 and 10
  * add man pages for zstdless and zstdgrep
- includes changes from zstd 1.3.6:
  * faster dictionary builder, also the new default for --train
  * previous (slower, slightly higher quality) dictionary builder
    to be selected via --train-cover
  * Faster dictionary decompression and compression under memory
    limits with many dictionaries used simultaneously
  * New command --adapt for compressed network piping of data
    adjusted to the perceived network conditions
- drop zstd-1.3.5-fix-list-stdin.patch, upstream
- switch from git tag snaphshot to bootstrapped tarball

-------------------------------------------------------------------
Tue Jul  3 08:13:06 UTC 2018 - astieger@suse.com

- update to 1.3.5:
  * much faster dictionary compression
  * small quality improvement for dictionary generation
  * slightly improved performance at high compression levels
  * automatic memory release for long duration contexts
  * fix overlapLog can be manually set
  * fix decoding invalid lz4 frames
  * fix performance degradation for dictionary compression when
    using advanced API
- add zstd-1.3.5-fix-list-stdin.patch to avoid test issues with
  --list when stdin is not a tty, patch from upstream
- disable failing pzstd tests

-------------------------------------------------------------------
Thu Jun 14 11:56:45 UTC 2018 - sweet_f_a@gmx.de

- fix pzstd tests
- add pzstd-global-gtest.patch 

-------------------------------------------------------------------
Thu Jun 14 08:11:32 UTC 2018 - sweet_f_a@gmx.de

- enable pzstd (parallel zstd)
- add pzstd.1.patch, taken from Fedora package (git 21908004)

-------------------------------------------------------------------
Thu May 24 14:36:12 CEST 2018 - kukuk@suse.de

- Use %license instead of %doc [bsc#1082318]

-------------------------------------------------------------------
Wed Mar 28 15:04:25 UTC 2018 - idonmez@suse.com

- Update to 1.3.4
  * perf: faster speed (especially decoding speed) on recent cpus
    (haswell+)
  * perf: much better performance associating --long with
    multi-threading
  * perf: better compression at levels 13-15
  * cli : asynchronous compression by default, for faster
    experience (use --single-thread for former behavior)
  * cli : smoother status report in multi-threading mode
  * cli : added command --fast=#, for faster compression modes
  * cli : fix crash when not overwriting existing files
  * api : `nbThreads` becomes `nbWorkers` : 1 triggers
    asynchronous mode
  * api : compression levels can be negative, for even more speed
  * api : ZSTD_getFrameProgression() : get precise progress
    status of ZSTDMT anytime
  * api : ZSTDMT can accept new compression parameters during
    compression
  * api : implemented all advanced dictionary decompression prototypes

-------------------------------------------------------------------
Tue Feb  6 00:00:00 CET 2018 - dsterba@suse.cz

- build the static library (depency for btrfsprogs-static)

-------------------------------------------------------------------
Thu Dec 21 11:02:54 UTC 2017 - idonmez@suse.com

- Update to 1.3.3
  * perf: improved zstd_opt strategy (levels 16-19)
  * fix : bug #944 : multithreading with shared ditionary and
    large data, reported by @gsliepen
  * cli : fix : content size written in header by default
  * cli : fix : improved LZ4 format support, by @felixhandte
  * cli : new : hidden command -b -S, to benchmark multiple files
    and generate one result per file
  * api : change : when setting pledgedSrcSize, use
    ZSTD_CONTENTSIZE_UNKNOWN macro value to mean "unknown"
  * api : fix : support large skippable frames, by @terrelln
  * api : fix : re-using context could result in suboptimal
    block size in some corner case scenarios
  * api : fix : streaming interface was adding a useless
    3-bytes null block to small frames

-------------------------------------------------------------------
Sun Oct 29 15:52:37 UTC 2017 - astieger@suse.com

- update to 1.3.2:
  * new long range mode, using --long command
  * new ability to generate and decode magicless frames
  * changed maximum nb of threads reduced to 200, to avoid address
    space exhaustion in 32-bits mode
  * fix multi-threading compression works with custom allocators
  * fix ZSTD_sizeof_CStream() was over-evaluating memory usage
  * fix a rare compression bug when compression generates very large
    distances and bunch of other conditions
    (only possible at --ultra -22)
  * fix 32-bits build can now decode large offsets (levels 21+)
  * cli added LZ4 frame support by default
  * cli improved --list output
  * cli now can split input file for dictionary training, using
    command -B#
  * cli new clean operation artefact on Ctrl-C interruption
  * do not change /dev/null permissions when using command -t with
    root access
  * cli fix write file size in header in multiple-files mode
  * api: added macro ZSTD_COMPRESSBOUND() for static allocation
  * api: new advanced decompression API
  * api: sizeof_CCtx() used to over-estimate
  * build: fix : no-multithread variant compiles without pool.c
    dependency
  * build: better compatibility with reproducible builds
  * license: changed /examples license to BSD + GPLv2
  * license: fix a few header files to reflect new license 

-------------------------------------------------------------------
Mon Aug 21 13:34:50 UTC 2017 - idonmez@suse.com

- Update to v1.3.1
  * License is now BSD + GPL-2.0
  * See https://github.com/facebook/zstd/releases for the
    complete changelog.

-------------------------------------------------------------------
Mon Mar 27 14:04:29 UTC 2017 - idonmez@suse.com

- Update to v1.1.4
  See https://github.com/facebook/zstd/releases for details.
- Drop zstd-lib-no-rebuild.patch

-------------------------------------------------------------------
Wed Dec  7 09:25:33 UTC 2016 - idonmez@suse.com

- Fix group name for the shared library 

-------------------------------------------------------------------
Sun Dec  4 12:25:27 UTC 2016 - idonmez@suse.com

- Update to version 1.1.1
  * New : cli commands -M#, --memory=, --memlimit=,
    --memlimit-decompress= to limit allowed memory consumption
    during decompression
  * Improved : slightly better compression ratio at --ultra levels
  * Improved : better memory usage when using streaming
    compression API
  * Added : API : ZSTD_initCStream_usingCDict(), 
    ZSTD_initDStream_usingDDict() (experimental section)
  * Changed : zstd_errors.h is now installed within /include
    (and replaces errors_public.h)
  * Fixed : several sanitizer warnings

-------------------------------------------------------------------
Sun Oct 16 17:55:02 UTC 2016 - jengelh@inai.de

- Update descriptions

-------------------------------------------------------------------
Wed Oct  5 09:03:14 UTC 2016 - bwiedemann@suse.com

- initial package version 1.1.0
  based on https://pbrady.fedorapeople.org/zstd.spec

