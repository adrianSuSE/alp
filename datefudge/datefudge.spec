#
# spec file for package datefudge
#
# Copyright (c) 2020 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           datefudge
Version:        1.24
Release:        0
Summary:        A preload library to fake system time
License:        GPL-2.0-or-later
Group:          Development/Tools/Other
URL:            https://packages.qa.debian.org/d/datefudge.html
#!RemoteAsset: sha256:80efab249232deeb142bf8a0ec66f2cf596c45560e4fdd28f3b5bdd290ac0385
Source:         http://cdn.debian.net/debian/pool/main/d/datefudge/%{name}_%{version}.tar.xz
#!RemoteAsset: sha256:452581e5b7a61ab80a45a7dae1b8d44c13f443a3dea5cfedb227f1cecb4e09d8
Source2:        http://cdn.debian.net/debian/pool/main/d/datefudge/%{name}_%{version}.dsc#/%{name}.asc
#!RemoteAsset: sha256:41b93ae50c564aac58668c80d11461189078c64bc526d96eb2c183891f62749c
Source3:        https://db.debian.org/fetchkey.cgi?fingerprint=35E876FAB4D3732E93B4D237631DE7553BE8AFD4#/%{name}.keyring

%description
This program fakes the system date so that programs think the
wall clock is different. The faking is not complete; timestamp
on files are not affected in any way.

%prep
# the signature is on the Debian .dsc. Extract the checksums and verify against source
echo "`grep -A1 "Files:" %{SOURCE2} | grep %{name}_%{version}.tar.xz | cut -d\  -f2`  %{SOURCE0}" | md5sum -c
echo "`grep -A1 "Checksums-Sha1" %{SOURCE2} | grep %{name}_%{version}.tar.xz | cut -d\  -f2`  %{SOURCE0}" | sha1sum -c
echo "`grep -A1 "Checksums-Sha256" %{SOURCE2} | grep %{name}_%{version}.tar.xz | cut -d\  -f2`  %{SOURCE0}" | sha256sum -c
%setup -q
sed "s/VERSION := \$\(.*\)/VERSION := %{version}/g" -i Makefile
sed 's/-o root -g root/-p/g' -i Makefile

%build
CFLAGS="%{optflags}" make libdir=%{_libdir} %{?_smp_mflags}

%install
make install libdir=%{_libdir} DESTDIR=%{buildroot} %{?_smp_mflags}

%files
%license COPYING
%doc README
%{_mandir}/man1/datefudge.1%{?ext_man}
%{_bindir}/datefudge
%{_libdir}/datefudge/datefudge.so
%dir %{_libdir}/datefudge

%changelog
