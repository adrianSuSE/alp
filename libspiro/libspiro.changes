-------------------------------------------------------------------
Sat Aug 15 21:50:34 UTC 2020 - Dirk Mueller <dmueller@suse.com>

- update to 20200505:
  * Bugfix for CVE-2019-19847 affecting {call-test14 to call-test19}.
  * Fix a memory access bug/error created earlier by patch 2017oct28
    Users using tagpoint libspiro20150702 are unaffected by this bug.
    Users using tagpoint libspiro20190731 are recommended to upgrade.
    Thanks to Frederic Cambus for calling attention to these faults.
  * Add optional 'end knot' for open curves (useful for displaying).
  * CRA Version also higher than so-bump 1.0.5 used on some distros;
    this maybe of interest to distros that bumped an earlier version.
  * Some garbage-in/garbage-out checks to verify we have 'ah' pairs,
    and we don't start with ']', or end with '['. Add libspiro.3 man.
  * Corrected set_di_to_x1y1() to use a constant bandwidth of 0.0005
  * Code improvements and bug fixes for better tagged/spiro results.
  * Several improvements added to further increase libspiro's speed.

-------------------------------------------------------------------
Thu Dec 19 12:46:12 UTC 2019 - pgajdos@suse.com

- version update to 20190731
  * Scaling bug fixed. This allows libspiro to scale, and move spiro
    paths, therefore allowing users and/or programs the ability of
    using/making templates.
  * Additional spiro controls 'anchor' and 'handle' added.
  * Toggle switch ncq added, to allow further control of output results.

-------------------------------------------------------------------
Wed Dec 16 15:17:58 UTC 2015 - pgajdos@suse.com

- update to version 0.5.20150702
  * Important bug fix issue #11 (missing file not included in 0v4).
  * 2 Minor bug fixes caught on Coverity scan, free() and if c=3.
  * Re-edit lib and tests to be more accommodating of older compilers.
  * Verify libspiro output data is correct for test curves {0,1,2,4}.

-------------------------------------------------------------------
Sat Feb 14 18:36:51 UTC 2015 - p.drouand@gmail.com

- Update to version 0.3.20150131
  * fixes for LibSpiro package definitions (bfo#196780)
  * allowing other programs to seek LibSpiro using PKG_CHECK_MODULES().
    (bao#43373)
- Update home page and download source Urls
- Add autoconf, automake and libtool requirements; needed to generate
  the configure script
- Remove depreciated AUTHORS section
- Package AUTHORS, ChangeLog and COPYING into the doc directory
- gpl.txt is not provided anymore by upstream

-------------------------------------------------------------------
Mon Feb  6 13:46:11 UTC 2012 - cfarrell@suse.com

- license update: GPL-2.0+
  See gpl.txt and e.g. spiro.c - there is no indication that this is
  BSD-3-Clause licensed and every indication that it is GPL-2.0+ licensed

-------------------------------------------------------------------
Sat Feb  4 14:06:20 UTC 2012 - jengelh@medozas.de

- Restore ldconfig call for shlib package

-------------------------------------------------------------------
Sat Feb  4 08:27:28 UTC 2012 - i@marguerite.su

- rename spec file and changelog to meet Factory requirements.

-------------------------------------------------------------------
Sat Feb  4 01:24:47 UTC 2012 - jengelh@medozas.de

- Remove redundant tags/sections
- pkgconfig file: libm should not be in the shared link
- Remove pointless Obsoletes, the supposedly old name has never
  been in use before

-------------------------------------------------------------------
Sat Feb  4 01:23:40 UTC 2012 - jengelh@medozas.de

- make license SPDX compatible.

-------------------------------------------------------------------
Sun Dec 25 19:03:37 UTC 2011 - i@marguerite.su

- enable debuginfo package 

-------------------------------------------------------------------
Sat Nov 26 15:51:32 UTC 2011 - i@marguerite.su

- formated specfile to merge into home:opensuse_zh 

