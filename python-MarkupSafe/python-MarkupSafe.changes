-------------------------------------------------------------------
Sun Jun 13 14:46:40 UTC 2021 - Michael Ströder <michael@stroeder.com>

- skip building for Python 2.x

-------------------------------------------------------------------
Tue Jun  1 10:23:06 UTC 2021 - Antonio Larrosa <alarrosa@suse.com>

- Update to v2.0.1
  * Mark top-level names as exported so type checking understands
    imports in user projects.
  * Fix some types that weren’t available in Python 3.6.0.

- Update to v2.0.0
  * Drop Python 2.7, 3.4, and 3.5 support.
  * Markup.unescape uses html.unescape() to support HTML5 character
    references. #117
  * Add type annotations for static typing tools. #149

-------------------------------------------------------------------
Fri Apr 16 07:30:01 UTC 2021 - Dirk Müller <dmueller@suse.com>

- allow tests to be disabled (still on by default)

-------------------------------------------------------------------
Tue Mar  5 16:46:50 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Update to 1.1.1:
  * Fix segfault when __html__ method raises an exception when
    using the C speedups. The exception is now propagated
    correctly. (#109)

-------------------------------------------------------------------
Thu Feb 21 16:13:28 UTC 2019 - John Vandenberg <jayvdb@gmail.com>

- Update to v1.1.0
  - Drop support for Python 2.6 and 3.3.
  - Build wheels for Linux, Mac, and Windows, allowing systems without
    a compiler to take advantage of the C extension speedups
  - Use newer CPython API on Python 3, resulting in a 1.5x speedup
  - ``escape`` wraps ``__html__`` result in ``Markup``, consistent with
    documented behavior
- Switch to using pytest in %check as setup.py test no longer works
- Use more precise URL https://github.com/pallets/markupsafe
- Add docs/ to %doc, including the changelog
- Remove AUTHORS from %doc, removed upstream in 6247e015

-------------------------------------------------------------------
Mon Jan 14 16:01:46 CET 2019 - kukuk@suse.de

- Use %license instead of %doc [bsc#1082318]

-------------------------------------------------------------------
Tue Apr  4 15:13:20 UTC 2017 - jmatejek@suse.com

- update source url
- fix obs/prov to refer to old python
- drop _speedups.c from installed directory

-------------------------------------------------------------------
Wed Mar 22 15:11:29 UTC 2017 - tbechtold@suse.com

- Add missing BuildRequires for python-rpm-macros

-------------------------------------------------------------------
Wed Mar 22 13:08:44 UTC 2017 - tbechtold@suse.com

- update to 1.0.0
  + No upstream changelog
- Switch to singlespec approach

-------------------------------------------------------------------
Tue Jul 15 10:42:00 UTC 2014 - toddrme2178@gmail.com

- Update to 0.23
  + No upstream changelog

-------------------------------------------------------------------
Thu May  8 13:51:56 UTC 2014 - toddrme2178@gmail.com

- Update to 0.21
  + No upstream changelog
  
  -------------------------------------------------------------------
Thu Mar 13 14:03:44 UTC 2014 - mcihar@suse.cz

- update to 0.19:
  + Various Python 3.x fixes

-------------------------------------------------------------------
Thu Oct 24 11:08:17 UTC 2013 - speilicke@suse.com

- Require python-setuptools instead of distribute (upstreams merged)

-------------------------------------------------------------------
Tue Jun 25 11:56:41 UTC 2013 - dmueller@suse.com

- update to 0.18:
  + Fixed interpolation on tuples
  + Varios Python 3.x fixes

-------------------------------------------------------------------
Wed May 23 05:24:01 UTC 2012 - highwaystar.ru@gmail.com

- python3 package added
- minor spec improvement 

-------------------------------------------------------------------
Thu Sep  8 20:24:51 UTC 2011 - andrea.turrini@gmail.com

- Fixed typo in description of python-MarkupSafe.spec

-------------------------------------------------------------------
Thu Sep  1 13:31:24 UTC 2011 - saschpe@suse.de

- Update to 0.15
- Changed license to BSD-3-Clause
- Renamed to python-MarkupSafe (from python-markupsafe)

-------------------------------------------------------------------
Mon Aug 30 16:06:34 UTC 2010 - jfunk@funktronics.ca

- Update to 0.11
    Since MarkupSafe 0.10 there is now also a separate escape function
    called `escape_silent` that returns an empty string for `None` for
    consistency with other systems that return empty strings for `None`
    when escaping (for instance Pylons' webhelpers).

-------------------------------------------------------------------
Thu Jul  1 18:08:55 UTC 2010 - jfunk@funktronics.ca

- Initial release

