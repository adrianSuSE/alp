-------------------------------------------------------------------
Mon Jun 28 17:37:10 UTC 2021 - Matej Cepl <mcepl@suse.com>

- Add use_python3.patch to allow use of Python3 instead of
  Python2 for generating files.

-------------------------------------------------------------------
Mon Oct 19 13:39:35 UTC 2020 - Cristian Rodríguez <crrodriguez@opensuse.org>

- Always pretend we do not have ftime(3), function is deprecated
  and absent from next glibc release.

-------------------------------------------------------------------
Fri Aug 16 11:40:05 UTC 2019 - Michel Normand <normand@linux.vnet.ibm.com>

- Add libpgm-5.2.122-configure-rdtsc-checking-chg.patch
  same as https://github.com/steve-o/openpgm/pull/63

-------------------------------------------------------------------
Mon Jan 28 12:51:34 UTC 2019 - luca.boccassi@gmail.com

- Backport patches from upstream to avoid adding a non-existing
  foo/lib/pgm-5.2/include directory to pkg-config's CFLAGS which
  breaks applications using strict compiler flags, and to make the
  build reproducible regardless of the reported system/architecture

Added:
- libpgm-5.2.122-pkg-config-do-not-add-I-to-non-existing-directory.patch
- libpgm-5.2.122-reproducible-architecture.patch

-------------------------------------------------------------------
Fri Jul 13 07:39:53 UTC 2018 - dimstar@opensuse.org

- Add baselibs.conf: build -32bit packages, which are needed by
  zeromq's -32bit implementation. [bsc#1146257]

-------------------------------------------------------------------
Tue Mar 27 11:49:58 CEST 2018 - kukuk@suse.de

- Use %license instead of %doc [bsc#1082318]

-------------------------------------------------------------------
Tue Oct 24 12:47:51 UTC 2017 - luca.boccassi@gmail.com

- Create m4 directory before running autoreconf -fi to avoid build
  failure on CentOS 6 due to older version of autoconf.

-------------------------------------------------------------------
Fri Oct  6 13:22:06 UTC 2017 - bwiedemann@suse.com

- Add libpgm-5.2.122-reproducible.patch to make build reproducible
  (boo#1047218)

-------------------------------------------------------------------
Tue Feb  3 14:11:33 UTC 2015 - jengelh@inai.de

- Update description and RPM groups

-------------------------------------------------------------------
Tue Jan 13 14:35:23 UTC 2015 - mvyskocil@opensuse.org

- Fix package to conform openSUSE packaging standards
 * rename to openpgm to match with upstream and pkgconfig name
 * cleanup weird build systemd and use standard macros
 * cleanup description
 * move protocol description to devel file
 * drop static library

-------------------------------------------------------------------
Thu Jan  8 12:18:15 UTC 2015 - mvyskocil@opensuse.org

- Update to 5.2.122 (no upstream changelog available) 

-------------------------------------------------------------------
Thu Nov 22 16:38:36 CET 2012 - pascal.bleser@opensuse.org

- initial version (5.1.118)

