-------------------------------------------------------------------
Fri Nov 12 19:15:34 UTC 2021 - Ignaz Forster <iforster@suse.com>

- Update to version 1.6
  * Adapt rd.retry to also trigger initqueue timeout tasks
    [gh#kubic-project/health-checker#11]
  * Reboot system and let the GRUB health-checker part try to find
    a working initrd if root file system could not be mounted -
    instead of ending up in an emergency shell
  * Correctly declare Bash scripts as such

-------------------------------------------------------------------
Tue Jul 13 10:23:39 UTC 2021 - Stefan Schubert <schubi@suse.com

- Added /usr/local/libexec/health-checker for user defined plugins.

-------------------------------------------------------------------
Wed Aug 19 14:39:29 UTC 2020 - Ignaz Forster <iforster@suse.com>

- Update to version 1.4
  * Add checks for /tmp and systemd-logind
  * Build system cleanup

-------------------------------------------------------------------
Tue May 19 12:06:30 UTC 2020 - Dominique Leuenberger <dimstar@opensuse.org>

- Use the right variables for systemd unitdir and dracut modules
  directory.

-------------------------------------------------------------------
Tue May 12 15:49:31 UTC 2020 - Ignaz Forster <iforster@suse.com>

- Update to version 1.3.4
  * Really fix plugindir replacement in configure.ac script

-------------------------------------------------------------------
Fri May  8 15:51:03 UTC 2020 - Ignaz Forster <iforster@suse.com>

- Update to version 1.3.3
  * Fix plugindir replacement in configure.ac script
  * Use Python 3 for building documentation

-------------------------------------------------------------------
Wed May  6 14:26:27 UTC 2020 - Ignaz Forster <iforster@suse.com>

- Use pkgconfig macro instead of package dependencies

-------------------------------------------------------------------
Wed May  6 10:00:20 UTC 2020 - Ignaz Forster <iforster@suse.com>

- Update to version 1.3.2
  * Use pkgconf to determine installation directories instead of
    guessing

-------------------------------------------------------------------
Thu Jan 30 13:33:49 UTC 2020 - Ignaz Forster <iforster@suse.com>

- Update to version 1.3.1
  * Support multiple menuentries in GRUB configuration
    [gh#kubic-project/health-checker#5]

-------------------------------------------------------------------
Mon Dec 16 16:37:07 UTC 2019 - Ignaz Forster <iforster@suse.com>

- Update to version 1.3
  * Support /var on non-root device for reading health data
  * Avoid GRUB error message if env_block is not set [boo#1151072]
  * Don't show message on manual emergency shell invocation
  * Fix handling when booting a non-default snapshot

-------------------------------------------------------------------
Mon May  6 15:57:27 CEST 2019 - kukuk@suse.de

- Update to version 1.2.3
  * Fix crio RPM name

-------------------------------------------------------------------
Wed Mar 20 13:17:09 CET 2019 - kukuk@suse.de

- Update to version 1.2.2
  * Fix product namings

-------------------------------------------------------------------
Thu Mar 14 09:03:32 CET 2019 - kukuk@suse.de

- Update to version 1.2.1
  * Make sure telmetrics payload is not empty in success case

-------------------------------------------------------------------
Wed Feb 27 15:20:11 CET 2019 - kukuk@suse.de

- Update to version 1.2
  * Add hooks to send telemetrics events

-------------------------------------------------------------------
Mon Feb  4 10:57:58 CET 2019 - kukuk@suse.de

- Update to version 1.1
  * new plugins for crio and kubelet
- Add new sub-package with test cases for openSUSE Kubic

-------------------------------------------------------------------
Wed Dec 19 14:15:00 CET 2018 - kukuk@suse.de

- Split the CaaSP plugin into a MicroOS and CaaSP part

-------------------------------------------------------------------
Wed Aug  8 17:07:38 UTC 2018 - jengelh@inai.de

- Use noun phrase in summary.

-------------------------------------------------------------------
Wed May 23 13:05:39 CEST 2018 - kukuk@suse.de

- Update to version 1.0
  * Skip health checker if emergency shell started by user
  * Add GRUB2 fallback handling
  * Adapt to new unified /var directory.
  * Get basic version of rollback from initrd working

-------------------------------------------------------------------
Wed Aug 23 10:47:58 CEST 2017 - kukuk@suse.de

- Update to version 0.5
  - Fix logger arguments

-------------------------------------------------------------------
Thu May 25 07:36:23 UTC 2017 - jengelh@inai.de

- Resolve orthographical issues in descriptions.

-------------------------------------------------------------------
Thu May 18 13:51:44 CEST 2017 - kukuk@suse.de

- Update to version 0.4
- Rename package to health-checker
- Create new sub-package for CaaSP plugins

-------------------------------------------------------------------
Fri Mar 24 15:44:23 CET 2017 - kukuk@suse.de

- Update to version 0.2
  - Add plugins for etcd, etc-overlayfs and rebootmgr

-------------------------------------------------------------------
Thu Mar 23 16:03:14 CET 2017 - kukuk@suse.de

- Initial version

