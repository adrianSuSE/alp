[gdb/testsuite] Fix gdb.base/step-over-syscall.exp with -m32, AMD case

On an OBS machine with openSUSE Leap 15.2 and target board unix/-m32 I run
into:
...
(gdb) x/2i $pc^M
=> 0xf7fd5155 <__kernel_vsyscall+5>:    syscall ^M
   0xf7fd5157 <__kernel_vsyscall+7>:    int    $0x80^M
(gdb) PASS: gdb.base/step-over-syscall.exp: fork: displaced=off: \
  pc before/after syscall instruction
stepi^M
[Detaching after fork from child process 19924]^M
0xf7fd5159 in __kernel_vsyscall ()^M
1: x/i $pc^M
=> 0xf7fd5159 <__kernel_vsyscall+9>:    pop    %ebp^M
(gdb) PASS: gdb.base/step-over-syscall.exp: fork: displaced=off: stepi fork insn
print /x $pc^M
$2 = 0xf7fd5159^M
(gdb) PASS: gdb.base/step-over-syscall.exp: fork: displaced=off: pc after stepi
FAIL: gdb.base/step-over-syscall.exp: fork: displaced=off: \
  pc after stepi matches insn addr a
fter syscall
...

This is essentially a duplicate of the problem solved by commit 14852123287
"[gdb/testsuite] Fix gdb.base/step-over-syscall.exp with -m32", only here the
insn before "int $0x80" is syscall instead of sysenter.

The background here is that we're executing this code in the linux kernel
(from ./arch/x86/entry/vdso/vdso32/system_call.S):
...
        #define SYSENTER_SEQUENCE       "movl %esp, %ebp; sysenter"
        #define SYSCALL_SEQUENCE        "movl %ecx, %ebp; syscall"

        /* If SYSENTER (Intel) or SYSCALL32 (AMD) is available, use it. */
        ALTERNATIVE_2 "", SYSENTER_SEQUENCE, X86_FEATURE_SYSENTER32, \
                          SYSCALL_SEQUENCE,  X86_FEATURE_SYSCALL32
        ALTERNATIVE "", SYSENTER_SEQUENCE, X86_FEATURE_SEP
...
and depending on the active feature, we either have sysenter or syscall.

Fix this by recognizing the "syscall", "int    $0x80" sequence.

Tested on x86_64-linux.

---
 gdb/testsuite/gdb.base/step-over-syscall.exp | 9 +++++----
 1 file changed, 5 insertions(+), 4 deletions(-)

diff --git a/gdb/testsuite/gdb.base/step-over-syscall.exp b/gdb/testsuite/gdb.base/step-over-syscall.exp
index ecfb7be481d..a4821d7cd5a 100644
--- a/gdb/testsuite/gdb.base/step-over-syscall.exp
+++ b/gdb/testsuite/gdb.base/step-over-syscall.exp
@@ -162,12 +162,13 @@ proc setup { syscall } {
     }
 
     # If we encounter a sequence:
-    #   0xf7fd5155 <__kernel_vsyscall+5>:    sysenter
+    #   0xf7fd5155 <__kernel_vsyscall+5>:    sysenter / syscall
     #   0xf7fd5157 <__kernel_vsyscall+7>:    int    $0x80
     #   0xf7fd5159 <__kernel_vsyscall+9>:    pop    %ebp
-    # then a stepi at sysenter will step over the int insn, so make sure
-    # next_insn_addr points after the int insn.
-    if { $actual_syscall_insn == "sysenter" } {
+    # then a stepi at sysenter/syscall will step over the int insn,
+    # so make sure next_insn_addr points after the int insn.
+    if { $actual_syscall_insn == "sysenter"
+	 || $actual_syscall_insn == "syscall" } {
 	set test "pc after sysenter instruction"
 	set re_int_insn "\[ \t\]*int\[ \t\]\[^\r\n\]*"
 	set re [multi_line \
