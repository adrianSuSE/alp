[gdb/testsuite] Work around skip_prologue problems in gdb.threads/process-dies-while-detaching.exp

On powerpc64le-linux, I run into:
...
[Inferior 1 (process 5156) exited normally]^M
(gdb) FAIL: gdb.threads/process-dies-while-detaching.exp: single-process: \
  detach: detach: continue to breakpoint: _exit (the program exited)
...

What happens is the following:
- a breakpoint is set on _exit,
- a continue is issued
- the continue is supposed to hit the breakpoint, but instead
  the program exits.

I traced this down to the breakpoint on _exit being set too far from function
entry.  This is caused by the skip_prologue function (in rs6000-tdep.c)
optimistically ignoring insns it doesn't recognize.  In particular, it walks
past the system call instruction "sc" which initiates the actual exit.

While this needs fixing, we don't want to be testing this behaviour in this
test-case.

[ Initially I tried to fix it by setting a breakpoint on "*_exit" instead, but
that one only sets one location.  The breakpoint on "_exit" sets two
locations, one in /lib64/libc.so.6 and one in /lib64/ld64.so.2.  I tried on
x86_64 and there the breakpoint on "*_exit" mapped to the /lib64/libc.so.6
location, and the test-case passed.  But on powerpc it mapped to the
/lib64/ld64.so.2 location and I still got the same failures. ]

Fix this by setting two breakpoints on the calls to _exit and exit instead.

Tested on x86_64-linux and powerpc64le-linux.

---
 gdb/testsuite/gdb.threads/process-dies-while-detaching.c   | 4 ++--
 gdb/testsuite/gdb.threads/process-dies-while-detaching.exp | 8 ++++++--
 2 files changed, 8 insertions(+), 4 deletions(-)

diff --git a/gdb/testsuite/gdb.threads/process-dies-while-detaching.c b/gdb/testsuite/gdb.threads/process-dies-while-detaching.c
index 502b4622614..c4c0b0a648b 100644
--- a/gdb/testsuite/gdb.threads/process-dies-while-detaching.c
+++ b/gdb/testsuite/gdb.threads/process-dies-while-detaching.c
@@ -46,7 +46,7 @@ void *
 thread_function (void *arg)
 {
   pthread_barrier_wait (&start_threads_barrier);
-  _exit (0);
+  _exit (0); /* Exit in thread.  */
 }
 
 /* The fork child's entry point.  */
@@ -63,7 +63,7 @@ child_function (void)
     pthread_create (&threads[i], NULL, thread_function, NULL);
   pthread_barrier_wait (&start_threads_barrier);
 
-  exit (0);
+  exit (0); /* Exit in child.  */
 }
 
 /* This is defined by the .exp file if testing the multi-process
diff --git a/gdb/testsuite/gdb.threads/process-dies-while-detaching.exp b/gdb/testsuite/gdb.threads/process-dies-while-detaching.exp
index ac1aad26ec5..458d3bbeb56 100644
--- a/gdb/testsuite/gdb.threads/process-dies-while-detaching.exp
+++ b/gdb/testsuite/gdb.threads/process-dies-while-detaching.exp
@@ -126,8 +126,12 @@ proc detach_and_expect_exit {inf_output_re test} {
 # Run to _exit in the child.
 
 proc continue_to_exit_bp {} {
-    gdb_breakpoint "_exit" temporary
-    gdb_continue_to_breakpoint "_exit" ".*_exit.*"
+    set line [gdb_get_line_number "Exit in child"]
+    gdb_breakpoint $line temporary
+    set line [gdb_get_line_number "Exit in thread"]
+    gdb_breakpoint $line temporary
+    gdb_continue_to_breakpoint "exit" ".*exit.*"
+    delete_breakpoints
 }
 
 # If testing single-process, simply detach from the process.
