-------------------------------------------------------------------
Sun Dec 12 11:48:03 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.29.3
  libmpg123:
  * Catch more NULL pointer arguments in LFS wrappers
    (most prominently: mpg123_feedseek(), bug 328).
  mpg123:
  * Fix regression that did _not_ enable --remote-err on -s
    anymore.
  * Fix typos in man page (thanks to Naglis Jonaitis).
  * Drop mixed-up value limits on remote control SEQ command. It
    is up to you if you want to distort your sound.
  * Add note about equalizer frequency bands to man page.
  build:
  * add BUILD_PROGRAMS option to ports/cmake

-------------------------------------------------------------------
Sat Oct 23 12:07:00 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.29.2
  * libmpg123: Fix non-live-decoder safeguard for
    mpg123_framebyframe_decode() (was a no-op in practice).

-------------------------------------------------------------------
Mon Oct 18 06:45:26 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.29.1
  mpg123:
  * Keep default output encoding of s16 for raw and file outputs
    also with the new resampler. This reverts the unintentional
    change in 1.26.0 of switching to f32 for forced output rate
    unless the NtoM resampler is selected. In any case, you
    should make sure to specify your desired --encoding if you
    depend on it.
  * Catch error in indexing (mpg123_scan() return value was
    ignored before, bug 322).
  mpg123-strip:
  * Lift the resync limit, as it should be to clean up really
    dirty streams.
  mpg123-id3dump:
  * Also lift resync limit for the same reasons.
  libout123:
  * Fix reporting of device property flags for buffer
  libmpg123:
  * More safeguarding against attempts to decode if decoder
    setup failed and user ignored the returned error code (bug
    322)

-------------------------------------------------------------------
Mon Sep  6 07:08:48 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.29.0
  build:
  * added --enable-runtime-tables
  libmpg123:
  * Float deocder runtime table computation is back as option,
    based on suggestion and initial patch by Ethan Halsall for a
    smaller download size of the wasm decoder built from
    libmpg23. This only trims the size of the binary on disk
    (network), for runtime overhead and a bit of uneasyness about
    concurrency during table computation, which happens
    implicitly on handle initialization, only guarded by an
    integer flag. This does _not_ revive mpg123_init().
  * The ID3v2 UTF-16 BOM check is now a straight-on loop and not
    a recursive function.

-------------------------------------------------------------------
Mon Jul 12 10:51:17 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.28.2
  libout123:
  * Complete the fix for bug 314, reopening the device after
    format setup failure.

-------------------------------------------------------------------
Fri Jul  9 14:30:59 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.28.1
   build:
   * Explain --with-default-audio in configure help better.
   * Fix build of arm_fpu (regression of configure reorg).
   * Re-introduce AC_PROG_C_C99 macro for autoconf 2.69, it's
     only obsolete after that.
   * Un-break CMake build for botched move of CheckCPUArch.c.in.
   libmpg123:
   * Make mpg123.h.in usable again with MPG123_NO_CONFIGURE,
   for external uses.
   * Use predefined MPG123_API_VERSION in mpg123.h.in for the
     same.
   * Fix an integer constant definition for the most negative 32
     bit number to avoid justified compiler complaints.
   libsyn123:
   * More support for MPG123_NO_CONFIGURE.
   * Optionally use predefined SYN123_API_VERSION in syn123.h.in
     for the same.
   * Add a cast to silence integer sign warning for offset in
     muloffdiv64().
   libout123:
   * Pulse module advertises wider format support now, not
     just s16. This makes mpg123 -e s24 work with it, not just
     out123.
   * Optionally use predefined OUT123_API_VERSION in out123.h.in
     for non-configure use.

-------------------------------------------------------------------
Sat Jun  5 17:59:09 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.28.0
  build:
  * Fix up the build to actually build all library objects with
    libtool consistently, also ensuring no pointless static
    archives for output modules.
  * Adapted things to autoconf 2.71, requiring 2.69 now
  * Improved configure to be more useful --with-default-audio to
    define the search order, fix static build for --with-audio
    being a list (just choosing the first one).
  * Ensure consistent use of LINK_MPG123_DLL in headers.
  build (ports/cmake):
  * Hardcode ports/cmake CPU detection for x64 and ARM as
    CMAKE_SYSTEM_PROCESSOR is useless crap (bug 298 for real).
  * Added JACK output, fixed handling of compat_str there
  libsyn123:
  * Fix syn123_mix() to actually do intermediate conversion when
    input and output encoding are the same but non-float. This
    makes out123 --mix work with s16 input and output, which is
    not that special!
  libmpg123:
  *  Fix misguided handling of part2_3_length checks in
     III_get_scale_factors_1() and III_get_scale_factors_2()
     which invalidated decoding of a mono source encoded as
     ms+i-stereo (bug 312). This was a regression introduced
     with version 1.25.7.
  libout123:
  * Print basic module loading errors only for last one in list.
    This enables use of an output module search list that
    anticipates module files not installed with the main package.

-------------------------------------------------------------------
Sat May  8 20:09:45 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.27.2
  * Ensure debug.h is included last where it matters to avoid
    conflicts with debug/warning macros in system headers
  * Fix some debug/printf integer casts for 32 bit platforms.

-------------------------------------------------------------------
Fri May  7 04:45:32 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.27.0
  libmpg123:
  * Running on precomputed tables now, no need to call
    mpg123_init() anymore. That and mpg123_exit() are both just
    empty shells. You can omit them if you do not care about
    earlier libmpg123. You can check for MPG123_API_VERSION >=
    46.
  * Added API that avoids enums, mapped-to by default unless
    MPG123_ENUM_API is defined.
  libout123:
  * Added API that avoids enums, mapped-to by default unless
    MPG123_ENUM_API is defined.
  * Added device enumeration for win32, win32_wasapi, alsa,
    pulse.
    This increments the output module ABI version to 3.
  * Changed default output module order to put pulse before alsa
    since we now ensure that pulse is not inadvertedly started by
    the autospawn feature. This improves the experience on
    desktop systems with pulse where the alsa to pulse use
    causes glitches.
    Note that on a modern Linux desktop (Ubuntu), you will not
    escape an instance of pulseaudio being started, with even the
    enumeration of the ALSA default device summoning the daemon.
    If you _want_ sound daemon autospawn behaviour on other
    platforms, you need to trigger it outside of libout123.
  * examples: Update for dropped mpg123_init(), more sensible
    copyright notes.
  out123:
  * safer limiting of maximum playback rate
  * Added --list-devices.
  mpg123:
  * Fix --continue output to print track_count+1 as continue
    position after hitting the end of playlist. Makes
    scripts/conplay go to back to the beginning again (regression
    in 1.24.0, bug 250).
  * Remote control API version 9 with @I { .. @I } wrapping of
    ID3 and playlist display.
  * Added --list-devices.
  * Fix terminal control logic to better handle cases where
    stdin or stderr is not a terminal, also avoid enabling
    control if you specify stdin as input file.
  * Updated debugging/warning/error message macros to include
    the function name.

-------------------------------------------------------------------
Mon Mar 22 20:45:47 UTC 2021 - Luigi Baldoni <aloisio@gmx.com>

- Update to version  1.26.5
  * Add ./configure --enable-xdebug (for the resampler issue).
  * Avoid denormals in the resampler by adding an alternating
    offset (helps performance without -ffast-math, depending on
    platform).
  libmpg123:
  * Fix ID3v2 APIC parsing when frame length bit is set (bug
    306).
  * Also handle the group flag (skip the group byte).
  * Also fix up frame flag handling for ID3v2.3. Did not crop up
    yet, but it was just wrong. Impact was not detecting and
    bailing out on compressed or encrypted frames properly.

-------------------------------------------------------------------
Wed Feb 17 16:05:44 UTC 2021 - Fabian Vogt <fvogt@suse.com>

- Avoid unconditional Supplements

-------------------------------------------------------------------
Thu Dec 24 15:14:56 UTC 2020 - aloisio@gmx.com

- Update to version 1.26.4
  * Clarify seeking documentation regarding samples and PCM
    frames.
  * Fix cmake build to install fmt123.h.
  * Some cmake build fixes, tinyalsa addition by Maarten.
  * libmpg123: explicitly handle some irrelevant corner cases in
    tabinit

-------------------------------------------------------------------
Fri Jul 17 07:19:43 UTC 2020 - aloisio@gmx.com

- Update to version 1.26.3
  * Fix accurate (--enable-int-quality)

-------------------------------------------------------------------
Sun Jul  5 10:53:59 UTC 2020 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.26.2
  * Enable terminal control by default only when both input and
    output are connected to a terminal. This avoids messing with
    terminal settings when piping stderr to a pager, which takes
    over terminal input anyway, while mpg123 still thinks it got
    control.
  * More CMake build fixes
  * Use PROG_LIBS for output modules, to reinstate not
    necessarily proper but previous behaviour
  * Refine LFS support in libsyn123, avoiding
    architecture-dependent syn123.h

-------------------------------------------------------------------
Sat May 30 13:57:25 UTC 2020 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.26.1
  * Fix cmake build by actually including the read_api_version
    file in the distro.
  * Fix big-endian build, stupid omission of a variable
    declaration, semicolon.
  * Silence a harmless warning for build without realtime
    priority.
- Drop fix-ppc64_1.patch and fix-ppc64_2.patch (merged upstream)

-------------------------------------------------------------------
Tue May 26 13:18:11 UTC 2020 - Luigi Baldoni <aloisio@gmx.com>

- Add fix-ppc64_1.patch and fix-ppc64_2.patch

-------------------------------------------------------------------
Mon May 25 15:35:37 UTC 2020 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.26.0
  * Too many changes to list, see NEWS
- Add libsyn123 subpackage
- Drop Group tag
- Spec cleanup

-------------------------------------------------------------------
Sat Oct 26 10:58:43 UTC 2019 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.25.13
  libmpg123:
  * Reset the flag for having a frame to decode before trying to
    parse a new one. This prevents very unkind behaviour
    (crashes) when combinging mpg123_scan() with decoding later
    on for damaged streams that have a mixture of different MPEG
    versions.

-------------------------------------------------------------------
Sat Aug 24 19:01:13 UTC 2019 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.25.12
  * Fix dynamic build with gcc -fsanitize=address (check for all
    dl functions before deciding that separate -ldl is not
    needed).
  libmpg123:
  * Fix an out-of-bounds read of maximal two bytes for truncated
    RVA2 frames (oss-fuzz-bug 15975). The earlier fix around the
    same location needed one thought more. Actually, another
    though was needed, oss-fuzz-bug 16009 documents the
    incomplete fix.
  * Fix an invalid write of one zero byte for empty ID3v2 frames
    that demand de-unsyncing (oss-fuzz-bug 16050).
  * Correct preprocessor syntax in mangle.h, no #error in a
    #define line. (bug 273, thanks to nmlgc).

-------------------------------------------------------------------
Thu Jul 18 08:55:03 UTC 2019 - Luigi Baldoni <aloisio@gmx.com>

- Update to version 1.25.11
  libmpg123:
  * Fix out-of-bounds reads in ID3 parser for unsynced frames.
    (oss-fuzz-bug 15852)
  * Fix out-of-bounds read for RVA2 frames with non-delimited
    identifier. (oss-fuzz-bug 15852)
  * Fix implementation-defined parsing of RVA2 values.
    (oss-fuzz-bug 15862)
  * Fix undefined parsing of APE header for skipping. Also
    prevent endless loop on premature end of supposed APE header.
    (oss-fuzz-bug 15864)
  * Fix some syntax to make pedantic compiler happy.

- Spec cleanup

-------------------------------------------------------------------
Sat Jun 23 15:14:07 UTC 2018 - bjorn.lie@gmail.com

- Conditionalize pkgconfig(openal) BuildRequires and mpg123-openal
  sub-package, fix build for SLE12 SP3.

-------------------------------------------------------------------
Mon Mar  5 17:10:47 UTC 2018 - aloisio@gmx.com

- Update to version 1.25.10
  * libout123: Fix error messages beginning from OUT123_ARG_ERROR
    (bug 261).
  version 1.25.9
  * mpg123: Fix --icy-interval handling to work with stream from
    stdin. (curl | mpg123 --icy-interval=n -)
  * libmpg123: Fix another invalid read and segfault on damaged
    (fuzzed) files with part2_3_length == 0 (set maxband=1, pulled
    from upcoming 1.26.0).

-------------------------------------------------------------------
Sun Dec  3 05:08:39 UTC 2017 - aloisio@gmx.com

- Update to version 1.25.8
  mpg123:
  * Also disable cursor/video games for empty TERM (not just unset
    and dumb).
  libmpg123:
  * Accept changing mode extension bits when looking for next
    header for detecting free-format streams (bug 257).
  * Fix compute_bpf() for free format streams (needed to estimate
    track length and working fuzzy seeking in absence of an Info
    tag).

-------------------------------------------------------------------
Tue Sep 26 17:34:59 UTC 2017 - zaitor@opensuse.org

- Drop pkgconfig(esound) BuildRequires and mpg123-esound
  sub-package, esound is a long obsoleted sound server.

-------------------------------------------------------------------
Tue Sep 26 08:52:53 UTC 2017 - aloisio@gmx.com

- Update to version 1.25.7
  mpg123:
  * Do not play with cursor and inverse video for progress bar
    when TERM=dumb.
  * Fix parsing of host port for numerical IPv6 addresses (just did
    not work before, only for textual host names).
  libmpg123:
  * Proper fix for the xrpnt overflow problems by correctly
    initialising certain tables for MPEG 2.x layer III. The checks
    that catch the resulting overflow are still in place, but
    likely superfluous now. Note that this means certain valid
    files would have been misdecoded before, if anyone actually
    produced them. Thanks to Robert Hegemann for the fix!
  * Silently handle granules with part2_3_length == 0, but
    scalefac_compress != 0 (ignore the latter).

-------------------------------------------------------------------
Fri Aug 11 08:11:26 UTC 2017 - aloisio@gmx.com

- Update to version 1.25.6
  * Hotfix for bug 255: Overflow reading frame data bits in layer
    II decoding. Now, all-zero data is returned if the frame data
    is exhausted. This might have a slight impact on performance,
    but not easily measurable so far.

-------------------------------------------------------------------
Tue Aug  8 20:22:15 UTC 2017 - aloisio@gmx.com

- Update to version 1.25.5
  * Avoid another buffer read overflow in the ID3 parser on 32 bit
    platforms (bug 254).

-------------------------------------------------------------------
Mon Jul 24 11:51:43 UTC 2017 - aloisio@gmx.com

- Update to version 1.25.4
  libmpg123:
  * Prevent harmless call to memcpy(NULL, NULL, 0).
  * More early checking of ID3v2 encoding values to avoid bogus
    text being stored.

-------------------------------------------------------------------
Tue Jul 18 15:55:51 UTC 2017 - aloisio@gmx.com

- Update to version 1.25.3
  libmpg123:
  * Better checks for xrpnt overflow in III_dequantize_sample()
    before each use, avoiding false positives and catching cases
    that were rendered harmless by alignment-enlarged buffers.

-------------------------------------------------------------------
Tue Jul 11 10:36:15 UTC 2017 - aloisio@gmx.com

- Update to version 1.25.2
  libmpg123:
  * Extend pow tables for layer III to properly handle files
    with i-stereo and 5-bit scalefactors. Never observed them
    for real, just as fuzzed input to trigger the read overflow.
    Note: This one goes on record as CVE-2017-11126, calling
    remote denial of service. While the accesses are out of
    bounds for the pow tables, they still are safely within
    libmpg123's memory (other static tables). Just wrong values
    are used for computation, no actual crash unless you use
    something like GCC's AddressSanitizer, nor any information
    disclosure.
  * Avoid left-shifts of negative integers in layer I decoding.

-------------------------------------------------------------------
Mon Jul  3 06:45:17 UTC 2017 - aloisio@gmx.com

- Update to version 1.25.1
  * libmpg123:
    + Avoid memset(NULL, 0, 0) to calm down the paranoid.
    + Fix bug 252, invalid read of size 1 in ID3v2 parser due to
      forgotten offset from the frame flag bytes (unnoticed in
      practice for a long time). Fuzzers are in the house again.
      This one got CVE-2017-10683.
    + Avoid a mostly harmless conditional jump depending on
      uninitialised fr->lay in compute_bpf() (mpg123_position())
      when track is not ready yet.
    + Fix undefined shifts on signed long mask in layer3.c
      (worked in practice, never right in theory). Code might be
      a bit faster now, even. Thanks to Agostino Sarubbo for
      reporting.
  1.25.0:
  * Silence test for artsc-config if it is not there.
  * Make sure -static-libgcc from LDFLAGS gets through libtool,
    fixing 32 bit Windows builds (depend on libgcc DLL otherwise).
  * Fix build with non-GNU make by using plain rm -f instead of
    silly $(RM) in libout123/modules makefile fragment.
  * Make build work on iOS, including coreaudio backend.
  * libmpg123:
    + Finally provide position-independent code for x86 with
      assembly optimisations.The textrels are gone thanks to Won
      Kyu Park and Taihei Momma.
    + Clarify some license language in files descending from the
      original MMX optimisation.
    + Fix return value overflow check for MPG123_BUFFERFILL.
    + Introduced mpg123_getformat2() to enable the FORMAT command
      for the generic control not stealing MPG123_NEW_FORMAT from
      the main playback loop. The sequence LOADPAUSED-FORMAT-PAUSE
      (play) is supposed to work now.
    + Enable aarch64 optimisations on *BSD by default, too. You
      can always override that stupid OS whitelist using
      --with-optimization, anyway.
    + Use of the i486 decoder is now discouraged more
      prominently, in configure output.
  * out123: Fix stupid crash with verbose mode and tone
    generation (print the string if the pointer is non-null, not if
    it is null).
  * libout123: More consistent error messages for dynamic and
    legacy (built-in) modules. Namely, you get a hint how if you
    choose a different module than the built-in ones for a static
    libout123.

- Fixes (boo#1046766)

-------------------------------------------------------------------
Tue May 16 11:59:08 UTC 2017 - meissner@suse.com

- dont require mpg123-32bit, it is not present

-------------------------------------------------------------------
Tue May 16 11:30:52 UTC 2017 - mpluskal@suse.com

- Update baselibs.conf

-------------------------------------------------------------------
Sun Apr 30 17:51:31 UTC 2017 - meissner@suse.com

- add a baselibs.conf, so 32bit wine can use it or even
  build against it.

-------------------------------------------------------------------
Thu Mar 23 13:03:43 UTC 2017 - aloisio@gmx.com

- Create mpg123-openal as separate package
- Added Supplements lines for some subpackages

-------------------------------------------------------------------
Fri Mar 17 14:07:04 UTC 2017 - jengelh@inai.de

- Ensure neutrality of description

-------------------------------------------------------------------
Sun Mar  5 06:09:00 UTC 2017 - aloisio@gmx.com

- Update to version 1.24.0
  * Avoid repeating genre in metadata printout for 
    specifications like (144)Thrash Metal.
  * In remote control mode, only enforce --quiet if no 
    verbosity was required.
  * Prevent --loop and --shuffle or --random from messing with 
    the remote control LOADLIST command (printout of the list
    would loop without reason).
  * Fix the mpg123 command (esp. our provided binaries on 
    Windows) to now find modules again relative to the
    executable directory, not the current working directory.
    This was a regression in 1.23 and might be security-relevant
    if you called mpg123 in working directories with untrusted 
    content. Note that mpg123 1.23 looked for modules relative
    to the current working directory only if the installation
    prefix for modules did not exist. So, usage on an intact
    installation (with /usr/lib/mpg123 or the like) was safe.
    Nevertheless this new version fixes the search to be 
    relative to the binary path as it was with 1.22 and before.
  * At least consistent behaviour of playlist code in the face 
    of looping. Looping is about individual tracks, always.
    They are looped also in random mode. Jumping (prev/next
    keys) is between tracks and resets the loop counter.
    The display of currently playing track in the playlist is 
    fixed for random and looped play now (bug 198).
  * Looping is now mentioned for a to-be-repeated track with 
    --verbose.
  * Move some compiler nagging from --enable-debug to 
    --enable-nagging, fix up some new build failures by adding
    some pesky feature test macros.
  * Try not to pollute the terminal buffer with old progress 
    bars in inverse video. Only the currently live one shall
    be seen. That one is pretty. The others are not.
  * Using plain dlopen()/LoadLibrary() for opening modules 
    instead of libltdl. This also means that
    --with-module-suffix is gone in configure.
  * Windows builds only work when Unicode support is there 
    (older than Windows 2000/XP will definitely not work
    anymore).
  * The out123 tool now features tone generation, with a mix 
    of differing wave patterns. Makes sense to be able to test
    the audio  output by itself, and it's fun. See --wave-freq
    and related parameters.
  * libmpg123 version 43:
    + Add flags MPG123_NO_PEEK_END and MPG123_FORCE_SEEKABLE, 
      as suggested by Bent Bisballe Nyeng.
    + Build fix for MSVC (consistent definition of ssize_t, 
      spotted by manx, bug 243).
    + Build fix for --with-cpu=ppc_nofpu (thanks to Michael 
      Kostylev, bug 244).
    + Add asm optimized MSVC++ Win32|x64 and UWP|x64 builds
    + Remove old, broken MSVC++ builds
  * libout123 version 2:
    + Added OUT123_BINDIR.
    + New search order for output plugin directory: 
      MPG123_MODDIR, or (relative to executable directory
      OUT123_BINDIR) ../lib/mpg123, plugins
      libout123/modules/.libs, libout123/modules, 
      ../libout123/modules/.libs, ../libout123/modules, and at
      last the installation prefix $libdir/mpg213/. This shall
      ensure that a build inside a source tree does not try to
      use old modules from the system prefix. The normal libtool 
      wrapper deals with the shared libout123 or libmpg123 only,
      not modules. Note that if you set MPG123_MODDIR to a
      non-existing directory, no modules will be found (earlier
      versions fell back to other choices).
    + The OUT123_NAME parameter is now copied by
      out123_param_from(), as is the newly added OUT123_BINDIR.
    + Coreaudio: Use AudioComponents API on OSX >= 10.6 (thanks 
      to Michael Weiser).
    + Coreaudio: Fix behaviour of out123_drop(), not killing 
      the output anymore without re-opening the device (bug 236,
      thanks to Taihei for the fix).

- Build esound, pulse, jack, portaudio, sdl modules and created
  package for each of them.

-------------------------------------------------------------------
Fri Nov 11 13:09:20 UTC 2016 - dimstar@opensuse.org

- Initial package for openSUSE Tumbleweed, version 1.23.8.

