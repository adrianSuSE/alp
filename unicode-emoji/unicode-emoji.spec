#
# spec file for package unicode-emoji
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


%define long_version 13.0.0
Name:           unicode-emoji
Version:        13.1
Release:        0
Summary:        Unicode Emoji Data Files
License:        Unicode
Group:          System/I18n/Chinese
URL:            https://www.unicode.org/emoji/
#!RemoteAsset: sha256:96212602d5b49a6171018b0c53fbb02fe73e9ee5c7fb00fbac156a5652b11371
Source0:        https://www.unicode.org/copyright.html
#!RemoteAsset: sha256:406ea73c3b4e08c61e97f84ace4783aeaa9cae146c96fc2b89c6269f43f7de30
Source1:        https://www.unicode.org/Public/emoji/%{version}/ReadMe.txt
#!RemoteAsset: sha256:d2686f400a638c80775d7c662556fb8fa8dd3bbe4aa548d9d31624264c6e1bb1
Source2:        https://www.unicode.org/Public/%{long_version}/ucd/emoji/emoji-data.txt
#!RemoteAsset: sha256:69383979addf95080729ada27c1f134c8935234d84feb8f1676f0fcac93c1bac
Source3:        https://www.unicode.org/Public/emoji/%{version}/emoji-sequences.txt
#!RemoteAsset: sha256:8e3cc312a2a85115184738fa7ed97b3a9b3d0d221ffdaec3b22fd0314a18cd58
Source4:        https://www.unicode.org/Public/emoji/%{version}/emoji-test.txt
#!RemoteAsset: sha256:f1c6076d6a935596df86f362ef94aa3aec35dc210e6a96b63e7951d47f005e40
Source5:        https://www.unicode.org/Public/emoji/%{version}/emoji-zwj-sequences.txt
#!RemoteAsset: sha256:2efb7e9461e19c5f8e8fdb3b9a8122fb8148f50a901f0c2d4c8ba42209b5b55a
Source6:        https://www.unicode.org/Public/%{long_version}/ucd/emoji/emoji-variation-sequences.txt
BuildArch:      noarch

%description
Unicode Emoji Data Files are the machine-readable
emoji data files associated with
http://www.unicode.org/reports/tr51/index.html

%prep

%build

%install
install -d %{buildroot}%{_datadir}/unicode/emoji
cp -p %{SOURCE0} .
cp -p %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4} %{SOURCE5} %{SOURCE6} \
%{buildroot}%{_datadir}/unicode/emoji

%files
%license copyright.html
%dir %{_datadir}/unicode
%dir %{_datadir}/unicode/emoji
%doc %{_datadir}/unicode/emoji/ReadMe.txt
%{_datadir}/unicode/emoji/emoji-*txt

%changelog
