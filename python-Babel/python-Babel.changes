-------------------------------------------------------------------
Tue Oct  5 14:53:29 UTC 2021 - Stefan Schubert <schubi@suse.de>

- Added BuildRequires:  alts

-------------------------------------------------------------------
Thu Sep 30 15:35:45 UTC 2021 - Stefan Schubert <schubi@suse.de>

- Use libalternatives instead of update-alternatives. 

-------------------------------------------------------------------
Tue May 11 21:40:39 UTC 2021 - Dirk Müller <dmueller@suse.com>

- update to 2.9.1:
  * bsc#1185768 (CVE-2021-42771) The internal locale-data loading
    functions now validate the name of the locale file to be
    loaded and only allow files within Babel's data directory.

-------------------------------------------------------------------
Fri Mar  5 10:31:45 UTC 2021 - Markéta Machová <mmachova@suse.com>

- Substitute broken %ifpython3 macro

-------------------------------------------------------------------
Wed Dec 16 23:03:10 UTC 2020 - Michael Ströder <michael@stroeder.com>

- removed obsolete patches
  * python383.patch
  * pytest6.patch
- update to 2.9.0
  * Improvements
    - CLDR: Use CLDR 37 – Aarni Koskela (#734)
    - Dates: Handle ZoneInfo objects in get_timezone_location, get_timezone_name - Alessio Bogon (#741)
    - Numbers: Add group_separator feature in number formatting - Abdullah Javed Nesar (#726)
  * Bugfixes
    - Dates: Correct default Format().timedelta format to 'long' to mute deprecation warnings – Aarni Koskela
    - Import: Simplify iteration code in "import_cldr.py" – Felix Schwarz
    - Import: Stop using deprecated ElementTree methods "getchildren()" and "getiterator()" – Felix Schwarz
    - Messages: Fix unicode printing error on Python 2 without TTY. – Niklas Hambüchen
    - Messages: Introduce invariant that _invalid_pofile() takes unicode line. – Niklas Hambüchen
    - Tests: fix tests when using Python 3.9 – Felix Schwarz
    - Tests: Remove deprecated 'sudo: false' from Travis configuration – Jon Dufresne
    - Tests: Support Py.test 6.x – Aarni Koskela
    - Utilities: LazyProxy: Handle AttributeError in specified func – Nikiforov Konstantin (#724)
    - Utilities: Replace usage of parser.suite with ast.parse – Miro Hrončok
    - Support Py.test 6 - Aarni Koskela (#747, #750, #752)
  * Documentation
    - Update parse_number comments – Brad Martin (#708)
    - Add __iter__ to Catalog documentation – @CyanNani123

-------------------------------------------------------------------
Thu Aug 27 12:38:55 UTC 2020 - Marketa Calabkova <mcalabkova@suse.com>

- Add patch to fix building with new pytest 6.0.1:
  * pytest6.patch

-------------------------------------------------------------------
Tue May 19 10:38:24 UTC 2020 - Tomáš Chvátal <tchvatal@suse.com>

- Add patch to fix building with new python 3.8.3:
  * python383.patch

-------------------------------------------------------------------
Tue May 19 10:36:45 UTC 2020 - Tomáš Chvátal <tchvatal@suse.com>

- Do not bother with documentation package, it is all available online

-------------------------------------------------------------------
Mon Jan 13 14:40:12 UTC 2020 - Marketa Calabkova <mcalabkova@suse.com>

- update to 2.8.0
  * CLDR: Upgrade to CLDR 36.0
  * Messages: Don't even open files with the "ignore" extraction method
  * Numbers: Fix formatting very small decimals when quantization is disabled
  * Messages: Attempt to sort all messages

-------------------------------------------------------------------
Tue Jun  4 05:31:14 UTC 2019 - pgajdos@suse.com

- version update to 2.7.0
  Possibly incompatible changes
  * General: Internal uses of ``babel.util.odict`` have been replaced with
    ``collections.OrderedDict`` from The Python standard library.
  Improvements
  * CLDR: Upgrade to CLDR 35.1 - Alberto Mardegan, Aarni Koskela (#626, #643)
  * General: allow anchoring path patterns to the start of a string - 
    Brian Cappello (#600)
  * General: Bumped version requirement on pytz - @chrisbrake (#592)
  * Messages: `pybabel compile`: exit with code 1 if errors were encountered
    - Aarni Koskela (#647)
  * Messages: Add omit-header to update_catalog - Cédric Krier (#633)
  * Messages: Catalog update: keep user comments from destination by default
    - Aarni Koskela (#648)
  * Messages: Skip empty message when writing mo file - Cédric Krier (#564)
  * Messages: Small fixes to avoid crashes on badly formatted .po files
    - Bryn Truscott (#597)
  * Numbers: `parse_decimal()` `strict` argument and `suggestions`
    - Charly C (#590)
  * Numbers: don't repeat suggestions in parse_decimal strict - Serban
    Constantin (#599)
  * Numbers: implement currency formatting with long display names
    - Luke Plant (#585)
  * Numbers: parse_decimal(): assume spaces are equivalent to non-breaking
    spaces when not in strict mode - Aarni Koskela (#649)
  * Performance: Cache locale_identifiers() - Aarni Koskela (#644)
  Bugfixes
  * CLDR: Skip alt=... for week data (minDays, firstDay, weekendStart,
    weekendEnd) - Aarni Koskela (#634)
  * Dates: Fix wrong weeknumber for 31.12.2018 - BT-sschmid (#621)
  * Locale: Avoid KeyError trying to get data on WindowsXP - mondeja (#604)
  * Locale: get_display_name(): Don't attempt to concatenate variant
    information to None - Aarni Koskela (#645)
  * Messages: pofile: Add comparison operators to _NormalizedString - Aarni
    Koskela (#646)
  * Messages: pofile: don't crash when message.locations can't be sorted
    - Aarni Koskela (#646)

-------------------------------------------------------------------
Tue Dec  4 13:05:10 UTC 2018 - Matej Cepl <mcepl@suse.com>

- Remove superfluous devel dependency for noarch package

-------------------------------------------------------------------
Sun Jun 24 15:32:48 UTC 2018 - arun@gmx.de

- update to version 2.6.0:
  * Possibly incompatible changes
    + Numbers: Refactor decimal handling code and allow bypass of
      decimal quantization. (@kdeldycke) (PR #538)
    + Messages: allow processing files that are in locales unknown to
      Babel (@akx) (PR #557)
    + General: Drop support for EOL Python 2.6 and 3.3 (@hugovk) (PR
      #546)
  * Other changes
    + CLDR: Use CLDR 33 (@akx) (PR #581)
    + Lists: Add support for various list styles other than the
      default (@akx) (#552)
    + Messages: Add new PoFileError exception (@Bedrock02) (PR #532)
    + Times: Simplify Linux distro specific explicit timezone setting
      search (@scop) (PR #528)
  * Bugfixes
    + CLDR: avoid importing alt=narrow currency symbols (@akx) (PR
      #558)
    + CLDR: ignore non-Latin numbering systems (@akx) (PR #579)
    + Docs: Fix improper example for date formatting (@PTrottier) (PR
      #574)
    + Tooling: Fix some deprecation warnings (@akx) (PR #580)
  * Tooling & docs
    + Add explicit signatures to some date autofunctions (@xmo-odoo)
      (PR #554)
    + Include license file in the generated wheel package (@jdufresne)
      (PR #539)
    + Python 3.6 invalid escape sequence deprecation fixes (@scop) (PR
      #528)
    + Test and document all supported Python versions (@jdufresne) (PR
      #540)
    + Update copyright header years and authors file (@akx) (PR #559)

-------------------------------------------------------------------
Sun May  6 19:35:28 UTC 2018 - arun@gmx.de

- update to version 2.5.3:
  * This is a maintenance release that reverts undesired API-breaking
    changes that slipped into 2.5.2 (see
    https://github.com/python-babel/babel/issues/550). It is based on
    v2.5.1 (f29eccd) with commits 7cedb84, 29da2d2 and edfb518
    cherry-picked on top.

- changes from version 2.5.2:
  * Bugfixes
    + Revert the unnecessary PyInstaller fixes from 2.5.0 and 2.5.1
      (#533) (@yagebu)

-------------------------------------------------------------------
Tue Feb 27 12:28:43 UTC 2018 - aplanas@suse.com

- Export TZ and LC_ALL before running the tests.  Without this
  python-Babel is not able to see the TZ variable, and produce
  a `local` LOCALTZ

-------------------------------------------------------------------
Thu Oct  5 16:04:48 UTC 2017 - arun@gmx.de

- specfile:
  * require freezegun for tests

- update to version 2.5.1:
  * Use a fixed datetime to avoid test failures (#520)
    (@narendravardi)
  * Parse multi-line __future__ imports better (#519) (@akx)
  * Fix validate_currency docstring (#522)
  * Allow normalize_locale and exists to handle various unexpected
    inputs (#523) (@suhojm)
  * Make PyInstaller support more robust (#525, #526)
    (@thijstriemstra, @akx)

-------------------------------------------------------------------
Wed Aug 23 15:40:01 UTC 2017 - tbechtold@suse.com

- update to 2.5.0:
  * Numbers: Add currency utilities and helpers (#491) (@kdeldycke)
  * Support PyInstaller (#500, #505) (@wodo)
  * Dates: Add __str__ to DateTimePattern (#515) (@sfermigier)
  * Dates: Fix an invalid string to bytes comparison when parsing TZ files on Py3 (#498) (@rowillia)
  * Dates: Formatting zero-padded components of dates is faster (#517) (@akx)
  * Documentation: Fix "Good Commits" link in CONTRIBUTING.md (#511) (@naryanacharya6)
  * Documentation: Fix link to Python gettext module (#512) (@Linkid)
  * Messages: Allow both dash and underscore separated locale identifiers in pofiles (#489, #490) (@akx)
  * Messages: Extract Python messages in nested gettext calls (#488) (@sublee)
  * Messages: Fix in-place editing of dir list while iterating (#476, #492) (@MarcDufresne)
  * Messages: Stabilize sort order (#482) (@xavfernandez)
  * Time zones: Honor the no-inherit marker for metazone names (#405) (@akx)

-------------------------------------------------------------------
Sat May  6 03:37:17 UTC 2017 - toddrme2178@gmail.com

- Fix provides/obsoletes

-------------------------------------------------------------------
Fri Mar 31 19:38:21 UTC 2017 - aloisio@gmx.com

- Update to 2.4.0
  * CLDR: CLDR 29 is now used instead of CLDR 28 (#405) (@akx)
  * Messages: Add option ‘add_location’ for location line
    formatting (#438, #459) (@rrader, @alxpy)
  * Numbers: Allow full control of decimal behavior (#410)
    (@etanol)
- Converted to single-spec

-------------------------------------------------------------------
Mon May  2 19:00:01 UTC 2016 - dmueller@suse.com

- update to 2.3.4:
  * CLDR: The lxml library is no longer used for CLDR importing, so it should not cause strange failures e
  * CLI: Every last single CLI usage regression should now be gone, and both distutils and stand-alone CLI
  * CLI: Usage regressions that had snuck in between 2.2 and 2.3 should be no more. (https://github.com/py
  * Dates: Period (am/pm) formatting was broken in certain locales (namely zh_TW). Thanks to @jun66j5 for 
  * CLDR: Add an API for territory language data (https://github.com/python-babel/babel/pull/315)
  * Core: Character order and measurement system data is imported and exposed (https://github.com/python-b
  * Dates: Add an API for time interval formatting (https://github.com/python-babel/babel/pull/316)
  * Dates: More pattern formats and lengths are supported (https://github.com/python-babel/babel/pull/347)
  * Dates: Period IDs are imported and exposed (https://github.com/python-babel/babel/pull/349)
  * Dates: Support for date-time skeleton formats has been added (https://github.com/python-babel/babel/pu
  * Dates: Timezone formatting has been improved (https://github.com/python-babel/babel/pull/338)
  * Messages: JavaScript extraction now supports dotted names, ES6 template strings and JSX tags (https://
- drop fix-timezone-test.patch skip-dst-tests.patch, no longer necessary

-------------------------------------------------------------------
Mon Feb  1 10:40:23 UTC 2016 - toddrme2178@gmail.com

- update to version 2.2.0:
  * Bugfixes
    + General: Add __hash__ to Locale. (#303) (2aa8074)
    + General: Allow files with BOM if they're UTF-8 (#189) (da87edd)
    + General: localedata directory is now locale-data (#109)
      (2d1882e)
    + General: odict: Fix pop method (0a9e97e)
    + General: Removed uses of datetime.date class from *.dat files
      (#174) (94f6830)
    + Messages: Fix plural selection for chinese (531f666)
    + Messages: Fix typo and add semicolon in plural_forms (5784501)
    + Messages: Flatten NullTranslations.files into a list (ad11101)
    + Times: FixedOffsetTimezone: fix display of negative offsets
      (d816803)
  * Features
    + CLDR: Update to CLDR 28 (#292) (9f7f4d0)
    + General: Add __copy__ and __deepcopy__ to LazyProxy. (a1cc3f1)
    + General: Add official support for Python 3.4 and 3.5
    + General: Improve odict performance by making key search O(1)
      (6822b7f)
    + Locale: Add an ordinal_form property to Locale (#270) (b3f3430)
    + Locale: Add support for list formatting (37ce4fa, be6e23d)
    + Locale: Check inheritance exceptions first (3ef0d6d)
    + Messages: Allow file locations without line numbers (#279)
      (79bc781)
    + Messages: Allow passing a callable to `extract()` (#289)
      (3f58516)
    + Messages: Support 'Language' header field of PO files (#76)
      (3ce842b)
    + Messages: Update catalog headers from templates (e0e7ef1)
    + Numbers: Properly load and expose currency format types (#201)
      (df676ab)
    + Numbers: Use cdecimal by default when available (b6169be)
    + Numbers: Use the CLDR's suggested number of decimals for
      format_currency (#139) (201ed50)
    + Times: Add format_timedelta(format='narrow') support (edc5eb5)
- update to version 2.1.1:
  * Fix Locale.parse using "global.dat" incompatible types
    (https://github.com/python-babel/babel/issues/174)
  * Fix display of negative offsets in "FixedOffsetTimezone"
    (https://github.com/python-babel/babel/issues/214)
  * Improved odict performance which is used during localization file
    build, should improve compilation time for large projects
  * Add support for "narrow" format for "format_timedelta"
  * Add universal wheel support
  * Support 'Language' header field in .PO files
    (fixes https://github.com/python-babel/babel/issues/76)
  * Test suite enhancements (coverage, broken tests fixed, etc)
  * Documentation updated
- changes from version 2.0:
  * Added support for looking up currencies that belong to a territory
    through the :func:`babel.numbers.get_territory_currencies`
    function.
  * Improved Python 3 support.
  * Fixed some broken tests for timezone behavior.
  * Improved various smaller things for dealing with dates.
- changes from version 1.4:
  * Fixed a bug that caused deprecated territory codes not being
    converted properly by the subtag resolving.  This for instance
    showed up when trying to use "und_UK" as a language code
    which now properly resolves to "en_GB".
  * Fixed a bug that made it impossible to import the CLDR data
    from scratch on windows systems.
- fix update-alternatives
- add skip-dst-tests.patch: drop DST related tests (workaround
  issue #156)
- Add fix-timezone-test.patch: already included upstream
- Remove 0001-disable_timezone_tests.patch in favor of 
  fix-timezone-test.patch

-------------------------------------------------------------------
Fri Jul 24 07:19:45 UTC 2015 - seife+obs@b1-systems.com

- fix build on CentOS/RHEL by specifying TZ=UTC for %check
- fix CentOS/RHEL by depending on /usr/sbin/update-alternatives

-------------------------------------------------------------------
Thu May 28 12:21:10 UTC 2015 - toddrme2178@gmail.com

- Fix update-alternatives usage

-------------------------------------------------------------------
Sat Nov 08 17:11:00 UTC 2014 - Led <ledest@gmail.com>

- fix bashism in pre script

-------------------------------------------------------------------
Fri Aug 15 11:42:11 UTC 2014 - sfalken@opensuse.org

- Removed un-needed BuildRequire for timezone 

-------------------------------------------------------------------
Fri Aug 15 02:54:46 UTC 2014 - sfalken@opensuse.org

- Added 0001-disable_timezone_tests.patch
  + Disabling tests so package will build.  Tests can be re-enabled
    when upstream bug is resolved (gh#mitsuhiko/babel#106)

-------------------------------------------------------------------
Tue Jan  7 14:54:43 UTC 2014 - speilicke@suse.com

- Bring back argparse requirement for SP3

-------------------------------------------------------------------
Thu Oct 24 10:59:04 UTC 2013 - speilicke@suse.com

- Require python-setuptools instead of distribute (upstreams merged)

-------------------------------------------------------------------
Mon Sep 30 09:00:12 UTC 2013 - speilicke@suse.com

- Implement update-alternatives
- Fix testsuite run on SLE and run it only once
- Properly build (and install) HTML documentation

-------------------------------------------------------------------
Mon Sep 30 08:30:30 UTC 2013 - dmueller@suse.com

- make tests happy on openSUSE_12.2 (too old pytests)

------------------------------------------------------------------
Fri Sep 27 19:59:09 UTC 2013 - dmueller@suse.com

- update to 1.3.0:
 - support python 2.6, 2.7, 3.3+ and pypy - drop all other versions
 - use tox for testing on different pythons
 - Added support for the locale plural rules defined by the CLDR.
 - Added `format_timedelta` function to support localized formatting of
   relative times with strings such as "2 days" or "1 month" (ticket #126).
 - Fixed negative offset handling of Catalog._set_mime_headers (ticket #165).
 - Fixed the case where messages containing square brackets would break with
   an unpack error.
 - updated to CLDR 23
 - Make the CLDR import script work with Python 2.7.
 - Fix various typos.
 - Sort output of list-locales.
 - Make the POT-Creation-Date of the catalog being updated equal to
   POT-Creation-Date of the template used to update (ticket #148).
 - Use a more explicit error message if no option or argument (command) is
   passed to pybabel (ticket #81).
 - Keep the PO-Revision-Date if it is not the default value (ticket #148).
 - Make --no-wrap work by reworking --width's default and mimic xgettext's
   behaviour of always wrapping comments (ticket #145).
 - Add --project and --version options for commandline (ticket #173).
 - Add a __ne__() method to the Local class.
 - Explicitly sort instead of using sorted() and don't assume ordering
   (Jython compatibility).
 - Removed ValueError raising for string formatting message checkers if the
   string does not contain any string formattings (ticket #150).
 - Fix Serbian plural forms (ticket #213).
 - Small speed improvement in format_date() (ticket #216).
 - Fix so frontend.CommandLineInterface.run does not accumulate logging
   handlers (#227, reported with initial patch by dfraser)
 - Fix exception if environment contains an invalid locale setting (#200)
 - use cPickle instead of pickle for better performance (#225)
 - Only use bankers round algorithm as a tie breaker if there are two nearest
   numbers, round as usual if there is only one nearest number (#267, patch by
   Martin)
 - Allow disabling cache behaviour in LazyProxy (#208, initial patch from Pedro
   Algarvio)
 - Support for context-aware methods during message extraction (#229, patch
   from David Rios)
 - "init" and "update" commands support "--no-wrap" option (#289)
 - fix formatting of fraction in format_decimal() if the input value is a float
   with more than 7 significant digits (#183)
 - fix format_date() with datetime parameter (#282, patch from Xavier Morel)
 - fix format_decimal() with small Decimal values (#214, patch from George Lund)
 - fix handling of messages containing '\\n' (#198)
 - handle irregular multi-line msgstr (no "" as first line) gracefully (#171)
 - parse_decimal() now returns Decimals not floats, API change (#178)
 - no warnings when running setup.py without installed setuptools (#262)
 - modified Locale.__eq__ method so Locales are only equal if all of their
   attributes (language, territory, script, variant) are equal
 - resort to hard-coded message extractors/checkers if pkg_resources is
   installed but no egg-info was found (#230)
 - format_time() and format_datetime() now accept also floats (#242)
 - add babel.support.NullTranslations class similar to gettext.NullTranslations
   but with all of Babel's new gettext methods (#277)
 - "init" and "update" commands support "--width" option (#284)
 - fix 'input_dirs' option for setuptools integration (#232, initial patch by
   Étienne Bersac)
 - ensure .mo file header contains the same information as the source .po file
   (#199)
 - added support for get_language_name() on the locale objects.
 - added support for get_territory_name() on the locale objects.
 - added support for get_script_name() on the locale objects.
 - added pluralization support for currency names and added a '¤¤¤'
   pattern for currencies that includes the full name.
 - depend on pytz now and wrap it nicer.  This gives us improved support
   for things like timezone transitions and an overall nicer API.
 - Added support for explicit charset to PO file reading.
 - Added experimental Python 3 support.
 - Added better support for returning timezone names.
 - Don't throw away a Catalog's obsolete messages when updating it.
 - Added basic likelySubtag resolving when doing locale parsing and no
   match can be found.
- run tests
- use setuptools, not distribute
- add missing dependency on pytz
 
-------------------------------------------------------------------
Thu Sep  1 14:26:07 UTC 2011 - saschpe@suse.de

- Fixed license to BSD-3-Clause (SPDX style)

-------------------------------------------------------------------
Thu Sep  1 14:17:39 UTC 2011 - saschpe@suse.de

- Update to version 0.9.6:
  * Backport r493-494: documentation typo fixes.
  * Make the CLDR import script work with Python 2.7.
  * Fix various typos.
  * Fixed Python 2.3 compatibility (ticket #146, #233).
  * Sort output of list-locales.
  * Make the POT-Creation-Date of the catalog being updated equal to
    POT-Creation-Date of the template used to update (ticket #148).
  * Use a more explicit error message if no option or argument (command) is
    passed to pybabel (ticket #81).
  * Keep the PO-Revision-Date if it is not the default value (ticket #148).
  * Make --no-wrap work by reworking --width's default and mimic xgettext's
    behaviour of always wrapping comments (ticket #145).
  * Fixed negative offset handling of Catalog._set_mime_headers (ticket #165).
  * Add --project and --version options for commandline (ticket #173).
  * Add a __ne__() method to the Local class.
  * Explicitly sort instead of using sorted() and don't assume ordering
    (Python 2.3 and Jython compatibility).
  * Removed ValueError raising for string formatting message checkers if the
    string does not contain any string formattings (ticket #150).
  * Fix Serbian plural forms (ticket #213).
  * Small speed improvement in format_date() (ticket #216).
  * Fix number formatting for locales where CLDR specifies alt or draft 
    items (ticket #217)
  * Fix bad check in format_time (ticket #257, reported with patch and tests by
    jomae)
  * Fix so frontend.CommandLineInterface.run does not accumulate logging 
    handlers (#227, reported with initial patch by dfraser)
  * Fix exception if environment contains an invalid locale setting (#200)
- Renamed to python-Babel (from python-babel)

-------------------------------------------------------------------
Mon Dec 20 15:23:47 UTC 2010 - saschpe@suse.de

- re-added dependency to python-setuptools

-------------------------------------------------------------------
Wed Dec 15 14:29:04 UTC 2010 - saschpe@suse.de

- added documentation:
  * files: COPYING ChangeLog INSTALL.txt README.txt
  * HTML documentation

-------------------------------------------------------------------
Sat Dec 11 20:59:55 UTC 2010 - saschpe@gmx.de

- re-generated spec file with py2pack:
  * now builds for Fedora and Mandriva

-------------------------------------------------------------------
Wed Sep  1 16:43:56 UTC 2010 - jmatejek@novell.com

- added python-devel to buildrequires, to fix build on new python
  (because the package requires doctest, which requires unittest,
  which should probably both go into python-devel. (but this is
  subject to change))
- dropped check for obsolete SUSE version

-------------------------------------------------------------------
Sun Apr 25 18:18:39 UTC 2010 - bitdealer@gmail.com

- Update to 0.9.5.
- Switch to noarch.
- Clean up spec.

