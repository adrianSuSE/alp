-------------------------------------------------------------------
Mon Feb 14 21:28:26 UTC 2022 - Dirk Müller <dmueller@suse.com>

- update to 1.1.3:
  * Fix some header files cannot be used alone (#550).
  * Fix the method of introducing system pybind11 (#566).
  * Support Node.js 16 (#597).
  * Support Python 3.9 (#603).
  * Fixed conversion errors.
  * Several other small fixes.  

-------------------------------------------------------------------
Fri Mar 12 21:26:02 UTC 2021 - Dirk Müller <dmueller@suse.com>

- update to 1.1.2:
  * Added Hong Kong Traditional Chinese conversion.
  * Fix the compilation compatibility problem, including parallel compilation.
  * Fixed the serious performance degradation problem introduced since 1.1.0. 
- drop opencc-1.1.1-missing-builtin-types.patch: upstream

-------------------------------------------------------------------
Mon Sep 28 05:07:44 UTC 2020 - Marguerite Su <i@marguerite.su>

- add opencc-1.1.1-missing-builtin-types.patch
  * SimpleConverter.hpp missed "#include <string>" that breaks pyzy 

-------------------------------------------------------------------
Sat Sep 12 19:49:53 UTC 2020 - Dirk Mueller <dmueller@suse.com>

- update to 1.1.1
  * Officially provide interface and TypeScript type annotations.
  * Update the dynamic link library `SOVERSION` to `1.1`, due to
  changes in the internal interface of C++.
  * Simplify the header file structure and speed up compilation. Remove
  unnecessary `using`.  Repair some Hong Kong standard characters.
  * The new dictionary format `ocd2`, based on Marisa Trie 0.2.5. The
  dictionary size is greatly reduced.
  * Change the default conversion of "Yong/Yong" and modify multiple
  phrase conversions.
  * Add benchmark test results.
  * Officially supports Japanese new font conversion.
  * Upgrade Node.js dependency and improve compatibility.
  * Fix multiple multi-platform compilation and compatibility issues.
  * Correct a large number of conversion errors.

-------------------------------------------------------------------
Wed Jan  3 19:38:59 UTC 2018 - tchvatal@suse.com

- Use python3 for all the operations do not implicitely inherit py2

-------------------------------------------------------------------
Thu Nov  2 13:43:51 UTC 2017 - mpluskal@suse.com

- Cleanup spec file a bit
- Use more of cmake macros

-------------------------------------------------------------------
Thu Oct  5 13:28:26 UTC 2017 - hillwood@opensuse.org

- Update to 1.0.5
  * Fix link error for mingw
  * Try fix error nodejs_version=4; Platform: x86
  * Add support for node stable && remove v0.12 
  * Update artifacts
  * Artifacts for appveyor

-------------------------------------------------------------------
Sat Mar 12 08:24:11 UTC 2016 - i@marguerite.su

- update version 1.0.3.1

-------------------------------------------------------------------
Wed Aug 12 00:16:09 UTC 2015 - i@marguerite.su

- update version 1.0.3

-------------------------------------------------------------------
Sun Feb 15 03:59:02 UTC 2015 - hillwood@linuxfans.org

- Update to 1.0.2
  * Update dictionary 
  * Fix scripts compatibility for Python3
  * Fix compatibility with Python3
  * Add non-allocation interface
  * Add Values() to DictEntry for convinience
  * Fix `OPENCC_DEFAULT_CONFIG_SIMP_TO_TRAD`

-------------------------------------------------------------------
Tue Jul  2 13:58:43 UTC 2013 - i@marguerite.su

- update version 0.4.3
  * add interface: `opencc_convert_utf8_free`
  * fix memeory leak problem for nodejs plugin

-------------------------------------------------------------------
Mon Nov  5 01:09:40 UTC 2012 - i@marguerite.su

- split a data package to be required by library.
  fix a conversion crash bug in fcitx.

-------------------------------------------------------------------
Sun Jun 24 19:02:21 UTC 2012 - i@marguerite.su

- fix fedora builds.

-------------------------------------------------------------------
Sun Jun  3 02:53:01 UTC 2012 - i@marguerite.su

- fix sles builds.

-------------------------------------------------------------------
Thu Feb 16 15:37:21 UTC 2012 - coolo@suse.com

- trigger the service so it's true

-------------------------------------------------------------------
Mon Jan 23 12:40:44 UTC 2012 - i@marguerite.su

- initial package 0.3.0 

