#
# spec file for package pigz
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           pigz
Version:        2.6
Release:        0
Summary:        Multi-core gzip version
License:        Zlib
Group:          Productivity/Archiving/Compression
URL:            https://www.zlib.net/pigz/
#!RemoteAsset: sha256:2eed7b0d7449d1d70903f2a62cd6005d262eb3a8c9e98687bc8cbb5809db2a7d
Source0:        https://www.zlib.net/%{name}/%{name}-%{version}.tar.gz
#!RemoteAsset: sha256:6c58147554cde5678a274a1a1407a1b77af987a4df72a65a5c45360310909d3e
Source1:        https://www.zlib.net/%{name}/%{name}-%{version}-sig.txt#/%{name}-%{version}.tar.gz.asc
Source9:        %{name}.keyring
BuildRequires:  zlib-devel

%description
A parallel implementation of gzip for modern multi-processor, multi-core machines

%prep
%setup -q
%autopatch -p1

%build
%make_build CFLAGS="%{optflags}"

%check
%make_build tests CFLAGS="%{optflags}"

%install
install -Dpm 0755 pigz \
  %{buildroot}%{_bindir}/pigz
install -Dpm 0644 pigz.1 \
  %{buildroot}%{_mandir}/man1/pigz.1
ln -sv pigz %{buildroot}%{_bindir}/unpigz

%files
%doc README
%{_bindir}/pigz
%{_bindir}/unpigz
%{_mandir}/man1/pigz.1%{?ext_man}

%changelog
