-------------------------------------------------------------------
Mon Dec 13 14:14:53 UTC 2021 - Danilo Spinella <danilo.spinella@suse.com>

- Update stb_image/stb_image_write to include the fixes for
  the following CVEs:
  CVE-2021-28021, bsc#1191743
  CVE-2021-42715, bsc#1191942
  CVE-2021-42716, bsc#1191944
  * 269.patch

-------------------------------------------------------------------
Fri Nov  5 11:58:38 UTC 2021 - Danilo Spinella <danilo.spinella@suse.com>

- Do not build examples to avoid a cycle with QT5Multimedia

-------------------------------------------------------------------
Wed Nov  3 14:42:33 UTC 2021 - Danilo Spinella <danilo.spinella@suse.com>

- Use the updated cmake3-full package instead of cmake on SLE12
- Do not build examples on SLE12
- Only build blackbox tests on TW

-------------------------------------------------------------------
Sat May 29 10:37:28 UTC 2021 - Ferdinand Thiessen <rpm@fthiessen.de>

- Update to 1.2.0
  * new BarcodeFormat names, old ones are deprecated
  * new ZXingQtCamReader demo app based on QtMultimedia and QtQuick
  * new QRCode reader, faster and better support for rotated symbols
  * added Structured Append support for DataMatrix, Aztec and MaxiCode
  * added DMRE support for DataMatrix
  * switch to the reimplemented 1D detectors, about 5x faster
  * a lot faster and more capable isPure detection for all 2D codes
  * 20% faster ReedSolomon error correcting
  * PDF417 is faster and supports flipped symbols
  * reduced false positive rate for UPC/EAN barcodes and improved
    Add-On symbol handling
  * proper ECI handling in all 2D barcodes
  * much improved python wrapper
  * deprecate the use of ResultMetadata
- Add cmake-check-system-first.patch
- Drop upstream merged 0001-Fix-build-with-GCC-11.patch

-------------------------------------------------------------------
Tue Feb  9 07:50:14 UTC 2021 - Christophe Giboudeaux <christophe@krop.fr>

- Update to 1.1.1. No changelog available.
- Drop fix-pkg-config-file.patch. Merged upstream
- Add 0001-Fix-build-with-GCC-11.patch to fix GCC 11 build
  issues (boo#1181915)

-------------------------------------------------------------------
Fri Jan 15 00:29:36 UTC 2021 - Simon Lees <sflees@suse.de>

- Use %cmake_build instead of %make_jobs
- Shouldn't need -DBUILD_SHARED_LIBRARY=ON

-------------------------------------------------------------------
Fri Oct 23 08:33:47 UTC 2020 - Antonio Larrosa <alarrosa@suse.com>

- Add baselibs.conf

-------------------------------------------------------------------
Fri Sep  4 07:16:43 UTC 2020 - Antonio Larrosa <alarrosa@suse.com>

- Add fix for pkg-config file so the version and library are set properly
  in the resulting .pc file:
  * fix-pkg-config-file.patch

-------------------------------------------------------------------
Sun Aug 16 19:19:01 UTC 2020 - Christophe Giboudeaux <christophe@krop.fr>

- Update to 1.1.0
  * Add Python binding
  * Bug fixes from upstream XZing project
  * Many performance improvements for 1D readers
  * More meta-data exported when reading specific format
  * Minor bug fixes and improvements for corner cases
  * Improve DataMatrix encoder
  * Add interface to simplify basic usage
  * WASM API to support pixels array as input
  * Few minor bug fixes
  * A new and (hopefully) 'future proof' single ReadBarcode entry point
    into the decoding functionality.
  * The LuminanceSource based API is now deprecated but still compiles.
  * A new BarcodeFormats flag type to specify the set of barcodes to look for.
  * Deprecated unrelyable Result::resultPoints in favor of well defined
    Result::position.
  * Deprecated Result::metadata() -> ORIENTATION in favor
    of Result::orientation.
  * New Binarizer enum in DecodeHints to specify the binarizer for
    the ReadBarcode API.
  * New DecodeHints::isPure property to speed up detection for
    'pure' input use-cases.
  * New 'unified' CMake structure to build (most) of the project from
    the top-level project.
  * New ZXingReader and ZXingWriter example code also meant for distributing.
  * New simplified and consistent Python API (breaking change)
  * ReedSolomon error detection code 2x speedup.
  * Enable basic MaxiCode support.
  * Fix coutry-code metatdata decoding for UPC/EAN codes.
  * Slightly improved QRCode detection for rotated symbols.
  * Faster PDF417 decoder.
  * Lots of minor code readability and general cleanup improvements.
- Drop patches:
  * add-missing-includes-of-stdexcept-header.patch
  * fix-library-installation-and-versioning.patch

-------------------------------------------------------------------
Thu Dec  5 08:57:46 UTC 2019 - Martin Liška <mliska@suse.cz>

- Add add-missing-includes-of-stdexcept-header.patch
  in order to fix boo#1158377.

-------------------------------------------------------------------
Thu May 23 12:11:25 UTC 2019 - Jan Engelhardt <jengelh@inai.de>

- Trim metadata redundancies from description.

-------------------------------------------------------------------
Wed May 22 05:06:25 UTC 2019 - Luca Beltrame <lbeltrame@kde.org>

- Fixup URL and Source

-------------------------------------------------------------------
Fri May 17 05:22:57 UTC 2019 - Luca Beltrame <lbeltrame@kde.org>

- Initial package for openSUSE
- Add upstream patch to add proper installability and library
  versioning:
  * fix-library-installation-and-versioning.patch
