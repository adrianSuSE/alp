-------------------------------------------------------------------
Thu Nov 18 21:30:11 UTC 2021 - Jan Engelhardt <jengelh@inai.de>

- Update to release 1.2.1
  * expr: add last match time support
  * expr: missing netlink attribute in last expression

-------------------------------------------------------------------
Tue May 25 12:42:58 UTC 2021 - Jan Engelhardt <jengelh@inai.de>

- Update to release 1.2.0
  * table: add table owner support
  * expr: socket: add cgroups v2 support

-------------------------------------------------------------------
Fri Jan 15 21:17:24 UTC 2021 - Jan Engelhardt <jengelh@inai.de>

- Update to release 1.1.9
  * Improve formatting of registers in bitwise dumps.

-------------------------------------------------------------------
Tue Oct 27 10:14:27 UTC 2020 - Jan Engelhardt <jengelh@inai.de>

- Update to release 1.1.8
  * libnftnl: export nftnl_set_elem_fprintf
  * examples: add support for NF_PROTO_INET family
  * table: add userdata support
  * object: add userdata and comment support
  * chain: add userdata and comment support
  * src: add support for chain ID attribute

-------------------------------------------------------------------
Fri Jun  5 13:04:57 UTC 2020 - Jan Engelhardt <jengelh@inai.de>

- Update to release 1.1.7
  * udata: add NFTNL_UDATA_SET_DATA_INTERVAL

-------------------------------------------------------------------
Wed Apr  1 18:40:30 UTC 2020 - Jan Engelhardt <jengelh@inai.de>

- Update to release 1.1.6
  * add slave device matching
  * support for NFTNL_SET_EXPR

-------------------------------------------------------------------
Mon Dec  2 15:45:50 UTC 2019 - Jan Engelhardt <jengelh@inai.de>

- Update to release 1.1.5
  * flowtable: add support for handle attribute
  * obj/ct_timeout: Avoid array overrun in timeout_parse_attr_data()

-------------------------------------------------------------------
Mon Aug 19 12:04:35 UTC 2019 - Jan Engelhardt <jengelh@inai.de>

- Update to new upstream release 1.1.4
  * Add ct id support, add ct expectation support,
    add synproxy support.

-------------------------------------------------------------------
Tue May 28 08:30:59 UTC 2019 - Jan Engelhardt <jengelh@inai.de>

- Update to new upstream release 1.1.3
  * expr: osf: add version option support
  * udata: add NFTNL_UDATA_* definitions
  * chain: support per chain rules listing

-------------------------------------------------------------------
Tue Nov 13 13:52:57 UTC 2018 - Jan Engelhardt <jengelh@inai.de>

- Update to new upstream release 1.1.2
  * This release adds supports for new kernel extensions: tproxy,
    tunneling, xfrm, osf, conntrack timeouts and helpers. This
    release deprecates the nftnl_rule_cmp() interface and the low
    level JSON infrastructure.
- Remove bufferov.diff (obsolete)

-------------------------------------------------------------------
Sat Jun  9 07:14:08 UTC 2018 - jengelh@inai.de

- Update to new upstream release 1.1.1
  * expr: add map lookups for numgen and hash statements
  * rule: add nftnl_rule_list_insert_at()
  * expr: add connlimit support
  * expr: extend fwd to support address and protocol

-------------------------------------------------------------------
Wed May  2 06:01:14 UTC 2018 - jengelh@inai.de

- Update to new upstream release 1.1.0
  * Add new API infrastructure to support "flow tables".

-------------------------------------------------------------------
Tue Jan  2 21:04:42 UTC 2018 - jengelh@inai.de

- Update to new upstream release 1.0.9
  * Mostly bug fixes plus one new nftnl_expr_fprintf() function.

-------------------------------------------------------------------
Fri Oct 13 00:29:52 UTC 2017 - jengelh@inai.de

- Update to new upstream release 1.0.8
  * ct: add support for zone, helper and eventmask
  * exthdr: tcp option set support
  * rt: tcpmss get support
  * ct: add average bytes per packet counter support
  * exthdr: Add support for exthdr flags
- Add bufferov.diff
- Drop baselibs.conf

-------------------------------------------------------------------
Tue Dec 20 21:59:39 UTC 2016 - jengelh@inai.de

- Update to new upstream release 1.0.7
  * Support for new kernel expressions: Number generator
    ("numgen"), routing ("rt"), range, inverted set lookups,
    inverted dynamic set updates (i.e. rule mismatch on full
    sets), packet quota, hash, Forward Information Base lookups
    ("fib"), reference to stateful objects, and "notrack".
  * Allow to add userdata to sets.
  * Support for stateful objects, including quota and counter
  * Support for layer 4 pseudoheader fields checksum updates

-------------------------------------------------------------------
Mon May 30 11:06:35 UTC 2016 - jengelh@inai.de

- Update to new upstream release 1.0.6
* New TLV infrastructure for user data are in rule, set and
  elements.
* Support for the new tracing infrastructure.
* Matching of ct bytes and packets.
* meta prandom support.
* Enhancements for the limit expressions.
* Support for payload mangling.
* Masquerading port range selection.

-------------------------------------------------------------------
Thu Sep 17 13:42:05 UTC 2015 - jengelh@inai.de

- Update to new upstream release 1.0.5
* Resolve improper symbol versioning in the shared library

-------------------------------------------------------------------
Wed Sep 16 11:56:20 UTC 2015 - jengelh@inai.de

- Update to new upstream release 1.0.4
* src: add batch abstraction
* table: add netdev family support
* chain: add netdev family support
* set: add support for set timeouts
* set_elem: add timeout support
* data: increase maximum possible data size
* set_elem: support expressions attached to set elements
* dynset: support expression templates

-------------------------------------------------------------------
Mon Dec 15 23:49:03 UTC 2014 - jengelh@inai.de

- Update to new upstream release 1.0.3
* No upstream summary provided
* Support for new features up to Linux 3.18

-------------------------------------------------------------------
Mon Dec  8 23:41:15 UTC 2014 - jengelh@inai.de

- Drop %version from subincludedir, it tends to break recompiles of
  already-built trees on library updates.

-------------------------------------------------------------------
Tue Sep 23 16:05:59 UTC 2014 - jengelh@inai.de

- Correct pkgconfig-assisted location of headers to match up with
  remaining packages

-------------------------------------------------------------------
Fri Jun 27 15:37:24 UTC 2014 - jengelh@inai.de

- Update to new upstream release 1.0.2
* support for Linux 3.15 features, event monitoring, and fixes for
  the XML/JSON infrastructure

-------------------------------------------------------------------
Fri Apr 18 12:31:22 UTC 2014 - jengelh@inai.de

- Update to new upstream release 1.0.1
* Memory leak and invalid read fixes
* Add conntrack label match support
* meta: Let user specify any combination of sreg/dreg
* expr: ct: Add support for setting the mark
- Remove 0001-build-resolve-build-failure-involving-linux-netlink..patch,
  0002-build-resolve-compile-error-involving-XT_EXTENSION_M.patch

-------------------------------------------------------------------
Mon Dec 17 00:03:37 UTC 2012 - jengelh@inai.de

- Initial package (1.0.0+git1) for build.opensuse.org
- Add 0001-build-resolve-build-failure-involving-linux-netlink..patch,
  0002-build-resolve-compile-error-involving-XT_EXTENSION_M.patch
