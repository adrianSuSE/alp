-------------------------------------------------------------------
Tue Nov 16 08:37:12 UTC 2021 - Steve Kowalik <steven.kowalik@suse.com>

- Update to 3.6.0:
  * Fixed DeprecationWarning in importlib.resources.read_text.
  * #5: Fixed warning in docs builds.

-------------------------------------------------------------------
Sat May 15 17:25:42 UTC 2021 - Arun Persaud <arun@gmx.de>

- specfile:
  * update copyright year
  * skip python2

- update to version 3.5.0:
  * Rely on PEP 420 for namespace package.

- changes from version 3.4.0:
  * Added WordSet.trim* methods.

- changes from version 3.3.0:
  * Require Python 3.6 or later.

-------------------------------------------------------------------
Sat Mar 14 08:09:33 UTC 2020 - Tomáš Chvátal <tchvatal@suse.com>

- Fix build on older openSUSE releases as with py3.6 we need the
  importlib_resources too

-------------------------------------------------------------------
Thu Jan 16 09:44:33 UTC 2020 - Tomáš Chvátal <tchvatal@suse.com>

- Update to 3.2.0:
  * Added normalize_newlines function.
  * Added wrap and unwrap functions and lorem_ipsum attribute containing the Lorem Ipsum sample text.

-------------------------------------------------------------------
Tue Mar 26 14:34:01 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Update to 3.0:
  * Remove collections dependency to avoid buildcycle
- Remove multibuild as per above

-------------------------------------------------------------------
Wed Feb 27 19:08:25 UTC 2019 - Tomáš Chvátal <tchvatal@suse.com>

- Use multibuild to avoid buildcycle

-------------------------------------------------------------------
Tue Feb 26 07:00:39 UTC 2019 - John Vandenberg <jayvdb@gmail.com>

- Add LICENSE
- Compile manually due to switch to pkgutil namespace technique
- Add fdupes
- Remove bcond_with test, allowing tests suite to run
- Add missing build dependency jaraco.collections
- Remove undesirable build dependencies pytest-flake8, pytest-sugar and
  collective.checkdocs
- Add missing runtime dependencies six, jaraco.base, jaraco.collections
  and jaraco.functools
- Update to v2.0
  * Switch to pkgutil namespace technique for the ``jaraco`` namespace.

-------------------------------------------------------------------
Thu Aug 23 10:51:49 UTC 2018 - dheidler@suse.de

- Initial Package for v1.10.1
