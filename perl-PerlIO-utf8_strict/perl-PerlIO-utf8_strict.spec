#
# spec file for package perl-PerlIO-utf8_strict
#
# Copyright (c) 2022 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


%define cpan_name PerlIO-utf8_strict
Name:           perl-PerlIO-utf8_strict
Version:        0.009
Release:        0
Summary:        Fast and correct UTF-8 IO
License:        Artistic-1.0 OR GPL-1.0-or-later
URL:            https://metacpan.org/release/%{cpan_name}
#!RemoteAsset: sha256:ba82cf144820655d6d4836d12dde65f8895a3d905aeb4aa0b421249f43284c14
Source0:        https://cpan.metacpan.org/authors/id/L/LE/LEONT/%{cpan_name}-%{version}.tar.gz
Source1:        cpanspec.yml
Patch0:         no-return-in-nonvoid-function.patch
BuildRequires:  perl
BuildRequires:  perl-macros
BuildRequires:  perl(Test::Exception)
BuildRequires:  perl(Test::More) >= 0.88
%{perl_requires}

%description
This module provides a fast and correct UTF-8 PerlIO layer. Unlike perl's
default ':utf8' layer it checks the input for correctness.

%prep
%autosetup  -n %{cpan_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
%make_build

%check
make test

%install
%perl_make_install
%perl_process_packlist
%perl_gen_filelist

%files -f %{name}.files
%doc Changes README
%license LICENSE

%changelog
