From lucas.demarchi@intel.com Wed Aug 18 23:25:03 2021
To: linux-modules <linux-modules@vger.kernel.org>
Subject: [PATCH v2] libkmod: Set builtin to no when module is created from path.
Date: Wed, 18 Aug 2021 14:24:39 -0700
Message-Id: <20210818212440.2224127-1-lucas.demarchi@intel.com>
From: Michal Suchanek <msuchanek@suse.de>

A recent bug report showed that modinfo doesn't give the signature
information for certain modules, and it turned out to happen only on
the modules that are built-in on the running kernel; then modinfo
skips the signature check, as if the target module file never exists.
The behavior is, however, inconsistent when modinfo is performed for
external modules (no matter which kernel version is) and the module
file path is explicitly given by a command-line argument, which
guarantees the presence of the module file itself.

Fixes: e7e2cb61fa9f ("modinfo: Show information about built-in modules")
Link: https://lore.kernel.org/linux-modules/CAKi4VAJVvY3=JdSZm-GD1hJqyCPYaYz-jBJ_REeY5BakVb6_ww@mail.gmail.com/
BugLink: https://bugzilla.opensuse.org/show_bug.cgi?id=1189537
Suggested-by: Lucas De Marchi <lucas.de.marchi@gmail.com>
Signed-off-by: Michal Suchanek <msuchanek@suse.de>
---
 libkmod/libkmod-module.c | 17 +++++++++--------
 1 file changed, 9 insertions(+), 8 deletions(-)

diff --git a/libkmod/libkmod-module.c b/libkmod/libkmod-module.c
index 6e0ff1a..6f7747c 100644
--- a/libkmod/libkmod-module.c
+++ b/libkmod/libkmod-module.c
@@ -431,17 +431,18 @@ KMOD_EXPORT int kmod_module_new_from_path(struct kmod_ctx *ctx,
 			return -EEXIST;
 		}
 
-		*mod = kmod_module_ref(m);
-		return 0;
-	}
+		kmod_module_ref(m);
+	} else {
+		err = kmod_module_new(ctx, name, name, namelen, NULL, 0, &m);
+		if (err < 0) {
+			free(abspath);
+			return err;
+		}
 
-	err = kmod_module_new(ctx, name, name, namelen, NULL, 0, &m);
-	if (err < 0) {
-		free(abspath);
-		return err;
+		m->path = abspath;
 	}
 
-	m->path = abspath;
+	m->builtin = KMOD_MODULE_BUILTIN_NO;
 	*mod = m;
 
 	return 0;
-- 
2.31.1


