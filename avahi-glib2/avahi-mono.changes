-------------------------------------------------------------------
Wed Dec  8 02:57:51 UTC 2021 - Yifan Jiang <yfjiang@suse.com>

- Disable monodoc on ppc64, ppc64le and s390x, because the mdoc.exe
  is completely not shipped with monocore-doc on them (bsc#1192212).

-------------------------------------------------------------------
Mon Oct 25 13:20:49 UTC 2021 - Yifan Jiang <yfjiang@suse.com>

- Change %python38_version_nodots to %suse_version which is
  compatible with Leap and SLE. See also:

  https://github.com/openSUSE/python-rpm-macros/issues/107

-------------------------------------------------------------------
Thu Sep 16 01:21:50 UTC 2021 - Stanislav Brabec <sbrabec@suse.com>

- Remove obsolete translation-update-upstream support
  (jsc#SLE-21105).

-------------------------------------------------------------------
Mon Aug 31 10:21:27 UTC 2020 - Antonio Larrosa <alarrosa@suse.com>

- Update to version 0.8

-------------------------------------------------------------------
Thu Aug  6 07:59:00 UTC 2020 - Jan Engelhardt <jengelh@inai.de>

- Skip the xargs dance and just use find directly.

-------------------------------------------------------------------
Wed Jan  8 10:17:46 UTC 2020 - Martin Liška <mliska@suse.cz>

- Use %make_build.

-------------------------------------------------------------------
Mon Aug 12 17:29:45 UTC 2019 - Bjørn Lie <bjorn.lie@gmail.com>

- Drop gtk2-devel and python-gtk-devel BuildRequires: No longer
  build gtk2 support. Following this, pass --disable-gtk to
  configure. Drop sub-package libavahi-ui0, no longer built.
- Drop long disabled sub-packages libavahi-qt4-1 and
  libavahi-qt4-devel.

-------------------------------------------------------------------
Fri Jul  5 12:22:21 UTC 2019 - matthias.gerstner@suse.com

- removal of SuSEfirewall2 service, since SuSEfirewall2 has been replaced by
  firewalld, see [1].

  [1]: https://lists.opensuse.org/opensuse-factory/2019-01/msg00490.html

-------------------------------------------------------------------
Thu Nov 23 13:37:31 UTC 2017 - rbrown@suse.com

- Replace references to /var/adm/fillup-templates with new 
  %_fillupdir macro (boo#1069468)

-------------------------------------------------------------------
Fri Apr 21 13:49:59 UTC 2017 - bwiedemann@suse.com

- use strip-nondeterminism to make build fully reproducible

-------------------------------------------------------------------
Sat Sep 17 13:52:32 UTC 2011 - jengelh@medozas.de

- Remove redundant tags/sections from specfile
- Use %_smp_mflags for parallel build
- Add libavahi-devel to baselibs

-------------------------------------------------------------------
Mon Mar 15 17:10:10 CET 2010 - sbrabec@suse.cz

- Added support for translation-update-upstream (FATE#301344).

-------------------------------------------------------------------
Sun Aug  9 12:43:26 CEST 2009 - coolo@novell.com

- use new python macros

-------------------------------------------------------------------
Mon Sep  1 10:22:46 CEST 2008 - meissner@suse.de

- Added GCC attribute alloc_size markup for allocator functions

-------------------------------------------------------------------
Fri Jul 18 17:18:20 CEST 2008 - sbrabec@suse.cz

- Updated to version 0.6.23:
  * A lot of translation updates
  * Beef up bnvc quite a bit, including passing a domain to browse
    in
  * Increase numer of open files resource limit to 300 so that we
    can deal with more clients simultaneously.
  * Rework 'poof' algorithm a bit to reduce traffic load on noisy
    links.
  * Build fixes
  * Minor other updates
  * Backwards compatible with Avahi 0.6.x with x < 23.

-------------------------------------------------------------------
Sun May 11 11:49:29 CEST 2008 - coolo@suse.de

- fix rename of xxbit packages

-------------------------------------------------------------------
Tue Mar 11 16:18:47 CET 2008 - sbrabec@suse.cz

- Fix build failure of avahi-mono.

-------------------------------------------------------------------
Thu Oct 11 14:36:43 CEST 2007 - sbrabec@suse.de

- Updated dependencies.

-------------------------------------------------------------------
Mon Aug  6 17:03:54 CEST 2007 - bk@suse.de

- replace -p /usr/sbin/ldconfig with %{run_ldconfig}

-------------------------------------------------------------------
Mon Jul 30 08:54:27 CEST 2007 - aj@suse.de

- Add gcc-c++ to BuildRequires.

-------------------------------------------------------------------
Mon Jul 16 09:14:26 CEST 2007 - aj@suse.de

- Create new spec file to avoid build cycle avahi->gnome->mono->avahi.

